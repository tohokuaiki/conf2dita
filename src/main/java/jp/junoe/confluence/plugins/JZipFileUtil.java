/*
 * 本プラグインで使用するZIPにファイルに関することの様々なちょっとした処理を集めたもの
 */
package jp.junoe.confluence.plugins;

import com.atlassian.confluence.pages.Attachment;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

public class JZipFileUtil {

    /**
     * ZIPファイルかどうかをチェックする
     *
     * @param file チェック対象ファイル
     * @return
     */
    public static boolean isZipFile(File file) {
        ZipFile zipFile = null;
        boolean ret = true;
        try {
            zipFile = new ZipFile(file, "MS932");
        } catch (IOException ex) {
            ret = false;
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException ex) {
                }
            }
        }

        return ret;
    }
    
    /**
     * ZipOutputStreamにエントリを追加する(String)
     * @param zos
     * @param entryPath
     * @param text
     * @throws java.io.IOException
     */
    public static void appendStringEntry(ZipOutputStream zos, String entryPath, String text) throws IOException {

            byte[] buf = new byte[1024];

            final ByteArrayInputStream bais = new ByteArrayInputStream(text.getBytes("UTF-8"));
            final BufferedInputStream is = new BufferedInputStream(bais);
            
            final ZipEntry errorEntry = new ZipEntry(entryPath);
            zos.putNextEntry(errorEntry);
            for (;;) {
                int len = is.read(buf);
                if (len < 0) {
                    break;
                }
                zos.write(buf, 0, len);
            }
    }
    
    /**
     * ZipOutputStreamにエントリを追加する(Attachment)
     * @param zos
     * @param entryPath
     * @param attachment
     * @throws java.io.IOException
     */
    public static void appendAttachmentEntry(ZipOutputStream zos, String entryPath, Attachment attachment) throws IOException {

            byte[] buf = new byte[1024];

            final ZipEntry errorEntry = new ZipEntry(entryPath);
            try {            
                zos.putNextEntry(errorEntry);
            } catch (IOException ex) {
                throw ex;
            }
            InputStream is = attachment.getContentsAsStream();
            for (;;) {
                int len = is.read(buf);
                if (len < 0) {
                    break;
                }
                zos.write(buf, 0, len);
            }
            IOUtils.closeQuietly(is);
            
//                        zos.putNextEntry(entry);
//                        is = a.getContentsAsStream();
//                        if (is != null) {
//                            for (;;) {
//                                int len = is.read(buf);
//                                if (len < 0) {
//                                    break;
//                                }
//                                zos.write(buf, 0, len);
//                            }
//                        }
//                        zos.closeEntry();
            
    }
    
    
    
            

    /**
     * ZIPフィル内のリストを取得
     *
     * @param file
     * @return
     */
    public static List<ZipEntry> getZipFileList(File file) {
        List<ZipEntry> list;
        ZipFile zipfile = null;
        list = new ArrayList();
        HashMap<String, Object> map;
        map = new HashMap();
        if (isZipFile(file)) {
            try {
                zipfile = new ZipFile(file, "MS932");
                Enumeration enumeration;
                enumeration = zipfile.getEntries();
                while (enumeration.hasMoreElements()) {
                    ZipEntry entry = (ZipEntry) enumeration.nextElement();
                    list.add(entry);
                    map.put(entry.getName(), entry);
                }
            } catch (IOException ex) {
            } finally {
                if (zipfile != null) {
                    try {
                        zipfile.close();
                    } catch (IOException ex) {
                    }
                }
            }
        }

        return list;
    }

    /**
     * Zipファイルのリストをソートする
     *
     * @param ziplist Zipファイルのリスト
     * @return
     */
    public static TreeMap<String, ZipEntry> sortZipFileList(List<ZipEntry> ziplist) {
        TreeMap<String, ZipEntry> list = new TreeMap<String, ZipEntry>();
        
        for (ZipEntry ze: ziplist){
            list.put(ze.getName(), ze);
        }
        
        return list;
    }
        
    
    /**
     * Zipファイルのリストをソートして親子関係のわかるものにする
     * @deprecated 
     * @param ziplist
     * @return 
     */
    public static ArrayList<HashMap<String, ArrayList<ZipEntry>>> sortZipFileList2(List<ZipEntry> ziplist) {
        ArrayList<HashMap<String, ArrayList<ZipEntry>>> list = new ArrayList();
        TreeMap<String, TreeMap<String, ZipEntry>> list2 = new TreeMap();
        String dir = "", filename = "";

        // 詰め込み
        for (ZipEntry ze : ziplist) {
            // ディレクトリリスト
            filename = ze.getName();
            int lastSlash = filename.lastIndexOf("/");
            if (lastSlash < 0) {
                dir = "/";
            } else {
                dir = filename.substring(0, lastSlash + 1);
            }
            if (list2.get(dir) == null) {
                list2.put(dir, new TreeMap<String, ZipEntry>());
            }
            // Append
            list2.get(dir).put(filename, ze);
        }

        for (Map.Entry<String, TreeMap<String, ZipEntry>> tm : list2.entrySet()) {
            // directory property
            HashMap<String, ArrayList<ZipEntry>> hm = new HashMap<String, ArrayList<ZipEntry>>();
            // files
            ArrayList<ZipEntry> files = new ArrayList<ZipEntry>();
            for (Map.Entry<String, ZipEntry> ze : tm.getValue().entrySet()) {
                files.add(ze.getValue());
            }
            hm.put(tm.getKey(), files);
            list.add(hm);
        }

        return list;
    }
    
    /**
     * ZIPファイルからentryで示したZipエントリの内容をStringとして取得(UTF-8固定)
     * @param zipfile
     * @param entry
     * @param fileCharset
     * @return
     * @throws IOException 
     */
    public static String getZipEntryContent(File zipfile, ZipEntry entry, String fileCharset) throws IOException {
        String content = "";
        
        ZipFile zip = new ZipFile(zipfile, fileCharset); // charset=MS932
        
        // 展開対象のZipエントリ内容を取得
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        BufferedOutputStream bos = null;
        byte[] buf = new byte[1024];
        
        baos = new ByteArrayOutputStream();
        bos = new BufferedOutputStream(baos); 
        is = zip.getInputStream(entry);
        int size = 0;
        while ((size = is.read(buf)) != -1) {
            bos.write(buf, 0, size);
        }
        bos.flush();
        
        content = baos.toString("UTF-8");
        IOUtils.closeQuietly(is);
        IOUtils.closeQuietly(bos);
        IOUtils.closeQuietly(baos);
        
        return content;
    }

    /**
     * zipファイルを解凍します。ソートした結果を返す.
     *
     * @param zipFilePath 解凍するZIPファイルのフルパス
     * @param outputDirPath 解凍先ディレクトリのフルパス
     * @param charset 文字コード(ファイル名またはディレクトリ名に使用される文字コード)
     * @return 出力ディレクトリ直下に解凍されたファイルまたディレクトリのリスト
     *
     * @throws IOException
     * @throws java.util.zip.ZipException
     */
    public static TreeMap<String, File> unZip(final String zipFilePath, final String outputDirPath, final String charset)
            throws IOException, ZipException, Exception {

        TreeMap<String, File> treemap = new TreeMap<String, File>();

        if (StringUtils.isEmpty(zipFilePath)) {
            throw new IllegalArgumentException("引数(zipFilePath)がnullまたは空文字です。");
        }
        if (StringUtils.isEmpty(outputDirPath)) {
            throw new IllegalArgumentException(
                    "引数(outputDirPath)がnullまたは空文字です。");
        }
        List<File> unzipList = unZip(new File(zipFilePath), new File(outputDirPath), charset);
        for (File f : unzipList) {
            treemap.put(f.getAbsolutePath(), f);
        }

        return treemap;

    }

    /**
     * zipファイルを解凍します。<br>
     * Ant1.8.1にて確認
     *
     * @param zipFile 解凍するZIPファイル
     * @param outputDir 解凍先ディレクトリ
     * @param charset 文字コード(ファイル名またはディレクトリ名に使用される文字コード)
     * @return 出力ディレクトリ直下に解凍されたファイルまたディレクトリのリスト
     * @throws FileNotFoundException
     * @throws ZipException
     * @throws IOException
     */
    private static List<File> unZip(final File zipFile, final File outputDir, final String charset)
            throws FileNotFoundException, ZipException, IOException, Exception {
        
        if (zipFile == null) {
            throw new IllegalArgumentException("引数(zipFile)がnullです。");
        }
        if (outputDir == null) {
            throw new IllegalArgumentException("引数(outputDir)がnullです。");
        }
        if (StringUtils.isEmpty(charset)) {
            throw new IllegalArgumentException("引数(charset)がnullまたは空文字です。");
        }
        if (outputDir.exists() && !outputDir.isDirectory()) {
            throw new IllegalArgumentException(
                    "引数(outputDir)はディレクトリではありません。outputDir=" + outputDir);
        }

        // 出力ディレクトリ直下に解凍されたファイルまたディレクトリのセット
        final Set<File> fileSet = new HashSet<File>();

        // 解答したファイルの親ディレクトリのセット
        final Set<File> parentDirSet = new HashSet<File>();

        ZipFile zip = null;
        try {
            try {
                // 文字コードを指定することで文字化けを回避
                zip = new ZipFile(zipFile, charset);
            } catch (IOException e) {
                throw e;
            }

            final Enumeration<?> zipEnum = zip.getEntries();
            while (zipEnum.hasMoreElements()) {
                // 解凍するアイテムを取得
                final ZipEntry entry = (ZipEntry) zipEnum.nextElement();

                if (entry.isDirectory()) {
                    // 解凍対象がディレクトリの場合
                    final File dir = new File(outputDir, entry.getName());
                    if (dir.getParentFile()
                            .equals(outputDir)) {
                        // 親ディレクトリが出力ディレクトリなのでfileSetに格納
                        fileSet.add(dir);
                    }
                    // ディレクトリは自分で生成
                    if (!dir.exists() && !dir.mkdirs()) {
                        throw new Exception("ディレクトリの生成に失敗しました。dir=" + dir);
                    }
                } else {
                    // 解凍対象がファイルの場合
                    final File file = new File(outputDir, entry.getName());
                    final File parent = file.getParentFile();
                    assert parent != null;

                    //if (parent.equals(outputDir)) {
                    // 解凍ファイルの親ディレクトリが出力ディレクトリの場合
                    fileSet.add(file);
                    //}

                    if (!parentDirSet.contains(parent)) {
                        // 親ディレクトリが初見の場合
                        parentDirSet.add(parent);

                        // 解凍ファイルの上位にある出力ディレクトリ直下のディレクトリを取得
                        final File rootDir = getRootDir(outputDir, file);
                        assert rootDir != null;
                        fileSet.add(rootDir);

                        // 親ディレクトリを生成
                        if (!parent.exists() && !parent.mkdirs()) {
                            throw new Exception("親ディレクトリの生成に失敗しました。parent=" + parent);
                        }
                    }

                    // 解凍対象のファイルを書き出し
                    FileOutputStream fos = null;
                    InputStream is = null;
                    try {
                        fos = new FileOutputStream(file);
                        is = zip.getInputStream(entry);

                        byte[] buf = new byte[1024];
                        int size = 0;
                        while ((size = is.read(buf)) != -1) {
                            fos.write(buf, 0, size);
                        }
                        fos.flush();
                    } catch (FileNotFoundException e) {
                        throw e;
                    } catch (ZipException e) {
                        throw e;
                    } catch (IOException e) {
                        throw e;
                    } finally {
                        if (fos != null) {
                            try {
                                fos.close();
                            } catch (IOException e1) {
                                throw new Exception("IOリソース開放失敗(FileOutputStream)", e1);
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e1) {
                                throw new Exception("IOリソース開放失敗(InputStream)", e1);
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            throw e;
        } catch (ZipException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } finally {
            if (zip != null) {
                try {
                    zip.close();
                } catch (IOException e) {
                    throw new Exception("IOリソース開放失敗(ZipFile)", e);
                }
            }
        }

        // Setだと何かと不便なのでListに変換
        List<File> retList = new ArrayList<File>(fileSet);

        return retList;
    }

    /**
     * 指定ディレクトリ直下にある、指定ファイルの親ディレクトリを再帰的に検索する。
     *
     *
     * @param dir
     * @param file
     * @return
     */
    private static File getRootDir(final File dir, final File file) {
        assert dir != null;
        assert !dir.exists() || dir.exists() && dir.isDirectory();
        assert file != null;

        final File parent = file.getParentFile();
        if (parent == null) {
            return null;
        }
        if (parent.equals(dir)) {
            return file;
        }
        return getRootDir(dir, parent);
    }
}
