package jp.junoe.confluence.plugins;

import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.core.util.PairType;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * HTTPサーブレットを使ったりする際に色々と便利なメソッド集
 *
 * @author Takashi
 */
public class JConfluenceHTTPUtil {

    /**
     * like php inArray
     * @param value
     * @param selector
     * @return 
     */
    public static boolean inSelectOptions(String value, List<PairType> selector){
        for (PairType pair: selector){
            if (pair.getKey().equals(value)){
                return true;
            }
        }
        return false;
    }


    /**
     * QueryString param1=value1&param2=&param3=value3&param3″を Map
     * {param1=["value1"], param2=[null], param3=["value3", null]} に変更して返す
     *
     * @param query
     * @return
     * @throws UnsupportedEncodingException
     */
    public static Map<String, List<String>> parseQuery(String query) throws UnsupportedEncodingException {
        return parseQuery(query, "&", "=");
    }
    
    /**
     * 接続パラメータを指定したparseQuery
     * @param query 
     * @param amp 接続パラメータ1
     * @param equal 接続パラメータ2
     * @return
     * @throws UnsupportedEncodingException 
     */
    public static Map<String, List<String>> parseQuery(String query, String amp, String equal) throws UnsupportedEncodingException {            
        final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
        //final String[] pairs = query.split(amp);
        final String[] pairs = StringUtils.split(query, amp);
        for (String pair : pairs) {
            final int idx = pair.indexOf(equal);
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<String>());
            }
            final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }    

    /**
     * サーブレットのアウトプットを行う
     *
     * @param response
     * @param templatePath Velocityのテンプレートパス
     * @param assignMap アサインする変数
     */
    public static void servletOutput(HttpServletResponse response, String templatePath, Map<String, Object> assignMap) {
        PrintWriter writer = null;
        try {
            response.setContentType("text/html; charset=UTF-8");
            Map<String, Object> defaultContext = MacroUtils.defaultVelocityContext();
            assignMap.put("context", defaultContext);
            writer = response.getWriter();
            writer.write(VelocityUtils.getRenderedTemplate(templatePath, assignMap));
            writer.close();
        } catch (IOException ex) {

        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    public static void servletJson(HttpServletResponse response, Map<String, Object> assignedMap) {
        PrintWriter writer = null;
        try {
            response.setContentType("application/json; charset=UTF-8");
            writer = response.getWriter();
            //writer.print(JSON.encode(assignedMap)); // なぜかJSONICでPOJOがencodeできない
            Gson GSON = new Gson();
            writer.print(GSON.toJson(assignedMap));
        } catch (IOException ex) {

        } finally {
            IOUtils.closeQuietly(writer);
        }
    }
}
