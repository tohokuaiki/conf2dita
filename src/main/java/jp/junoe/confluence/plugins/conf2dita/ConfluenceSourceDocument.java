/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.junoe.confluence.plugins.Conf2dita;
import static jp.junoe.confluence.plugins.Conf2dita.CONF2DITA_PAGEID_ATTRIBUTE;
import static jp.junoe.confluence.plugins.Conf2dita.CONF2DITA_SPACEKEY_ATTRIBUTE;
import jp.junoe.confluence.plugins.JConfluenceHTTPUtil;
import jp.junoe.confluence.plugins.JConfluenceXSLTUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.QName;
import org.dom4j.XPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ConfluenceのソースをXMLとして扱いやすくするためのラッパー
 *
 * @author Takashi
 */
public final class ConfluenceSourceDocument implements Cloneable {

    /**
     * ConfluenceのページXMLをWrapするテンプレートXMLのパス。(src/main/resourcesからの相対パス)
     */
    private static final String CONFLUENCE_XML_ROOTTAG_TEMPLATE = "/templates/xml/confluence_base.xml";

    /**
     * ConfluenceのページXMLで使用するネームスペースの一覧 ただし、Defaultにはhtmlというネームスペースを割り振っている。
     * TODO このリストは document.getRootElement().declaredNamespaces(); で作るべき？
     */
    private static final Map<String, String> namespace = new HashMap<String, String>() {
        {
            put("html", "http://www.atlassian.com/schema/confluence/4/");
            put("ac", "http://www.atlassian.com/schema/confluence/4/ac/");
            put("ri", "http://www.atlassian.com/schema/confluence/4/ri/");
        }
    };

    private final Document document;

//    private final ContentPropertyManager cm;
//
//    private final ContentEntityObject page;
    /**
     * ページオブジェクトからそのソースをXMLに納めたものを返すConstructor(Confluence=>DITAへの変換で使用する)
     *
     * @param page ページ
     * @param topicType トピックタイプ
     * @param ditaId
     * @param cm
     * @throws org.dom4j.DocumentException
     * @throws java.io.IOException
     */
    public ConfluenceSourceDocument(ContentEntityObject page, String topicType, String ditaId, ContentPropertyManager cm) throws DocumentException, IOException {

//        this.cm = cm;
//        this.page = page;
        String pageXML = page.getBodyAsString();
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put("confluenceDTDPath", JConfluenceXSLTUtil.setConfluenceDTDFiles());
        map.put("body", pageXML);
        map.put("title", page.getDisplayTitle());
        map.put("pageId", ditaId);
        map.put("topictype", topicType);
        pageXML = VelocityUtils.getRenderedTemplate(CONFLUENCE_XML_ROOTTAG_TEMPLATE, map);
        pageXML = DitaXmlUtil.EntityFilter.convertChar2NumericEntity(pageXML);
        document = DocumentHelper.parseText(pageXML);

        // image要素のditaプロパティは、ac:queryparams属性にbase64エンコードされた状態で入っているのでXSLTしやすいように変換
        List images = this.selectNodes("//ac:image");

        Element image;
        // for (Object oi: images){
        for (int i = 0, j = images.size(); i < j; i++) {
            if (images.get(i) instanceof Element) {
                image = (Element) images.get(i);
                parseImageQueryParameter(image);
            }
        }

        // html-elementのclassに直接保存されているditaプロパティをditaPropertyidのように付け直す
        List elements = this.selectNodes("//html:tr|//html:td|//html:th|//html:li");
        Element element;
        for (int i = 0, j = elements.size(); i < j; i++) {
            if (elements.get(i) instanceof Element) {
                element = (Element) elements.get(i);
                parseBase64ClassAttribute(element);
            }
        }

    }

    /**
     * ConfluenceのXMLが存在する場合(DITA=>Confluenceへの変換で使用する)
     *
     * @param confxml
     * @throws org.dom4j.DocumentException
     */
    public ConfluenceSourceDocument(String confxml) throws DocumentException {
        document = DocumentHelper.parseText(confxml);
        // image要素のditaプロパティは、ac:queryparams属性にditaProperty=の形で居れる
        // ex) <ac:image ac:queryparams="ditaProperty=aWQ9YWFhfHNj...base64encoded...YXNzPXxrZXlyZWY9&amp;" ac:thumbnail="true" ac:width="300">
        for (Object image : selectNodes("//ac:image")) {
            if (image instanceof Element) {
                parseImageElementDitaProperty((Element) image);
            }
        }

        // HTMLのclassに直接入れるケース. ()内はBase64エンコードされているものとする。
        // ditaProperty(foo=1&bar=2&baz=3)
        for (Object element : selectNodes("//html:tr|//html:td|//html:th")) {
            if (element instanceof Element) {
                parseDitaAttribute2Base64Class((Element) element);
            }
        }

        // prologにあるconfluenceのPage IDとSpaceKeyの情報は削除
        removeConfluencePageInformationOtherMetaElement();
    }

    @Override
    @SuppressWarnings("CloneDeclaresCloneNotSupported")
    public ConfluenceSourceDocument clone() {
        try {
            return (ConfluenceSourceDocument) super.clone();
        } catch (CloneNotSupportedException ex) {
            // throw new InternalError(ex.getMessage());
            return null;
        }
    }

    /**
     * tablePropertyをマージしたものを出力
     *
     * @see Document#asXML() passthru
     * @return
     */
    public String asXML() {
        ConfluenceSourceDocument clone = this.clone();
        return clone.document.asXML();
        // return document.asXML();
    }

    /**
     * 生のXMLを出力
     *
     * @see ConfluenceSourceDocument#asXML()
     * @return
     */
    public String asRawXML() {
        return document.asXML();
    }

    /**
     * 名前空間付の場合はPrefixが必要なので別メソッドに切り出しの上、 Queryには必ずネームスペースPrefix付のものを使うこと。
     * (Default prefixは"html"。省略不可)
     *
     * @see #namespace
     * @param query
     * @return
     */
    public final List selectNodes(String query) {
        XPath xpath = document.createXPath(query);
        xpath.setNamespaceURIs(namespace);
        List nodes = xpath.selectNodes(document);
        return nodes;
    }

    /**
     * @see #selectNodes(java.lang.String)
     * @param query
     * @param node
     * @return
     */
    public List selectNodes(String query, Node node) {
        XPath xpath = node.createXPath(query);
        xpath.setNamespaceURIs(namespace);
        List nodes = xpath.selectNodes(node);
        return nodes;
    }

    private Attribute getAttribute(Element node, String prefix, String name) {
        Attribute attr = null;
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        if (!StringUtils.isEmpty(namespaceUrl)) {
            QName qname = QName.get(name, prefix, namespaceUrl);
            attr = node.attribute(qname);
        }
        return attr;
        // String queryparams = image.attributeValue(QName.get("queryparams", "ac", "http://www.atlassian.com/schema/confluence/4/ac/"));    
    }

    private void addAttribute(Element element, String prefix, String name, String value) {
        Attribute attr = null;
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        if (!StringUtils.isEmpty(namespaceUrl)) {
            QName qname = QName.get(name, prefix, namespaceUrl);
            element.addAttribute(qname, value);
        }
    }

    private void addParameter(Element element, String prefix, String name, String value) {
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        Element parameter = DocumentHelper.createElement(QName.get("parameter", prefix, namespaceUrl));
        addAttribute(parameter, prefix, "name", name);
        parameter.add(DocumentHelper.createText(value));
        element.add(parameter);
    }

    /**
     * Confluence => DITA. <br>
     * Conflueceのimageに付けたQueryParamから属性を付ける
     *
     * @param image
     */
    private void parseImageQueryParameter(Element image) {

        Attribute imageprop;
        imageprop = getAttribute(image, "ac", "queryparams");
        if (imageprop != null) {
            String queryparams = imageprop.getValue();
            // LoggerFactory.getLogger(this.getClass()).error(queryparams);
            try {
                for (Map.Entry<String, List<String>> pquery : JConfluenceHTTPUtil.parseQuery(queryparams).entrySet()) {
                    List<String> valueList = pquery.getValue();
                    String values = "";
                    if (pquery.getKey().equals(Conf2dita.DITA_PROPERTY_PREFIX) && valueList.size() > 0) {
                        values = valueList.get(0);
                        Map<String, List<String>> decodeQueryParams = Conf2dita.Base64.decodeQueryParams(values, "|", "=");
                        for (Map.Entry<String, List<String>> pquery1 : decodeQueryParams.entrySet()) {
                            List<String> valueList1 = pquery1.getValue();
                            String key_value = null;
                            if (valueList1.size() > 0) {
                                for (String _v1 : valueList1) {
                                    if (_v1 != null && _v1.length() > 0) {
                                        key_value = _v1;
                                    }
                                    break; // only to get first ditaProperty...
                                }
                                if (key_value != null) {
                                    // image.addAttribute(new QName(
                                    addParameter(image, "ac", pquery1.getKey(), key_value);
                                }
                            }
                        }
                    }
                }
                // remove queryparams
                image.remove(imageprop);
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    /**
     * DITA => Confluence. <br>
     * insert &gt;ac:parameter name="queryparams" value="*****"/&lt; <br>
     * value is encoded in base64 encoding
     *
     * @param image
     */
    private void parseImageElementDitaProperty(Element image) {

        Element param;
        Attribute acName;
        String propPrefix = Conf2dita.DITA_PROPERTY_PREFIX;

        Map<String, String> queryparams = new HashMap<String, String>();
        List imageParams = selectNodes("ac:parameter", image);
        for (int i = imageParams.size() - 1; i >= 0; i--) {
            Object _param = imageParams.get(i);
            if (_param instanceof Element) {
                param = (Element) _param;
                acName = getAttribute(param, "ac", "name");
                if (acName != null && acName.getValue().startsWith(propPrefix)) {
                    queryparams.put(acName.getValue().substring(propPrefix.length()), param.getText());
                    image.remove(param);
                }
            }
        }

        if (queryparams.size() > 0) {
            String encodeQueryParams = Conf2dita.Base64.encodeQueryParams(queryparams, "|", "=");
            addAttribute(image, "ac", "queryparams", Conf2dita.DITA_PROPERTY_PREFIX + "=" + encodeQueryParams);
        }
    }

    /**
     * Confluence => DITA. <br>
     * Convert Confluence HTML Element class to DITA Properties
     *
     * @param element
     */
    private void parseBase64ClassAttribute(Element element) {
        Attribute classAttr = getAttribute(element, "html", "class");
        if (classAttr == null) {
            classAttr = element.attribute("class");
        }

        List<String> htmlClass = new ArrayList<String>();

        if (classAttr != null) {
            final String[] classStrings = StringUtils.split(classAttr.getValue(), " ");
            for (String s : classStrings) {
                if (StringUtils.startsWith(s, Conf2dita.DITA_PROPERTY_PREFIX)) {
                    s = StringUtils.substringAfter(s, Conf2dita.DITA_PROPERTY_PREFIX);
                    Map<String, List<String>> decodeAttrs = Conf2dita.Base64.decodeQueryParams(s, "&", "=");
                    for (Map.Entry<String, List<String>> e : decodeAttrs.entrySet()) {
                        List<String> value = e.getValue();
                        if (value.size() > 0 && value.get(0) != null) {
                            // addAttribute(element, "", "dita-" + e.getKey(), value.get(0));
                            element.addAttribute(Conf2dita.DITA_PROPERTY_PREFIX + e.getKey(), value.get(0));
                        }
                    }
                } else {
                    htmlClass.add(s);
                }
            }
            element.addAttribute("class", StringUtils.join(htmlClass, " "));
        }
    }

    /**
     * DITA => Confluence. <br>
     * Convert an element Dita Property to Base64 encoded html class.
     *
     * @param element tr,th,td...
     */
    private void parseDitaAttribute2Base64Class(Element element) {

        String ditaPropertyClass = getBase64DitaProperties(element);

        if (ditaPropertyClass.length() > 0) {
            ditaPropertyClass = Conf2dita.DITA_PROPERTY_PREFIX + ditaPropertyClass;
        }

        if (ditaPropertyClass.length() > 0) {
            Attribute elementClass = element.attribute("class");
            if (elementClass != null && !StringUtils.isEmpty(elementClass.getValue())) {
                ditaPropertyClass = elementClass.getValue() + " " + ditaPropertyClass;
            }
            element.addAttribute("class", ditaPropertyClass);
        }
    }

    /**
     * ElementからHTMLに張り付けられたditaプロパティ(ditaPropertypropとか...)を<br>
     * まとめてBase64エンコーディングして返却する。また、それらのditaプロパティをelementから削除する。
     *
     * @param element
     * @return
     */
    private String getBase64DitaProperties(Element element) {

        Attribute attr;
        Map<String, String> ditaprops = new HashMap<String, String>();
        String propName;
        String ditaPropertyClass = "";

        Object _attr;
        List attributes = element.attributes();
        for (int i = attributes.size() - 1; i >= 0; i--) {
            _attr = attributes.get(i);
            if (_attr instanceof Attribute) {
                attr = (Attribute) _attr;
                propName = attr.getName();
                if (StringUtils.startsWith(propName, Conf2dita.DITA_PROPERTY_PREFIX)) {
                    ditaprops.put(StringUtils.substringAfter(propName, Conf2dita.DITA_PROPERTY_PREFIX), attr.getValue());
                    element.remove(attr);
                }
            }
        }

        if (ditaprops.size() > 0) {
            ditaPropertyClass = Conf2dita.Base64.encodeQueryParams(ditaprops, "&", "=");
        }

        return ditaPropertyClass;
    }

    /**
     * prologにあるconfluenceのPage IDとSpaceKeyの情報を削除する。<br>
     * これは前回吐き出した際のConfluenceのページIDとスペースキー
     */
    private void removeConfluencePageInformationOtherMetaElement() {
        List prologs = selectNodes("//ac:structured-macro[@ac:name=\"dita-prolog\"]");
        for (int p = prologs.size() - 1; p >= 0; p--) {
            Element prolog;
            Object _pr = prologs.get(p);
            if (_pr instanceof Element) {
                prolog = (Element) _pr;
                List othermetas = selectNodes("//ac:structured-macro[@ac:name=\"dita-othermeta\"]", prolog);
                for (int o = othermetas.size() - 1; o >= 0; o--) {
                    Element om;
                    Object _om = othermetas.get(o);
                    if (_om instanceof Element) {
                        boolean del_othermeta = false;
                        om = (Element) _om;
                        for (Object _pn : selectNodes("ac:parameter[@ac:name=\"name\"]", om)) {
                            Element pn;
                            if (_pn instanceof Element) {
                                pn = (Element) _pn;
                                if (pn.getText().equalsIgnoreCase(CONF2DITA_PAGEID_ATTRIBUTE)
                                        || pn.getText().equalsIgnoreCase(CONF2DITA_SPACEKEY_ATTRIBUTE)) {
                                    del_othermeta = true;
                                    break;
                                }
                            }
                        }
                        if (del_othermeta) {
                            Element omParent = om.getParent(); // ac:rich-text
                            if (omParent.remove(om)) {
                                if (selectNodes("*", omParent).size() == 0) {
                                    omParent.getParent().getParent().remove(omParent.getParent());
                                }
                            }
                        }
                    }
                }
                // remove prolog if no children exists.
                if (selectNodes("ac:rich-text-body/*", prolog).size() == 0) {
                    prolog.getParent().remove(prolog);
                }
            }
        }
    }
}
