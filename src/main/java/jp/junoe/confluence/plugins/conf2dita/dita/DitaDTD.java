/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import com.atlassian.confluence.util.velocity.VelocityUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.junoe.confluence.plugins.JConfluenceFileUtil;
import jp.junoe.confluence.plugins.conf2dita.DitaXmlUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentType;

/**
 * DitaDTDに関するClass<br>
 * DIDを変更する際に、File/Stringからの入力には、set***DTD()を使う。既にDocumentObjectになっているものについては、replaceTo*****DTDを使う。
 *
 * @author Takashi
 */
public class DitaDTD {
    /* ======= DTD list ========= */

    /**
     * version 1.2
     */
    private static final String[] v12files = {
        /* topictype of maptype */
        "dita/v1.2/os/dtd1.2/bookmap/dtd/bookmap.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/concept.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/topic.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/task.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/generalTask.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossary.dtd",
        
        /* other dtd */
        "dita/v1.2/os/dtd1.2/bookmap/dtd/bookmap.ent",
        "dita/v1.2/os/dtd1.2/bookmap/dtd/bookmap.mod",
        "dita/v1.2/os/dtd1.2/ditaval/dtd/ditaval.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningAssessment.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningAssessment.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningAssessment.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningBase.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningBase.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningBookmap.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningContent.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningContent.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningContent.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningDomain.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningDomain.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningInteractionBaseDomain.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningInteractionBaseDomain.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningMap.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningMapDomain.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningMapDomain.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningMetadataDomain.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningMetadataDomain.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningOverview.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningOverview.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningOverview.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningPlan.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningPlan.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningPlan.mod",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningSummary.dtd",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningSummary.ent",
        "dita/v1.2/os/dtd1.2/learning/dtd/learningSummary.mod",
        "dita/v1.2/os/dtd1.2/machineryIndustry/dtd/machineryTask.dtd",
        "dita/v1.2/os/dtd1.2/machineryIndustry/dtd/machineryTaskbodyConstraint.mod",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/classifyDomain.ent",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/classifyDomain.mod",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/classifyMap.dtd",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/subjectScheme.dtd",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/subjectScheme.ent",
        "dita/v1.2/os/dtd1.2/subjectScheme/dtd/subjectScheme.mod",
        "dita/v1.2/os/dtd1.2/xnal/dtd/xnalDomain.ent",
        "dita/v1.2/os/dtd1.2/xnal/dtd/xnalDomain.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/basemap.dtd",
        "dita/v1.2/os/dtd1.2/base/dtd/basetopic.dtd",
        "dita/v1.2/os/dtd1.2/base/dtd/commonElements.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/commonElements.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/delayResolutionDomain.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/delayResolutionDomain.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/hazardstatementDomain.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/hazardstatementDomain.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/highlightDomain.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/highlightDomain.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/indexingDomain.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/indexingDomain.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/map.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/mapGroup.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/mapGroup.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/metaDecl.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/tblDecl.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/topic.mod",
        "dita/v1.2/os/dtd1.2/base/dtd/topicDefn.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/utilitiesDomain.ent",
        "dita/v1.2/os/dtd1.2/base/dtd/utilitiesDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/abbreviateDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/abbreviateDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/concept.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/concept.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/ditabase.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossary.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossary.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossentry.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossentry.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossentry.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossgroup.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossgroup.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossgroup.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossrefDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/glossrefDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/map.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/programmingDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/programmingDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/reference.dtd",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/reference.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/reference.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/softwareDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/softwareDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/strictTaskbodyConstraint.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/task.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/task.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/taskreqDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/taskreqDomain.mod",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/uiDomain.ent",
        "dita/v1.2/os/dtd1.2/technicalContent/dtd/uiDomain.mod",
    };

    /**
     * バージョンによってDTDファイルリスト取得
     *
     * @param version
     */
    private static ArrayList getDTDList(String version) {
        ArrayList<String> files = new ArrayList<String>();
        if (version.equals("v1.2")) {
            files.addAll(Arrays.asList(v12files));
        }
        return files;
    }

    /**
     *
     * @param version DITAのバージョン（今のところ"v1.2"で固定）
     * @return セットしたDTDのルートDirectoryパス
     */
    public static String setDTDFiles(String version) {
        String temp_dir = JConfluenceFileUtil.getTemporaryDir();
        String resource_path = "dtd/dita";
        String filepath_dest = "";
        String dtdContent;
        ArrayList<String> files = getDTDList(version);
        Map<String, String> dummyMap = null;
        File dtdDestination;
        File dtdDestinationDir;
        PrintWriter writer = null;
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        String rootDir = temp_dir + "/dtd/" + DitaXmlUtil.OASIS_DOMAIN;
        for (String filepath : files) {
            filepath_dest = rootDir + "/" + filepath;
            filepath = "/" + resource_path + "/" + filepath.replace("dita/v1.2/os/dtd1.2/", "");
            dtdDestination = new File(filepath_dest);
            if (!dtdDestination.exists()) {
                dtdDestinationDir = dtdDestination.getParentFile();
                if (dtdDestinationDir.exists() || dtdDestination.getParentFile().mkdirs()) {
                    dtdContent = VelocityUtils.getRenderedTemplate(filepath, dummyMap);
                    try {
                        fileWriter = new FileWriter(dtdDestination);
                        bufferedWriter = new BufferedWriter(fileWriter);
                        writer = new PrintWriter(bufferedWriter);
                        writer.print(dtdContent);
                    } catch (IOException ex) {
                        Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        IOUtils.closeQuietly(writer);
                        IOUtils.closeQuietly(bufferedWriter);
                        IOUtils.closeQuietly(fileWriter);
                    }
                }
            }
        }
        return temp_dir + "/dtd";
    }

    /**
     * バージョン省略時のフォローアップ
     *
     * @throws java.io.IOException
     * @see #setDTDFiles(java.lang.String)
     * @return
     */
    public static String setDTDFiles() throws IOException {
        return setDTDFiles("v1.2");
    }

    /**
     * DitaXMLのDTDの場所がインターネット上だとValidateに時間が掛かるのでローカルに変更する
     *
     * @param doc
     * @return 
     * @throws java.io.IOException
     */
    public static Document replaceToLocalDTD(Document doc) throws IOException {
        String dtdDir = setDTDFiles();

        DocumentType docType = doc.getDocType();
        docType.setPublicID(null);
        String systemID = docType.getSystemID();
        if (systemID.startsWith("http://")) {
            systemID = systemID.replace("http://", "/");
        } else if (systemID.matches("^[a-zA-Z]+\\.dtd$")){ 
            // task.dtdみたいにDTD名称だけがSYSTEMIDにあった場合
            String topicDomain = "technicalContent";
            if (systemID.equalsIgnoreCase("bookmap.dtd")){
                topicDomain = "bookmap";
            }
            systemID = "/" + DitaXmlUtil.OASIS_DOMAIN + "/dita/v1.2/os/dtd1.2/"+topicDomain+"/dtd/" + systemID;
        }
        systemID = dtdDir + systemID;
        docType.setSystemID(systemID);
        doc.setDocType(docType);

        return doc;
    }

    public static String replaceToLocalDTD(String xml) throws IOException, Exception {
        Document doc = DitaDocumentHelper.parseTextWithoutValidate(xml);
        Document docR = replaceToLocalDTD(doc);
        return docR.asXML();
    }

    /**
     * XMLのDTDをPublicIDであるhttp://のものにする
     *
     * @param doc
     * @return
     * @throws java.io.IOException
     */
    public static Document replaceToPublicDTD(Document doc) throws IOException {
        String dtdDir = setDTDFiles();
        String topicType = doc.getRootElement().getQualifiedName();

        DocumentType docType = doc.getDocType();
        if (docType.getSystemID() == null) {
//            return; // 書き換え済み
        }
        String systemID = docType.getSystemID();
        String publicID = systemID.replace('\\', '/');
        dtdDir = dtdDir.replace('\\', '/');
        publicID = publicID.substring(publicID.indexOf(dtdDir) + dtdDir.length());
        publicID = "http:/" + publicID;
        // publicID = publicID.replace(dtdDir, "http:/");
        docType.setSystemID(publicID);
        docType.setPublicID(getDitaTopicTypePublicID(topicType));

        doc.setDocType(docType);
        return doc;
    }

    /**
     *
     * @see DitaDTD#replaceToPublicDTD(org.dom4j.Document) 
     * @param xml
     * @return
     * @throws java.lang.Exception
     */
    public static String replaceToPublicDTD(String xml) throws Exception {
        Document doc = DitaDocumentHelper.parseTextWithoutValidate(xml);
        Document docR = replaceToPublicDTD(doc);
        return docR.asXML();
    }

    /**
     * topicTypeにあったDocType上のPublicIDを返す
     *
     * @param topicType
     * @return
     */
    public static String getDitaTopicTypePublicID(String topicType) {
        return "-//OASIS//DTD DITA " + StringUtils.capitalize(topicType) + "//EN";
    }

    /**
     * topicTypeにあったDocType上のSystemIDを返す
     *
     * @param topicType
     * @param isLocalDTD
     * @return
     * @throws java.io.IOException
     */
    public static String getDitaTopicTypeSystemID(String topicType, boolean isLocalDTD) throws IOException {
        String systemID = "";
        if (isLocalDTD) {
            systemID = DitaDTD.setDTDFiles();
        } else {
            systemID = "http://" + DitaXmlUtil.OASIS_DOMAIN;
        }
        systemID += "/dita/v1.2/os/dtd1.2/technicalContent/dtd/" + topicType.toLowerCase() + ".dtd";
        return systemID;
    }

}
