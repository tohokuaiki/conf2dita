/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.servlet.editor.ajax;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.GeneralUtil;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluenceHTTPUtil;
import jp.junoe.confluence.plugins.JConfluenceUserUtil;
import jp.junoe.confluence.plugins.conf2dita.AbstractServlet;
import org.apache.commons.lang.StringUtils;


/**
 *
 * @author Takashi
 */
public class DitaPropertyController extends AbstractServlet {

    private static final long serialVersionUID = 1L;

    private static final String TEMPLATE_PATH = "/templates/servlet/editor/ajax/ditaproperty_controller.vm";

    private final ContentPropertyManager contentPropertyManager;

    private final BandanaManager bandanaManager;

    private final PageManager pageManager;

    private final PermissionManager permissionManager;
    
    /**
     *
     * @param pageManager
     * @param contentPropertyManager
     * @param permissionManager
     * @param bandanaManager
     */
    public DitaPropertyController(
            PageManager pageManager, ContentPropertyManager contentPropertyManager,
            PermissionManager permissionManager, BandanaManager bandanaManager
            ) {
        this.contentPropertyManager = contentPropertyManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.bandanaManager = bandanaManager;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> m = new HashMap<String, Object>();
        boolean result = false;
        String error = "";
        try {
            Page page = pageManager.getPage(getPageId(req));
            if (page == null) {
                result = true;
            } else {
                // get topic type
                String topicType = Conf2dita.getTopicType(page, contentPropertyManager);
                if (StringUtils.isEmpty(topicType)) {
                    result = true; // not dita page
                } else {
                    if (JConfluenceUserUtil.canEditPage(permissionManager, page)) {
                        String topic_id = req.getParameter("topic_id");
                        if (StringUtils.isEmpty(topic_id)) {
                            contentPropertyManager.removeProperty(page, Conf2dita.DITA_TOPIC_ID);
                        } else {
                            contentPropertyManager.setStringProperty(page, Conf2dita.DITA_TOPIC_ID, topic_id);
                        }
                        result = true;
                    } else {
                        error = GeneralUtil.getI18n().getText("no.permission");
                    }
                }
            }
        } catch (Exception e) {
            error = e.getMessage();
        }
        m.put("result", result);
        m.put("error", error);
        JConfluenceHTTPUtil.servletJson(resp, m);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Page page = pageManager.getPage(getPageId(req));
        if (JConfluenceUserUtil.canViewPage(permissionManager, page)) {
            Map<String, Object> map = new HashMap<String, Object>();
            String topic_id = contentPropertyManager.getStringProperty(page, Conf2dita.DITA_TOPIC_ID);
            if (topic_id == null) {
                topic_id = "";
            }
            map.put("topic_id", topic_id);
            JConfluenceHTTPUtil.servletOutput(resp, TEMPLATE_PATH, map);
        }
    }

}
