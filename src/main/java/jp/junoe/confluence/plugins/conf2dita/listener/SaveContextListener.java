/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.listener;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageEvent;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.event.events.content.page.async.PageCreatedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import org.springframework.beans.factory.DisposableBean;

/**
 *
 * @author Takashi
 */
public class SaveContextListener implements DisposableBean {

    /**
     * イベントパブリッシャ
     */
    protected EventPublisher eventPublisher;

    private final ContentPropertyManager cm;

    public SaveContextListener(EventPublisher eventPublisher, ContentPropertyManager cm) {
        this.eventPublisher = eventPublisher;
        this.cm = cm;
        eventPublisher.register(this);
    }

    /**
     * ページを更新した際に実行される
     *
     * @param event
     */
    @EventListener
    public void onUpdateContextHandler(PageUpdateEvent event) {
        processPageEditedEvent(event);
    }

    @EventListener
    public void onCreateContextHandler(PageCreateEvent event) {
        // on create
        processPageEditedEvent(event);
    }

    @EventListener
    public void onCreatedContextHandler(PageCreatedEvent event) {
    }

    @EventListener
    public void onRestorePageHandler(PageRestoreEvent event) {
    }

    /**
     * 実際の処理 TablePropertyのバージョン情報をシンクロさせる
     * @param event
     */
    private void processPageEditedEvent(PageEvent event) {
//        ContentEntityObject content = event.getContent();
    }

    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }
}
