/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.spring.container.ContainerManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.JAXB;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluencePageUtil;
import jp.junoe.confluence.plugins.conf2dita.event.DitaPageSettingEvent;
import org.slf4j.LoggerFactory;

/**
 * Zipファイル内の1ditaファイルを構成 自身のファイルパス、添付ファイル、親階層ファイル、子階層ファイルの情報を持っている
 *
 * @author Takashi
 */
public final class DitaFileSet {

    public static final String DITA_TOPIC_TYPE_GENERATOR_IMPORT = "Import";

    /**
     * Confluence側のスペースキー
     */
    private String spaceKey;

    /**
     * Confluence側のPageId
     */
    private long pageId = 0;

    /**
     * Zipが展開された後のファイルのパス
     */
    private File file = null;

    /**
     * 親PageId　0の場合はスペースのルート
     */
    private long parentPageId;

    /**
     * ページタイトル
     */
    private String pageTitle;

    /**
     * ページの順番
     */
    private int position;

    /**
     * 子ページ
     */
    private List<DitaFileSet> children = new ArrayList<DitaFileSet>();

    /**
     * 添付ファイル
     */
    private List<File> attachments = new ArrayList<File>();

    /**
     * DitaXMLを扱う際のDita Object
     */
    private TopicDocument dita;

    /**
     * nothing todo
     */
    public DitaFileSet() {
    }

    /**
     * ditafileを元にこのObjectの各種情報を作る
     *
     * @param file
     * @throws java.lang.Exception
     */
    public DitaFileSet(File file) throws Exception {
        this.file = file;
        // util objectの作成
        dita = new TopicDocument(file);
        // Dita情報のセット
        this.setSpaceKey(dita.readSpaceKey());
        setPageId(dita.readPageId());
        setPageTitle(dita.readTitle());
    }

    /**
     * コンストラクタ(ファイルが無いのでAttachmentは不明になる)
     *
     * @param ditaxml
     * @throws java.lang.Exception
     * @see DitaFileSet(File)
     */
    public DitaFileSet(String ditaxml) throws Exception {
        this.file = new File("");
        dita = new TopicDocument(ditaxml);
        // Dita情報のセット
        this.setSpaceKey(dita.readSpaceKey());
        setPageId(dita.readPageId());
        setPageTitle(dita.readTitle());
    }

    /**
     * Get the value of spaceKey
     *
     * @return the value of spaceKey
     */
    public String getSpaceKey() {
        return spaceKey;
    }

    /**
     * Set the value of spaceKey
     *
     * @param spaceKey new value of spaceKey
     */
    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    /**
     * Get the value of pageId
     *
     * @return the value of pageId
     */
    public long getPageId() {
        return pageId;
    }

    /**
     * Set the value of pageId
     *
     * @param pageId new value of pageId
     */
    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    /**
     * Get the value of parentPageId
     *
     * @return the value of parentPageId
     */
    public long getParentPageId() {
        return parentPageId;
    }

    /**
     * Set the value of parentPageId
     *
     * @param parentPageId new value of parentPageId
     */
    public void setParentPageId(long parentPageId) {
        this.parentPageId = parentPageId;
    }

    /**
     * Get the value of pageTitle
     *
     * @return the value of pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * Set the value of pageTitle
     *
     * @param pageTitle new value of pageTitle
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    /**
     * Get the value of children
     *
     * @return the value of children
     */
    public List<DitaFileSet> getChildren() {
        return children;
    }

    /**
     * Set the value of children
     *
     * @param children new value of children
     */
    public void setChildren(List<DitaFileSet> children) {
        this.children = children;
    }

    /**
     * Get the value of file
     *
     * @return the value of file
     */
    public File getFile() {
        return file;
    }

    /**
     * Set the value of file
     *
     * @param file new value of file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Get the value of attachments
     *
     * @return the value of attachments
     */
    public List<File> getAttachments() {
        return attachments;
    }

    /**
     * あるディレクトリがこのページに対する添付ファイルの集積場所だと決めつけて設定する。
     * この場合、実際にはDITAで使用していないファイルも取り込んでしまう可能性がある。
     *
     * @param attachmentsDir new value of attachments
     */
    public void setAttachments(File attachmentsDir) {

        if (attachmentsDir.isDirectory()) {
            for (File f : attachmentsDir.listFiles()) {
                if (f.isFile()) {
                    attachments.add(f);
                }
            }
        }
    }

    /**
     * このObjectに結び付けられたDitaFileをにして添付ファイルの取得・設定を行う。
     *
     * @throws java.io.FileNotFoundException
     */
    public void setAttachments() throws FileNotFoundException {
        List<String> images = dita.readImagesPath();
        File imageFile = null;
        for (String href : images) {
            try {
                imageFile = (new File(file.getParentFile().getAbsolutePath() + File.separator + href)).getCanonicalFile();
            } catch (IOException ex) {
                imageFile = null;
            }
            if (imageFile != null) {
                if (imageFile.isFile()) {
                    if (attachments.indexOf(imageFile) < 0) {
                        attachments.add(imageFile);
                    }
                } else {
                    String message = GeneralUtil.getI18n().getText("conf2dita.action.import.error.filenotfound.inzip",
                            new String[]{imageFile.getName()});
                    throw new FileNotFoundException(message);
                }
            }
        }
    }

    /**
     * reset attachment property
     */
    public void clearAttachments() {
        attachments = new ArrayList<File>();
    }

    /**
     * このObjectをConfluenceに取り込む
     *
     * @param pageManager
     * @param labelManager
     * @param bandanaManager
     * @param contentPropertyManager
     * @param spaceImportTo
     * @param parentPage
     * @param user
     * @param importComment
     * @throws Exception
     */
    public void importToPage(
            PageManager pageManager, LabelManager labelManager, BandanaManager bandanaManager, ContentPropertyManager contentPropertyManager,
            Space spaceImportTo, Page parentPage, ConfluenceUser user,
            String importComment)
            throws Exception {

        Page originalPage = null;
        Page targetPage = null;
        boolean isNewPage;

        // Get variables from file
        String confXml = "";
        String topicType = null;
        String topicId = null;

        File f = this.getFile();
        if (f != null && f.isFile()) {
            // Ditaから情報取得
            ConfluenceSourceDocument confDoc = Conf2dita.d2c(f);
            // TableProperty情報を取得＆埋め込み
            confXml = confDoc.asRawXML();
//            LoggerFactory.getLogger(Conf2dita.class).error(confXml);
            topicType = (new TopicDocument(f)).readTopicType();
            topicId = (new TopicDocument(f)).readTopicId();
        }

        // タイトルは空白が不許可
        if (pageTitle.equals("")) {
            pageTitle = getRandomTitle();
        }

        // targetPageを取得する
        targetPage = JConfluencePageUtil.findTargetPage(pageId, pageTitle, pageManager, spaceImportTo);
        isNewPage = (targetPage == null);
        if (targetPage == null) {
            targetPage = new Page();
            targetPage.setSpace(spaceImportTo);
            targetPage.setPosition(position);
        } else {
            originalPage = (Page) targetPage.clone();
        }
        targetPage.setLastModificationDate(new Date());
        targetPage.setLastModifier(user);
        targetPage.setBodyAsString(confXml);
        targetPage.setTitle(pageTitle);
        targetPage.setParentPage(parentPage);
        targetPage.setVersionComment(importComment);

        DefaultSaveContext saveContext = new DefaultSaveContext(false, true, false);
        if (isNewPage) {
            pageManager.saveContentEntity(targetPage, saveContext);
        } else {
            pageManager.saveContentEntity(targetPage, originalPage, saveContext);
        }
        parentPage.addChild(targetPage);

        // TopicId情報を保存
        if (topicId != null) {
            contentPropertyManager.setStringProperty(targetPage, Conf2dita.DITA_TOPIC_ID, topicId);
        }

        // ページにTOPICTYPEを付けたイベントをPublish
        if (topicType != null) {
            EventPublisher eventPublisher = (EventPublisher) ContainerManager.getInstance().getContainerContext().getComponent("eventPublisher");
            eventPublisher.publish(new DitaPageSettingEvent(this, targetPage, topicType, DITA_TOPIC_TYPE_GENERATOR_IMPORT));
            // Event代わりにConf2dita.DITA_TOPIC_TYPE_LABEL_IMPORTED_PREFIXの付いたラベルを付ける
            labelManager.addLabel(targetPage, new Label(Conf2dita.DITA_TOPIC_TYPE_LABEL_IMPORTED_PREFIX + topicType));
        }

        // 添付ファイル
        for (File atf : getAttachments()) {
            JConfluencePageUtil.attachFile(
                    pageManager.getAttachmentManager(), targetPage,
                    atf.getAbsolutePath(),
                    atf.getName(), importComment);
        }
        // 子ページ

        for (DitaFileSet child : getChildren()) {
            child.importToPage(
                    pageManager, labelManager, bandanaManager, contentPropertyManager,
                    spaceImportTo, targetPage, user, importComment);
            // child.importToPage(spaceImportTo, pageManager, labelManager, targetPage, user, importComment);
        }
    }

    /**
     * タイトルが無かった時の様に適当なページタイトルを付ける
     *
     * @return
     */
    private String getRandomTitle() {
        return "Temporary title : " + (new Date()).toString() + " : " + Math.floor(Math.random() * 1000000);
    }

}
