/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.junoe.confluence.plugins.Conf2dita;
import org.apache.tools.zip.ZipOutputStream;
import jp.junoe.confluence.plugins.JConfluenceFileUtil;
import jp.junoe.confluence.plugins.JConfluencePageUtil;
import jp.junoe.confluence.plugins.JZipFileUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTDValidateErrorException;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaMapErrorException;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;
import jp.junoe.confluence.plugins.conf2dita.dita.MapDocument;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tools.zip.ZipEntry;
import org.slf4j.LoggerFactory;

/**
 * ConfluenceからDitaにExportする際のUtilクラス
 *
 * @author Takashi
 */
public class DitaExportUtils {
    
    public static final String EXPORT_NESTOPTION_NEST = "nest";
    
    public static final String EXPORT_NESTOPTION_FLAT = "flat";

    private static final String exportDitaFilePrefix = "ditaexport";

    private final Page rootPage;

    private final ContentPropertyManager contentPropertyManager;

    private final PageManager pageManager;

    private final I18NBean i18n;

    /**
     * ルートになるページをスキップするかどうか。スペースのHomePageからExportした場合でそのHomeがDITAではない場合にスキップする・・など
     */
    private final boolean skipRoot;

    byte[] buf = new byte[1024];

    /**
     * options
     */
    private String attachmentDir = "attachments";

    /**
     * Constructor
     *
     * @param page
     * @param cpm
     * @param pm
     * @param sRoot ルートファイルをスキップするかどうか
     */
    public DitaExportUtils(Page page, ContentPropertyManager cpm, PageManager pm, boolean sRoot) {
        rootPage = page;
        contentPropertyManager = cpm;
        pageManager = pm;
        skipRoot = sRoot;
        i18n = GeneralUtil.getI18n();
    }

    /**
     * Set the value of attachmentDir
     *
     * @param attachmentDir new value of attachmentDir
     */
    public void setAttachmentDir(String attachmentDir) {
        this.attachmentDir = attachmentDir;
    }

    /**
     * DITAファイルをどう格納するかのオプション
     */
    private String nestOption = EXPORT_NESTOPTION_FLAT;

    /**
     * Set the value of nestOption
     *
     * @return 
     */
    public String getNestOption() {
        return nestOption;
    }

    /**
     * Set the value of nestOption
     *
     * @param nestOption new value of nestOption
     */
    public void setNestOption(String nestOption) {
        this.nestOption = nestOption;
    }

    /**
     * 添付ファイルをどこに格納するかのオプション
     */
    private String attachmentPositionOption;

    /**
     * Set the value of attachmentPositionOption
     *
     * @param apOption new value of attachmentPositionOption
     */
    public void setAttachmentPositionOption(String apOption) {
        this.attachmentPositionOption = apOption;
    }

    /**
     * 子ページも出力するかのオプション
     */
    private boolean includeChildren;

    /**
     * Set the value of includeChildren
     *
     * @param includeChildren new value of includeChildren
     */
    public void setIncludeChildren(boolean includeChildren) {
        this.includeChildren = includeChildren;
    }

    /**
     * テンポラリ領域にDITAをExportしたZIPファイルを作成する
     *
     * @param pages
     * @param encoding
     * @return
     * @throws java.lang.Exception
     */
    public File createZip(List<Page> pages, String encoding) throws Exception {

        if (pages.isEmpty()) {
            throw new Exception("no pages index");
        }

        Page topPage = pages.get(0);
        String zipPath = JConfluenceFileUtil.getTemporaryDir() + "/"
                + JConfluenceFileUtil.getTemporaryFileName(topPage, exportDitaFilePrefix);

        File zipFile = new File(zipPath);
        ZipOutputStream zos = new ZipOutputStream(zipFile);
        zos.setEncoding(encoding);
        // append
        List<String> exportErrors = new ArrayList<String>();
        String storeError;
        for (Page page : pages) {
            storeError = storePageZipEntry(page, zos);
            if (!StringUtils.isEmpty(storeError)) {
                exportErrors.add(storeError);
            }
        }

        // store ditamap
        try {
            storeDitaMapZipEntry(zos);
        } catch (DitaMapErrorException ex) {
            exportErrors.add(ex.getMessage());
        } catch (IOException ex) {
            exportErrors.add(ex.getMessage());
        }

        // add error file
        if (exportErrors.size() > 0) {
            String errorMessages = StringUtils.join(exportErrors, "\n");
            JZipFileUtil.appendStringEntry(zos, "export_errors.txt", errorMessages);
        }
        IOUtils.closeQuietly(zos);

        return zipFile;
    }

    /**
     * あるページのZipEntryをZipOutputStreamに書き出す
     *
     * @param page
     * @return
     */
    private String storePageZipEntry(Page page, ZipOutputStream zos) {

        ZipEntry entry = null;
        BufferedInputStream is = null;
        TopicDocument ditaDoc = null;
        String ditaXML = null;
        String errorMessage = "";
        ByteArrayInputStream bais = null;

        // path
        String fPath = getPageZipEntryPath(page);
        String topicType = Conf2dita.getTopicType(page, contentPropertyManager);
        if (topicType.isEmpty()) {
            return ""; // nothing to do
        }
        if (!DitaTopicType.isSupported(topicType)) {
            errorMessage = fPath + ":\n" + i18n.getText("conf2dita.error.dita.notsupportedtopictype", new String[]{topicType});
            return errorMessage;
        }
        try {
            try {
                ditaDoc = Conf2dita.c2d(contentPropertyManager, page, topicType);
                fixAttachmentHref(ditaDoc, page);
                ditaDoc.validateDTD();
                ditaDoc.setPublicDTD();
                ditaXML = ditaDoc.asXML();
            } catch (DitaDTDValidateErrorException ex) {
                ditaXML = ex.getDTDErrorMessage(true, false);
                errorMessage = fPath + ":\n" + ex.getDTDErrorMessage(true, false);
            } catch (Exception ex) {
                ditaXML = ex.getMessage();
                errorMessage = fPath + ":\n" + ex.getMessage();
            }
            JZipFileUtil.appendStringEntry(zos, fPath, ditaXML);
            storeAttachmentsZipEntry(page, zos);
        } catch (IOException ex) {
            Logger.getLogger(DitaExportUtils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            IOUtils.closeQuietly(bais);
            IOUtils.closeQuietly(is);
        }

        return errorMessage;
    }

    /**
     * ページのAttachmentをZipOutputStreamに書き出す
     *
     * @param page
     * @param zos
     */
    private void storeAttachmentsZipEntry(Page page, ZipOutputStream zos) {

        List<Attachment> attachments = page.getAttachments();
        for (Attachment a : attachments) {
            if (a.getFileSize() >= 0) {
                if (a.isLatestVersion()) {
                    // path
                    String aPath = getAttachmentZipEntryPath(page, a);
                    try {
                        JZipFileUtil.appendAttachmentEntry(zos, aPath, a);
                    } catch (IOException ex) {
                        LoggerFactory.getLogger(this.getClass()).error(ex.getMessage());
                    }
                }
            }
        }
    }

    /**
     * ditaの添付で使われているパスをAttachmentのPathと合わせるようにする
     *
     * @param dita
     * @param page
     */
    private void fixAttachmentHref(TopicDocument dita, Page page) {
        String pageDir = getPageZipEntryDir(page);
        String attachDir = getAttachmentZipEntryDir(page);

        int index = StringUtils.indexOfDifference(pageDir, attachDir);
        if (index > 0) {
            List<String> syncDirs = JConfluenceFileUtil.syncDirectoryPath(pageDir, attachDir);
            if (syncDirs.size() == 2) {
                pageDir = syncDirs.get(0);
                attachDir = syncDirs.get(1);
            }
        }
        // 同じ場合は何もすることがない
        if (StringUtils.isEmpty(attachDir) && StringUtils.isEmpty(pageDir)) {
            return;
        }

        // さかのぼるディレクトリ
        String upDirectory = "";
        for (String split : StringUtils.split(pageDir, "/")) {
            upDirectory += "../";
        }

        dita.fixHrefPath(upDirectory, attachDir);
    }

    /**
     * ページのZIPエントリーパスを返す
     *
     * @param page
     * @return
     */
    public String getPageZipEntryPath(Page page) {
        String fpath = "";
        String dir = getPageZipEntryDir(page);
        String ditaId;
        if (!StringUtils.isEmpty(dir)) {
            fpath += dir + "/";
        }

        ditaId = Conf2dita.getTopicId(page, contentPropertyManager);
        fpath += ditaId + DitaXmlUtil.DITA_FILE_EXTENSION;

        return fpath;

    }

    /**
     * ページのZIPエントリーディレクトリを返す
     *
     * @param page
     * @return
     */
    private String getPageZipEntryDir(Page page) {
        String dir = "";

        if (StringUtils.equals(nestOption, EXPORT_NESTOPTION_NEST)) {
            String[] ids = JConfluencePageUtil.getAncestorIds(page);
            String[] rootIds = JConfluencePageUtil.getAncestorIds(rootPage);
            for (String _rootId : rootIds) {
                ids = ArrayUtils.removeElement(ids, _rootId);
            }
            if (skipRoot) {
                ids = ArrayUtils.removeElement(ids, rootPage.getId());
            }

            // change to pageid => dita topicid
            List<String> topicIds = new ArrayList<String>();
            for (String _id : ids) {
                topicIds.add(Conf2dita.getTopicId(pageManager.getPage(Long.parseLong(_id)), contentPropertyManager));
            }
            dir = StringUtils.join(topicIds, "/");
        }

        return dir;
    }

    private String getAttachmentZipEntryDir(Page page) {

        String dir = "";
        String ditaDir = getPageZipEntryDir(page);

        if (attachmentPositionOption.equals("separatedir")) { // ルートのAttachmentsフォルダ内
            dir = attachmentDir + "/" + ditaDir;
            if (!ditaDir.equals("")) {
                dir += "/";
            }
            dir += Conf2dita.getTopicId(page, contentPropertyManager);
        } else if (attachmentPositionOption.equals("withpage")) { // DITAファイルと同じ場所
            dir = ditaDir;
        } else if (attachmentPositionOption.equals("withpagedir")) { // DITAファイルと同じ場所のattachmentsフォルダ内
            if (!ditaDir.equals("")) {
                ditaDir += "/";
            }
            dir = ditaDir + attachmentDir;
        }

        return dir;
    }

    /**
     *
     * @param page
     * @return
     */
    private String getAttachmentZipEntryPath(Page page, Attachment attachment) {
        String path = "";
        String dir = getAttachmentZipEntryDir(page);
        if (!StringUtils.isEmpty(dir)) {
            path += dir + "/";
        }

//        String filename = Long.toString(attachment.getId());
//        path += filename + "." + attachment.getFileExtension();
        path += attachment.getTitle();

        return path;
    }

    /**
     * store ditamap to zip
     *
     * @param zos
     */
    private void storeDitaMapZipEntry(ZipOutputStream zos) throws DitaMapErrorException, IOException {
        String ditaMapXML = "";
        MapDocument ditamap;
        ditamap = MapDocument.create(this, rootPage, "map", contentPropertyManager);
        ditaMapXML = ditamap.asXML(true);

        JZipFileUtil.appendStringEntry(zos, "conf2dita" + DitaXmlUtil.DITAMAP_FILE_EXTENSION, ditaMapXML);
    }
}
