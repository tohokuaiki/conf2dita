/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;

/**
 *
 * @author Takashi
 */
@XmlRootElement(name = "ditaproperty")
@XmlAccessorType(XmlAccessType.FIELD)
public class DitaPropertyModel {

    @XmlElement(name = "topictype")
    private String topicType;

    @XmlElement(name = "topictypeGenerator")
    private String topicTypeGenerator;

    @XmlElement(name = "topicid")
    private String topicId;

    public DitaPropertyModel() {
    }

    public DitaPropertyModel(DitaTopicType ditaTopicType, String topicId) {
        this.topicType = ditaTopicType.getTopicType();
        this.topicTypeGenerator = ditaTopicType.getGenerator();
        this.topicId = topicId;
    }
}
