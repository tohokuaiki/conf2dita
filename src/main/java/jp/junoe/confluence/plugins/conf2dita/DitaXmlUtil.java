/**
 * DITAXMLを操作する際に使用する
 */
package jp.junoe.confluence.plugins.conf2dita;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * DITAの色々
 *
 * @author Takashi
 */
public class DitaXmlUtil {

    static public final String OASIS_DOMAIN = "docs.oasis-open.org";

    static public final String DITA_FILE_EXTENSION = ".dita";

    static public final String DITAMAP_FILE_EXTENSION = ".ditamap";

    static public final String DITA_VERSION = "v1.2";

    private static final Map<String, String> topic2body = new HashMap<String, String>() {
        {
            put("topic", "body");
            put("concept", "conbody");
            put("task", "taskbody");
            put("reference", "refbody");
            put("generaltask", "taskbody");
        }
    };

    /**
     * トピックタイプからBodyのタグ名を取得
     *
     * @param topicType
     * @return
     */
    public static String getTopicBodyQName(String topicType) {
        for (Map.Entry<String, String> e : topic2body.entrySet()) {
            if (e.getKey().equals(topicType)) {
                return e.getValue();
            }
        }
        return "";
    }

    /**
     * ditaとXHTMLのエンティティの違いを埋めるフィルタを提供。相互に変換可能
     */
    public static class EntityFilter {

        /**
         * Confluenceでは文字実体参照(Character entity references )が使われるので、数値文字参照(16進数
         * 16 based Numeric character reference)に変更
         */
        public static final Map<String, String> characterNumericEntityMap = new HashMap<String, String>() {
            {
                put("&nbsp;", " ");
                put("&iexcl;", "&#xA1;");
                put("&cent;", "&#xA2;");
                put("&pound;", "&#xA3;");
                put("&curren;", "&#xA4;");
                put("&yen;", "&#xA5;");
                put("&brvbar;", "&#xA6;");
                put("&sect;", "&#xA7;");
                put("&uml;", "&#xA8;");
                put("&copy;", "&#xA9;");
                put("&ordf;", "&#xAA;");
                put("&laquo;", "&#xAB;");
                put("&not;", "&#xAC;");
                put("&shy;", "&#xAD;");
                put("&reg;", "&#xAE;");
                put("&macr;", "&#xAF;");
                put("&deg;", "&#xB0;");
                put("&plusmn;", "&#xB1;");
                put("&sup2;", "&#xB2;");
                put("&sup3;", "&#xB3;");
                put("&acute;", "&#xB4;");
                put("&micro;", "&#xB5;");
                put("&para;", "&#xB6;");
                put("&middot;", "&#xB7;");
                put("&cedil;", "&#xB8;");
                put("&sup1;", "&#xB9;");
                put("&ordm;", "&#xBA;");
                put("&raquo;", "&#xBB;");
                put("&frac14;", "&#xBC;");
                put("&frac12;", "&#xBD;");
                put("&frac34;", "&#xBE;");
                put("&iquest;", "&#xBF;");
                put("&Agrave;", "&#xC0;");
                put("&Aacute;", "&#xC1;");
                put("&Acirc;", "&#xC2;");
                put("&Atilde;", "&#xC3;");
                put("&Auml;", "&#xC4;");
                put("&Aring;", "&#xC5;");
                put("&AElig;", "&#xC6;");
                put("&Ccedil;", "&#xC7;");
                put("&Egrave;", "&#xC8;");
                put("&Eacute;", "&#xC9;");
                put("&Ecirc;", "&#xCA;");
                put("&Euml;", "&#xCB;");
                put("&Igrave;", "&#xCC;");
                put("&Iacute;", "&#xCD;");
                put("&Icirc;", "&#xCE;");
                put("&Iuml;", "&#xCF;");
                put("&ETH;", "&#xD0;");
                put("&Ntilde;", "&#xD1;");
                put("&Ograve;", "&#xD2;");
                put("&Oacute;", "&#xD3;");
                put("&Ocirc;", "&#xD4;");
                put("&Otilde;", "&#xD5;");
                put("&Ouml;", "&#xD6;");
                put("&times;", "&#xD7;");
                put("&Oslash;", "&#xD8;");
                put("&Ugrave;", "&#xD9;");
                put("&Uacute;", "&#xDA;");
                put("&Ucirc;", "&#xDB;");
                put("&Uuml;", "&#xDC;");
                put("&Yacute;", "&#xDD;");
                put("&THORN;", "&#xDE;");
                put("&szlig;", "&#xDF;");
                put("&agrave;", "&#xE0;");
                put("&aacute;", "&#xE1;");
                put("&acirc;", "&#xE2;");
                put("&atilde;", "&#xE3;");
                put("&auml;", "&#xE4;");
                put("&aring;", "&#xE5;");
                put("&aelig;", "&#xE6;");
                put("&ccedil;", "&#xE7;");
                put("&egrave;", "&#xE8;");
                put("&eacute;", "&#xE9;");
                put("&ecirc;", "&#xEA;");
                put("&euml;", "&#xEB;");
                put("&igrave;", "&#xEC;");
                put("&iacute;", "&#xED;");
                put("&icirc;", "&#xEE;");
                put("&iuml;", "&#xEF;");
                put("&eth;", "&#xF0;");
                put("&ntilde;", "&#xF1;");
                put("&ograve;", "&#xF2;");
                put("&oacute;", "&#xF3;");
                put("&ocirc;", "&#xF4;");
                put("&otilde;", "&#xF5;");
                put("&ouml;", "&#xF6;");
                put("&divide;", "&#xF7;");
                put("&oslash;", "&#xF8;");
                put("&ugrave;", "&#xF9;");
                put("&uacute;", "&#xFA;");
                put("&ucirc;", "&#xFB;");
                put("&uuml;", "&#xFC;");
                put("&yacute;", "&#xFD;");
                put("&thorn;", "&#xFE;");
                put("&yuml;", "&#xFF;");
                put("&fnof;", "&#x192;");
                put("&Alpha;", "&#x391;");
                put("&Beta;", "&#x392;");
                put("&Gamma;", "&#x393;");
                put("&Delta;", "&#x394;");
                put("&Epsilon;", "&#x395;");
                put("&Zeta;", "&#x396;");
                put("&Eta;", "&#x397;");
                put("&Theta;", "&#x398;");
                put("&Iota;", "&#x399;");
                put("&Kappa;", "&#x39A;");
                put("&Lambda;", "&#x39B;");
                put("&Mu;", "&#x39C;");
                put("&Nu;", "&#x39D;");
                put("&Xi;", "&#x39E;");
                put("&Omicron;", "&#x39F;");
                put("&Pi;", "&#x3A0;");
                put("&Rho;", "&#x3A1;");
                put("&Sigma;", "&#x3A3;");
                put("&Tau;", "&#x3A4;");
                put("&Upsilon;", "&#x3A5;");
                put("&Phi;", "&#x3A6;");
                put("&Chi;", "&#x3A7;");
                put("&Psi;", "&#x3A8;");
                put("&Omega;", "&#x3A9;");
                put("&alpha;", "&#x3B1;");
                put("&beta;", "&#x3B2;");
                put("&gamma;", "&#x3B3;");
                put("&delta;", "&#x3B4;");
                put("&epsilon;", "&#x3B5;");
                put("&zeta;", "&#x3B6;");
                put("&eta;", "&#x3B7;");
                put("&theta;", "&#x3B8;");
                put("&iota;", "&#x3B9;");
                put("&kappa;", "&#x3BA;");
                put("&lambda;", "&#x3BB;");
                put("&mu;", "&#x3BC;");
                put("&nu;", "&#x3BD;");
                put("&xi;", "&#x3BE;");
                put("&omicron;", "&#x3BF;");
                put("&pi;", "&#x3C0;");
                put("&rho;", "&#x3C1;");
                put("&sigmaf;", "&#x3C2;");
                put("&sigma;", "&#x3C3;");
                put("&tau;", "&#x3C4;");
                put("&upsilon;", "&#x3C5;");
                put("&phi;", "&#x3C6;");
                put("&chi;", "&#x3C7;");
                put("&psi;", "&#x3C8;");
                put("&omega;", "&#x3C9;");
                put("&thetasym;", "&#x3D1;");
                put("&upsih;", "&#x3D2;");
                put("&piv;", "&#x3D6;");
                put("&bull;", "&#x2022;");
                put("&hellip;", "&#x2026;");
                put("&prime;", "&#x2032;");
                put("&Prime;", "&#x2033;");
                put("&oline;", "&#x203E;");
                put("&frasl;", "&#x2044;");
                put("&weierp;", "&#x2118;");
                put("&image;", "&#x2111;");
                put("&real;", "&#x211C;");
                put("&trade;", "&#x2122;");
                put("&alefsym;", "&#x2135;");
                put("&larr;", "&#x2190;");
                put("&uarr;", "&#x2191;");
                put("&rarr;", "&#x2192;");
                put("&darr;", "&#x2193;");
                put("&harr;", "&#x2194;");
                put("&crarr;", "&#x21B5;");
                put("&lArr;", "&#x21D0;");
                put("&uArr;", "&#x21D1;");
                put("&rArr;", "&#x21D2;");
                put("&dArr;", "&#x21D3;");
                put("&hArr;", "&#x21D4;");
                put("&forall;", "&#x2200;");
                put("&part;", "&#x2202;");
                put("&exist;", "&#x2203;");
                put("&empty;", "&#x2205;");
                put("&nabla;", "&#x2207;");
                put("&isin;", "&#x2208;");
                put("&notin;", "&#x2209;");
                put("&ni;", "&#x220B;");
                put("&prod;", "&#x220F;");
                put("&sum;", "&#x2211;");
                put("&minus;", "&#x2212;");
                put("&lowast;", "&#x2217;");
                put("&radic;", "&#x221A;");
                put("&prop;", "&#x221D;");
                put("&infin;", "&#x221E;");
                put("&ang;", "&#x2220;");
                put("&and;", "&#x2227;");
                put("&or;", "&#x2228;");
                put("&cap;", "&#x2229;");
                put("&cup;", "&#x222A;");
                put("&int;", "&#x222B;");
                put("&there4;", "&#x2234;");
                put("&sim;", "&#x223C;");
                put("&cong;", "&#x2245;");
                put("&asymp;", "&#x2248;");
                put("&ne;", "&#x2260;");
                put("&equiv;", "&#x2261;");
                put("&le;", "&#x2264;");
                put("&ge;", "&#x2265;");
                put("&sub;", "&#x2282;");
                put("&sup;", "&#x2283;");
                put("&nsub;", "&#x2284;");
                put("&sube;", "&#x2286;");
                put("&supe;", "&#x2287;");
                put("&oplus;", "&#x2295;");
                put("&otimes;", "&#x2297;");
                put("&perp;", "&#x22A5;");
                put("&sdot;", "&#x22C5;");
                put("&lceil;", "&#x2308;");
                put("&rceil;", "&#x2309;");
                put("&lfloor;", "&#x230A;");
                put("&rfloor;", "&#x230B;");
                put("&lang;", "&#x2329;");
                put("&rang;", "&#x232A;");
                put("&loz;", "&#x25CA;");
                put("&spades;", "&#x2660;");
                put("&clubs;", "&#x2663;");
                put("&hearts;", "&#x2665;");
                put("&diams;", "&#x2666;");
                put("&quot;", "&#x22;");
//                put("&amp;", "&#x26;");
//                put("&lt;", "&#x3C;");
//                put("&gt;", "&#x3E;");
                put("&OElig;", "&#x152;");
                put("&oelig;", "&#x153;");
                put("&Scaron;", "&#x160;");
                put("&scaron;", "&#x161;");
                put("&Yuml;", "&#x178;");
                put("&circ;", "&#x2C6;");
                put("&tilde;", "&#x2DC;");
                put("&ensp;", "&#x2002;");
                put("&emsp;", "&#x2003;");
                put("&thinsp;", "&#x2009;");
                put("&zwnj;", "&#x200C;");
                put("&zwj;", "&#x200D;");
                put("&lrm;", "&#x200E;");
                put("&rlm;", "&#x200F;");
                put("&ndash;", "&#x2013;");
                put("&mdash;", "&#x2014;");
                put("&lsquo;", "&#x2018;");
                put("&rsquo;", "&#x2019;");
                put("&sbquo;", "&#x201A;");
                put("&ldquo;", "&#x201C;");
                put("&rdquo;", "&#x201D;");
                put("&bdquo;", "&#x201E;");
                put("&dagger;", "&#x2020;");
                put("&Dagger;", "&#x2021;");
                put("&permil;", "&#x2030;");
                put("&lsaquo;", "&#x2039;");
                put("&rsaquo;", "&#x203A;");
                put("&euro;", "&#x20AC;");
            }
        };

        /**
         * HTMLエンティティで文字参照にするものを列挙
         */
        public static final Map<String, String> charcterEntityReference = new HashMap<String, String>() {
            {
                put("&", "&amp;");
                put("\"", "&quot;");
                put("'", "&apos;");
                put(">", "&gt;");
                put("<", "&lt;");
            }
        };

        /**
         * From 文字実態参照 to 数値文字参照
         *
         * @param text
         * @return
         */
        public static String convertChar2NumericEntity(String text) {

            for (Map.Entry<String, String> e : characterNumericEntityMap.entrySet()) {
                text = text.replace(e.getKey(), e.getValue());
            }
            return text;
        }

        public static String convertCharacterEntityReference(String text) {
            for (Map.Entry<String, String> e : charcterEntityReference.entrySet()) {
                text = text.replace(e.getKey(), e.getValue());
            }
            return text;
        }
    }

    public static class RegExp {

        /**
         * attachment directory available 
         */
        public static Pattern attachmentDir = Pattern.compile("^[\\w]+[\\w-\\.]*$");
        
    }
}
