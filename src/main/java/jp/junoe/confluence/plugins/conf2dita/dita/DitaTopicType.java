/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.conf2dita.exception.TopicTypeNotSupportException;
import org.apache.commons.lang.StringUtils;

/**
 * About dita topic type information using in this plugin.\n dita topictype is
 * saved in contentProperty.
 *
 * @author Takashi
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "root")
public class DitaTopicType {

    /**
     * supported topictype
     */
    private static List supportedTopicTypes = new ArrayList<String>() {
        {
            add("topic");
            add("concept");
            add("task");
            // add("generaltask");
        }
    };
    
    @XmlTransient
    private ContentPropertyManager cm;
    
    @XmlTransient
    private ContentEntityObject page;

    /**
     * topictypeが入る
     */
    @XmlElement(name = "topictype")
    private String topicType;

    /**
     * Get the value of topicType
     *
     * @return the value of topicType
     */
    public String getTopicType() {
        return topicType;
    }

    /**
     * Set the value of topicType
     *
     * @param topicType new value of topicType
     * @throws
     * jp.junoe.confluence.plugins.conf2dita.exception.TopicTypeNotSupportException
     */
    public void setTopicType(String topicType) throws TopicTypeNotSupportException {
        
        if (!StringUtils.isEmpty(topicType)){
            if (!isSupported(topicType)){
                throw new TopicTypeNotSupportException(topicType);
            }
        }

        this.topicType = topicType;
    }

    /**
     * このTopicTypeを設定した手段。Ex:Label, SmartDITA,...
     */
    @XmlElement(name = "generator")
    private String generator;

    /**
     * Get the value of generator
     *
     * @return the value of generator
     */
    public String getGenerator() {
        return generator;
    }

    /**
     * Set the value of generator
     *
     * @param generator new value of generator
     */
    public void setGenerator(String generator) {
        this.generator = generator;
    }

    /**
     * for JAXB.unmershal
     */
    public DitaTopicType() {
    }

    /**
     * constructor
     *
     * @param page
     * @param cm
     */
    public DitaTopicType(ContentEntityObject page, ContentPropertyManager cm) {
        this.page = page;
        this.cm = cm;
    }

    /**
     * topicTypeをサポートしてるかどうか
     * @param topicType
     * @return
     */
    public static boolean isSupported(String topicType){
        return supportedTopicTypes.indexOf(topicType) >= 0 ;
    }
    
    /**
     * オブジェクトをロードする
     * @param page
     * @param cm
     * @return
     */
    public static DitaTopicType load(ContentEntityObject page, ContentPropertyManager cm) {
        String topicTypeString = cm.getTextProperty(page, Conf2dita.DITA_TOPIC_TYPE);
        DitaTopicType root = null;
        if (!StringUtils.isEmpty(topicTypeString)) {
            root = JAXB.unmarshal(new StringReader(topicTypeString), DitaTopicType.class);
        }

        if (root == null) {
            root = new DitaTopicType(page, cm);
        } else {
            root.cm = cm;
            root.page = page;
        }

        return root;
    }

    /**
     * このObjectをXMLとしてContentPropertyに保存する
     */
    public void store() {
        if (cm != null && page != null) {
            if (StringUtils.isEmpty(topicType)) {
                cm.removeProperty(page, Conf2dita.DITA_TOPIC_TYPE);
            } else {
                StringWriter writer = new StringWriter();
                JAXB.marshal(this, writer);
                cm.setTextProperty(page, Conf2dita.DITA_TOPIC_TYPE, writer.toString());
            }
        }
    }
}
