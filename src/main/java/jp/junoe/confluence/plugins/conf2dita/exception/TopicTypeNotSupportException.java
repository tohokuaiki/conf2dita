/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jp.junoe.confluence.plugins.conf2dita.exception;

/**
 *
 * @author Takashi
 */
public class TopicTypeNotSupportException extends Conf2ditaException{

    public TopicTypeNotSupportException(String topicType) {
        super("toipctype \"" + topicType + "\" is not supported.");        
    }
}
