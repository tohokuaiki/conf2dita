/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDocumentHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;

/**
 * dom4jのDocumentクラスのDita用ラッパークラス的な。
 *
 * @author Takashi
 */
public class TopicDocument extends DitaDocument {

    /**
     * DitaのTopicTypeが入っている
     */
    public final String topicType;

    /**
     * @see DitaDocument#asXML() DitaDocumentでXML出力をする際にあえて文字参照にしたいものを列挙
     */
    public static final String[] characterAsXMLConvert = {"'", "\""};

    /**
     * Constructor DITAXMLを引数に取りそれに対してあれこれを行う
     *
     * @param ditaXMLFile
     * @throws org.dom4j.DocumentException DITAXMLを正常にロードできない
     */
    public TopicDocument(File ditaXMLFile) throws DocumentException {

        String ditaXML;
        try {
            ditaXML = FileUtils.readFileToString(ditaXMLFile, "UTF-8");
        } catch (IOException ex) {
            throw new DocumentException(ex.getMessage());
        }
        document = DitaDocumentHelper.parseTextWithoutValidate(ditaXML);
        topicType = document.getDocType().getElementName();
    }

    /**
     *
     * @see #DitaXmlUtil(java.io.File)
     * @param ditaXML
     * @throws org.dom4j.DocumentException
     */
    public TopicDocument(String ditaXML) throws DocumentException {
        document = DitaDocumentHelper.parseTextWithoutValidate(ditaXML);
        topicType = document.getDocType().getElementName();
    }

    /**
     *
     * @param document
     */
    public TopicDocument(Document document) {
        this.document = document;
        topicType = document.getDocType().getElementName();
    }

    /**
     * href属性値のパスをあわせる
     *
     * @see DitaExportUtils#fixAttachmentHref
     * @param upDirectory 上にさかのぼる場合の
     * @param attachDir
     */
    public void fixHrefPath(String upDirectory, String attachDir) {
        // image
        Attribute attr;
        String href;
        for (Object o : document.selectNodes("//image/@href")) {
            attr = (Attribute) o;
            href = "";
            if (!StringUtils.isEmpty(upDirectory)) {
                href += upDirectory;
            }
            if (!StringUtils.isEmpty(attachDir)) {
                href += attachDir + "/";
            }
            href += attr.getValue();
            attr.setValue(href);
        }
    }

    /**
     * DitaXMLからスペースキーの取得
     *
     * @return
     */
    public String readSpaceKey() {
        String spaceKey = "";
        List othermetas = document.selectNodes("//othermeta[@name=\"" + Conf2dita.CONF2DITA_SPACEKEY_ATTRIBUTE + "\"]");
        Element othermeta = null;
        for (Object om : othermetas) {
            othermeta = (Element) om;
            spaceKey = othermeta.attribute("content").getValue();
            break;
        }
        return spaceKey;
    }

    /**
     * DitaXMLから以前にExportした時のConfluenceのページIDを取得\ ページIDはExport時に
     * othermetaに、name=Conf2dita.CONF2DITA_PAGEID_ATTRIBUTEで埋め込んでいる
     *
     * @return 以前にExportしたConfluenceのページID
     */
    public long readPageId() {
        long pageId = 0;
        String pageId_s = "";
        List othermetas = document.selectNodes("//othermeta[@name=\"" + Conf2dita.CONF2DITA_PAGEID_ATTRIBUTE + "\"]");
        Element othermeta = null;
        for (Object om : othermetas) {
            othermeta = (Element) om;
            pageId_s = othermeta.attribute("content").getValue();
            break;
        }
        if (StringUtils.isEmpty(pageId_s)) {
            return pageId;
        }
        try {
            pageId = Long.parseLong(pageId_s);
        } catch (NumberFormatException ex) {
        }
        return pageId;
    }

    /**
     * DITAのトピックIDを取得
     *
     * @return
     */
    public String readTopicId() {
        return document.valueOf("/" + topicType + "/@id");
    }

    /**
     * DitaXMLからトピックタイトルを取得
     *
     * @return トピックのタイトル
     */
    public String readTitle() {
        String ttl = "";
        ttl = document.valueOf("/" + topicType + "/title");
        return ttl;
    }

    /**
     * DITAのトピックタイプを取得
     *
     * @return
     */
    public String readTopicType() {
        return document.getRootElement().getQualifiedName().toLowerCase();
    }

    /**
     * 画像のパスをすべて返す
     *
     * @return
     */
    public List readImagesPath() {
        List<String> imagesPath = new ArrayList<String>();
        Element image = null;
        for (Object o : document.selectNodes("//image")) {
            image = (Element) o;
            String href = image.attribute("href").getValue();
            if (imagesPath.indexOf(href) < 0) {
                imagesPath.add(href);
            }
        }
        return imagesPath;
    }

}
