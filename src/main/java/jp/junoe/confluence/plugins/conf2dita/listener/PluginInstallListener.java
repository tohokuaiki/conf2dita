package jp.junoe.confluence.plugins.conf2dita.listener;

import com.atlassian.confluence.event.events.plugin.PluginInstallEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import java.io.File;
import java.io.IOException;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluenceFileUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.DisposableBean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Takashi
 */
public class PluginInstallListener implements DisposableBean {

    /**
     * イベントパブリッシャ
     */
    protected EventPublisher eventPublisher;

    public PluginInstallListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        eventPublisher.register(this);
    }

    /**
     * ページを更新した際に実行される
     *
     * @param event
     */
    @EventListener
    public void onInstallPluginHandler(PluginInstallEvent event) {
        if (event.getPluginKey().toLowerCase().endsWith(Conf2dita.DITA)) {
            // temporary directory の XSLTとDTDを削除
            String temporaryDir = JConfluenceFileUtil.getTemporaryDir();
            try {
                FileUtils.deleteDirectory(new File(temporaryDir + "/xslt/conf2dita"));
                FileUtils.deleteDirectory(new File(temporaryDir + "/dtd/confluence"));                
                FileUtils.deleteDirectory(new File(temporaryDir + "/dtd/docs.oasis-open.org"));
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }

}
