/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.webpanel;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import java.util.Map;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;

/**
 *
 * @author Takashi
 */
public class HeaderContext implements ContextProvider {

    private final ContentPropertyManager contentPropertyManager;

    public HeaderContext(ContentPropertyManager cm) {
        contentPropertyManager = cm;
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        Object _action = map.get("action");
        ConfluenceActionSupport action;
        ContentEntityObject page;
        if (_action instanceof ConfluenceActionSupport) {
            action = (ConfluenceActionSupport) _action;
            Map<String, Object> context = action.getContext();
            for (Map.Entry<String , Object> e: context.entrySet()){
                System.err.println(e.getKey());
                System.err.println(e.getValue().getClass());
            }
            
            Object _page = context.get("page");
            if (_page instanceof ContentEntityObject) {
                page = (ContentEntityObject) _page;
                DitaTopicType topicType = new DitaTopicType(page, contentPropertyManager);
                map.put("topicType", topicType);
            }
        }

        return map;
    }
}
