/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import jp.junoe.confluence.plugins.JConfluenceXSLTUtil;
import jp.junoe.confluence.plugins.conf2dita.DitaXmlUtil;
import static jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument.characterAsXMLConvert;
import net.sf.saxon.Configuration;
import net.sf.saxon.FeatureKeys;
import net.sf.saxon.StandardErrorListener;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Text;

/**
 *
 * @author Takashi
 */
public class DitaDocument {

    /**
     * read済みのDitaXMLが入っている
     */
    protected Document document;
    
    /**
     * 
     */
    protected String topicType;
    
    
    /**
     * 
     * @return 
     */
    public String getTopicType(){
        return topicType;
    }

    /**
     * XMLを文字列にするときにいろいろと処理する
     *
     * @return
     */
    public String asXML() {
        String xml;
        String placeHolder = "*^@#%#@^*"; // ありえないだろう文字列

        // "や'をきちんと出すために、いったんTextNodeで変更する
        for (Object o : selectNodes("//*/text()")) {
            Text t;
            if (o instanceof Text) {
                t = (Text) o;
                String text = t.getText();
                for (String s : characterAsXMLConvert) {
                    text = text.replace(s, placeHolder + s + placeHolder);
                }
                t.setText(text);
            }
        }

        xml = document.asXML();
        for (String s : characterAsXMLConvert) {
            String r = DitaXmlUtil.EntityFilter.charcterEntityReference.get(s);
            if (r == null) {
                r = s;
            }
            xml = xml.replace(placeHolder + s + placeHolder, r);
        }
        return xml;
    }
    
    

    /**
     * DTDをPublicなものに書き換える
     *
     * @throws java.io.IOException
     */
    public void setPublicDTD() throws IOException {
        document = DitaDTD.replaceToPublicDTD(document);
    }

    /**
     * DTDをLocalのものに書き換える
     *
     * @throws java.io.IOException
     */
    public void setLocalDTD() throws IOException {
        document = DitaDTD.replaceToLocalDTD(document);
    }

    /**
     * passthru to document
     *
     * @param query
     * @return
     */
    public List selectNodes(String query) {
        return document.selectNodes(query);
    }

    /**
     * passthru to document
     *
     * @return
     */
    public Element getRootElement() {
        return document.getRootElement();
    }

    /**
     * DitaXMLをDTDによりValidateする
     *
     * @throws javax.xml.transform.TransformerException
     * XMLとして体をなしていない場合（タグ名が無いというのがよくある）
     * @throws
     * jp.junoe.confluence.plugins.conf2dita.dita.DitaDTDValidateErrorException
     * DITAのDTD違反の場合
     *
     */
    public void validateDTD() throws TransformerException, DitaDTDValidateErrorException {
        /*
         Locale aDefault = Locale.getDefault();
         String country = aDefault.getCountry();
         System.out.println(country); // JP
         System.out.println(System.getProperty("user.language")); // ja
         Locale newLocale = new Locale("ja", "JP");
         Locale.setDefault(newLocale);
         */
        // String ditaXML = dita.asXML();
        String ditaXML = JConfluenceXSLTUtil.getFormattedXML(document);

        /* DOM4J */
        /*        SAXReader saxReader = new SAXReader(true);
         saxReader.setValidation(true);
         saxReader.setErrorHandler(new DTDError());

         try {
         saxReader.read(new ByteArrayInputStream(ditaXML.getBytes("UTF-8")));
         } catch (UnsupportedEncodingException ex) {
         System.out.println(ex.getMessage());
         } catch (DocumentException ex) {
         System.out.println("[DOM4J] " + ex.getLocalizedMessage());
         }
         */
        /* XMLReader */
        /*        SAXParserFactory saxpf = SAXParserFactory.newInstance();
         saxpf.setValidating(true);
         SAXParser newSAXParser = null;
         XMLReader xmlr = null;
         try {
         newSAXParser = saxpf.newSAXParser();
         xmlr = newSAXParser.getXMLReader();
         xmlr.setErrorHandler(new DTDError());
         xmlr.parse(new InputSource(new ByteArrayInputStream(ditaXML.getBytes("UTF-8"))));
         } catch (ParserConfigurationException ex) {
         Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
         } catch (SAXException ex) {
         System.out.println("[SAXParser] " + ex.getLocalizedMessage());
         Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
         } catch (UnsupportedEncodingException ex) {
         Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
         Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
         }
         */
        /* Saxon */
        String noopXsl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><xsl:stylesheet version=\"2.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"></xsl:stylesheet>";
        StreamSource xslSource;
        StreamSource xmlSource;
        // System.out.println("[SAXON] check");
        try {
            xslSource = new StreamSource(new ByteArrayInputStream(noopXsl.getBytes("UTF-8")));
            xmlSource = new StreamSource(new ByteArrayInputStream(ditaXML.getBytes("UTF-8")));
            // エラー時の出力をByteArrayOutputStreamで受けるようなSaxonのFactoryクラスを作成する
            Configuration saConfig = new Configuration();
            StandardErrorListener se = new StandardErrorListener();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream ps = new PrintStream(baos);
            se.setErrorOutput(ps);
            saConfig.setErrorListener(se);
            TransformerFactory tFactory = new net.sf.saxon.TransformerFactoryImpl(saConfig);
            tFactory.setAttribute(FeatureKeys.DTD_VALIDATION, true);
            Transformer transformer = tFactory.newTransformer(xslSource);
            transformer.transform(xmlSource, new StreamResult(new StringWriter()));
            IOUtils.closeQuietly(baos);
            IOUtils.closeQuietly(ps);
            if (baos.size() > 0) {
                throw new DitaDTDValidateErrorException(baos.toString(), document);
            }
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            // Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

}
