/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluenceXSLTUtil;
import jp.junoe.confluence.plugins.conf2dita.DitaExportUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.dom.DOMDocument;
import org.dom4j.dom.DOMDocumentType;

/**
 *
 * @author Takashi
 */
public class MapDocument extends DitaDocument {

    /**
     *
     */
    public static final HashMap<String, List<String>> supportedMapType = new HashMap<String, List<String>>() {
        {
            put("map", new ArrayList() {
                {
                    add("-//OASIS//DTD DITA Map//EN");
                    add("http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/map.dtd");
                }
            });
//            put("bookmap", new ArrayList() {
//                {
//                    add("-//OASIS//DTD DITA BookMap//EN");
//                    add("http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/bookmap/dtd/bookmap.dtd");
//                }
//            });
        }
    };

    /**
     * XML
     */
    private final ContentPropertyManager contentPropertyManager;

    /**
     *
     */
    private Page rootPage;

    /**
     *
     */
    private DitaExportUtils deu;

    /**
     * XMLから作成する
     *
     * @param ditaXML
     * @param cm
     * @return
     * @throws org.dom4j.DocumentException
     */
    public static MapDocument load(String ditaXML, ContentPropertyManager cm) throws DocumentException {
        return new MapDocument(DitaDocumentHelper.parseTextWithoutValidate(ditaXML), cm);
    }

    /**
     * 既存のものをload用のConstructor (for import)
     *
     * @param document
     * @param cm
     */
    public MapDocument(Document document, ContentPropertyManager cm) {
        this.document = document;
        this.contentPropertyManager = cm;
        topicType = document.getDocType().getElementName();
    }

    /**
     * create this Object.
     *
     * @param deu
     * @param rootPage
     * @param mapType
     * @param cm
     * @return
     * @throws DitaMapErrorException
     */
    public static MapDocument create(DitaExportUtils deu, Page rootPage, String mapType, ContentPropertyManager cm) throws DitaMapErrorException {

        DOMDocumentType docType;

        List<String> docInfo = MapDocument.supportedMapType.get(mapType);
        if (docInfo != null) {
            docType = new DOMDocumentType(mapType, docInfo.get(0), docInfo.get(1));
        } else {
            throw new DitaMapErrorException(mapType + " is not supported.");
        }
        MapDocument ditaMap = new MapDocument(deu, rootPage, docType, cm);

        // homePageかどうかで挙動を変える
        Element topicRef;
        if (rootPage.isHomePage()) {
            Element rootElement = ditaMap.document.getRootElement();
            for (Page child : rootPage.getSortedChildren()) {
                topicRef = ditaMap.getTopicRef(child);
                if (topicRef != null) {
                    rootElement.add(topicRef);
                    ditaMap.importChildPgae(child, topicRef);
                }
            }
        } else {
            topicRef = ditaMap.getTopicRef(rootPage);
            if (topicRef != null) {
                ditaMap.document.getRootElement().add(topicRef);
                ditaMap.importChildPgae(rootPage, topicRef);
            }
        }

        ditaMap.clearTopicHead();

        return ditaMap;
    }

    /**
     * 作成する用のConstructor
     *
     * @param deu
     * @param rootPage rootとなるページ
     * @param docType
     * @param cm
     */
    public MapDocument(DitaExportUtils deu, Page rootPage, DOMDocumentType docType, ContentPropertyManager cm) {
        this.deu = deu;
        this.rootPage = rootPage;
        contentPropertyManager = cm;
        document = new DOMDocument(docType);
        Element root = DocumentHelper.createElement(docType.getElementName());
        document.setRootElement(root);
    }

    /**
     * きれいに出力するasXML()
     *
     * @param prettyFormat
     * @return
     */
    public String asXML(boolean prettyFormat) {
        if (!prettyFormat) {
            return document.asXML();
        } else {
            return JConfluenceXSLTUtil.getFormattedXML(document);
        }
    }

    /**
     * ページをDITAMAPに取り込む（親ページが指定されている）
     *
     * @param page
     * @param parentPgae
     * @return
     */
    private void importChildPgae(Page page, Element appendElement) {
        Element topicRef;
        for (Page child : page.getSortedChildren()) {
            topicRef = getTopicRef(child);
            if (topicRef != null) {
                appendElement.add(topicRef);
                importChildPgae(child, topicRef);
            }
        }
    }

    /**
     * create topicref node of Confluence page.
     *
     * @param page
     * @return
     */
    private Element getTopicRef(Page page) {
        Element topicRef = null;

        String root = document.getDocType().getElementName().toLowerCase();
        String path = deu.getPageZipEntryPath(page);

        if (root.equals("bookmap")) {

        } else {
            String topicType = Conf2dita.getTopicType(page, contentPropertyManager);
            if (StringUtils.isEmpty(topicType)) {
                topicRef = DocumentHelper.createElement("topichead");
            } else {
                topicRef = DocumentHelper.createElement("topicref");
                topicRef.addAttribute("href", path).addAttribute("type", topicType);
            }
            topicRef.addAttribute("navtitle", page.getTitle());
        }

        return topicRef;
    }

    public int countTopicRef() {
        return this.selectNodes("//topicref").size();
    }

    public int countTopicHead() {
        return this.selectNodes("//topichead").size();
    }

    /**
     * topicrefを内包していないtopicheadは消す
     */
    private void clearTopicHead() {
        Element topichead;
        List topicheads = document.selectNodes("//topichead");
        for (int index = topicheads.size() - 1; index >= 0; index--) {
            Object ele = topicheads.get(index);
            if (ele instanceof Element) {
                topichead = (Element) ele;
                List refs = topichead.selectNodes(".//topicref");
                if (refs.isEmpty()) {
                    topichead.getParent().remove(topichead);
                }
            }
        }
    }

    /**
     *
     * @param targetElement
     * @param mapFilePath
     * @return
     */
    public List<Element> getChildTopics(Element targetElement, String mapFilePath) {

        List<Element> children = new ArrayList();
        Element ele;

        for (Object _ele : targetElement.selectNodes("topichead|topicref")) {
            if (_ele instanceof Element) {
                ele = (Element) _ele;
                children.add(ele);
            }
        }
        return children;
    }
}
