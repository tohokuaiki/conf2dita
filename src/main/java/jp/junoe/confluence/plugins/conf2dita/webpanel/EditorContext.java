/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jp.junoe.confluence.plugins.conf2dita.webpanel;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import java.util.Map;

/**
 *
 * @author Takashi
 */
public class EditorContext implements ContextProvider{

    public EditorContext() {
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        return map;
    }
}
