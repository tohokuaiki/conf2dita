/**
 * DITAをインポートする際に使う処理
 */
package jp.junoe.confluence.plugins.conf2dita;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.xml.transform.TransformerException;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluencePageUtil;
import jp.junoe.confluence.plugins.JZipFileUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTD;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTDValidateErrorException;
import jp.junoe.confluence.plugins.conf2dita.dita.MapDocument;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.tools.zip.ZipEntry;
import org.dom4j.Attribute;
import org.dom4j.DocumentException;
import org.dom4j.Element;

/**
 *
 * @author Takashi
 */
public class DitaImportUtils {

    /**
     * import as zip directory structure.
     */
    public static final String IMPORT_HIERARCHY_OPTION_DIRSTRUCTURE = "dirstructure";

    /**
     * import as flat.
     */
    public static final String IMPORT_HIERARCHY_OPTION_FLAT = "flat";

    /**
     * import as dita mapfile is.
     */
    public static final String IMPORT_HIERARCHY_OPTION_MAPFILE = "mapfile";

    private TopicDocument emptyTopic;

    private static final String DITA_EMPTY_TOPIC = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE topic PUBLIC \"-//OASIS//DTD DITA Topic//EN\" \"topic.dtd\"><topic id=\"\" xml:lang=\"en-us\"><title>%s</title><body/></topic>";

    /**
     * DITAインポート用のZIPファイルにおいて、添付ファイルが格納されるディレクトリ名
     *
     * @deprecated
     */
    private String attachmentsDir = "attachments";

    /**
     * インポートするZipファイル
     */
    private final File zipFile;

    /**
     *
     */
    private final List<ZipEntry> zipEntryList;

    public List<ZipEntry> getZipEntryList() {
        return zipEntryList;
    }

    /**
     * インポートに使用するMapファイル
     */
    private File mapFile;

    public File getMapFile() {
        return mapFile;
    }

    public void setMapFile(File mapFile) {
        this.mapFile = mapFile;
    }

    /**
     * ZIP展開したディレクトリ
     */
    public String unZipDirectory = null;

    /**
     * Mapファイルが無い際にZipの構造を反映するが、その起点となるディレクトリ
     */
    private final String zipRootDirectory;

    public String getZipRootDirectory() {
        return zipRootDirectory;
    }

    private final I18NBean i18n;

    /**
     * constructor
     *
     * @param zipFile file to import as dita files.
     * @param pageManager
     * @param page
     * @param contentPropertyManager
     */
    public DitaImportUtils(File zipFile, PageManager pageManager, AbstractPage page, ContentPropertyManager contentPropertyManager) {
        this.emptyTopic = null;
        this.zipFile = zipFile;
        this.zipEntryList = JZipFileUtil.getZipFileList(zipFile);
        this.zipRootDirectory = getZipEmptyRootPath(zipEntryList);
        this.i18n = GeneralUtil.getI18n();
    }

    /**
     *
     * @param pageManager
     * @param page
     * @return
     * @throws java.io.IOException
     */
    public TreeMap<String, HashMap<String, Object>> getZipListInfo(PageManager pageManager, AbstractPage page) throws Exception {

        TreeMap<String, HashMap<String, Object>> ziplistinfo = new TreeMap<String, HashMap<String, Object>>();

        String content = "";

        // ZIPの中からditamapとかタイトルとかを抜き出す
        HashMap<String, Object> zipinfo;
        TopicDocument dita;
        for (ZipEntry ze : zipEntryList) {
            if (!ze.isDirectory() && ze.getName().endsWith(DitaXmlUtil.DITA_FILE_EXTENSION)) {
                zipinfo = new HashMap<String, Object>();
                content = JZipFileUtil.getZipEntryContent(this.zipFile, ze, "MS932");
                if (!content.isEmpty()) {
                    try {
                        content = DitaDTD.replaceToLocalDTD(content);
                    } catch (Exception e) {
                        throw new Exception("Dita Document Error at " + ze.getName() + ". Message:" + e.getMessage());
                    }
                    try {
                        dita = new TopicDocument(content);  // see local
                        zipinfo.put("topictype", dita.topicType);
                        zipinfo.put("dita_title", dita.readTitle());
                        zipinfo.put("dita_exported_pageId", dita.readPageId());
                        zipinfo.put("dita_exported_spaceKey", dita.readSpaceKey());
                        zipinfo.put("dita_topic_id", dita.readTopicId());
                        // 現在のスペースで同じタイトルのページを探してみる
                        long pageId = dita.readPageId();
                        Page targetPage = JConfluencePageUtil.findTargetPage(pageId, dita.readTitle(), pageManager, page.getSpace());
                        if (targetPage != null) {
                            zipinfo.put("targetpage_id", targetPage.getIdAsString());
                            zipinfo.put("targetpage_title", targetPage.getTitle());
                        }
                        ziplistinfo.put(ze.getName(), zipinfo);
                    } catch (DocumentException e) {
                        throw new Exception("Document Error at " + ze.getName() + ". Message:" + e.getMessage());
                    }
                }
            }
        }

        return ziplistinfo;
    }

    /**
     *
     * @param pageManager
     * @param page
     * @param contentPropertyManager
     * @return
     * @throws java.io.IOException
     */
    public TreeMap<String, HashMap<String, Object>> getMapListInfo(PageManager pageManager, AbstractPage page, ContentPropertyManager contentPropertyManager)
            throws IOException, Exception {

        TreeMap<String, HashMap<String, Object>> maplistinfo = new TreeMap<String, HashMap<String, Object>>();

        String content = "";

        // ZIPの中からditamapとかタイトルとかを抜き出す
        HashMap<String, Object> zipinfo;
        MapDocument ditamap;

        for (ZipEntry ze : zipEntryList) {
            if (!ze.isDirectory() && ze.getName().endsWith(DitaXmlUtil.DITAMAP_FILE_EXTENSION)) {
                zipinfo = new HashMap<String, Object>();
                content = JZipFileUtil.getZipEntryContent(this.zipFile, ze, "MS932");
                if (!content.isEmpty()) {
                    content = DitaDTD.replaceToLocalDTD(content);
                    // ditamap = new MapDocument(content);  // see local
                    ditamap = MapDocument.load(content, contentPropertyManager);
                    zipinfo.put("topictype", ditamap.getTopicType());
                    zipinfo.put("topicrefnum", ditamap.countTopicRef());
                    zipinfo.put("topicheadnum", ditamap.countTopicHead());
                    maplistinfo.put(ze.getName(), zipinfo);
                }
            }
        }

        return maplistinfo;
    }

    /**
     * 展開したZIPファイルの中でルートディレクトリのパスを見つける
     *
     * @param unZip
     * @return
     */
    public String getDitaImportZipRootPath(Map<String, File> unZip) {
        String rootPath = "";
        File file = null;
        for (Map.Entry<String, File> e : unZip.entrySet()) {
            file = e.getValue();
            if (file.isFile() && file.getAbsolutePath().endsWith(DitaXmlUtil.DITA_FILE_EXTENSION)) {
                if (rootPath.equals("") || rootPath.length() > file.getParent().length()) {
                    rootPath = file.getParent();
                }
            }
        }
        return rootPath;
    }

    /**
     * ZIPの中からルートディレクトリのフォルダ名を見つける。ZIPは往々にして空パスを1つ含んでしまうのでその対応<br>
     * トップ階層から調べて、子要素がディレクトリ1つのみだったらルート
     *
     * @param ziplist
     * @return
     */
    private String getZipEmptyRootPath(List<ZipEntry> ziplist) {
        String rootPath = "";
        // minimum entry name
        String minimumEntryName = "";
        for (ZipEntry ze : ziplist) {
            if (minimumEntryName.length() == 0 || minimumEntryName.length() > ze.getName().length()) {
                minimumEntryName = ze.getName();
            }
        }

        String sameChars = "";
        for (int i = 1; i <= minimumEntryName.length(); i++) {
            boolean plusOne = true;
            sameChars = minimumEntryName.substring(0, i);
            for (ZipEntry ze : ziplist) {
                if (!ze.getName().startsWith(sameChars)) {
                    plusOne = false;
                    sameChars = sameChars.substring(0, sameChars.length() - 1);
                    break;
                }
            }
            if (!plusOne) {
                break;
            }
        }

        if (sameChars.contains("/")) {
            rootPath = sameChars.substring(0, sameChars.indexOf("/"));
        }

        return rootPath;
    }

    /**
     * unZipしたファイルを階層化してArrayListに納める(ディレクトリ階層版)
     *
     * @param targetDir 本ZIPファイルにおいてどの階層をターゲットとして処理するかの指示。
     * @param asFlat trure =>フラットに取り込む. false => ディレクトリの階層をそのまま取り込む.
     * @return
     * @throws Exception
     */
    public ArrayList<DitaFileSet> stratifyZipFile(String targetDir, boolean asFlat) throws Exception {

        ArrayList<DitaFileSet> list = new ArrayList<DitaFileSet>();

        if (targetDir == null) {
            targetDir = unZipDirectory;
            if (!zipRootDirectory.isEmpty()) {
                targetDir += File.separator + zipRootDirectory;
            }
        }

        File dir = new File(targetDir);
        DitaFileSet df;
        if (dir.isDirectory()) {
            // 実行
            if (asFlat) {
                String ext = DitaXmlUtil.DITA_FILE_EXTENSION.substring(1);
                String[] ditaextension = new String[]{ext, ext.toUpperCase()};
                Collection<File> ditaFiles = FileUtils.listFiles(dir, ditaextension, true);
                for (File ditaFile : ditaFiles) {
                    df = createDitaFileSet(ditaFile);
                    list.add(df);
                }
            } else {
                for (File file : dir.listFiles()) {
                    // ファイルの拡張子が.ditaのみピックアップ
                    if (file.isFile()) {
                        if (Conf2dita.isDitaExt(file)) {
                            df = createDitaFileSet(file);
                            String path = df.getFile().getCanonicalPath().replace(this.unZipDirectory, "");
                            // 子ファイル
                            String childDitaDir = getImportZipChildDitaPath(path, this.unZipDirectory);
                            ArrayList<DitaFileSet> c = stratifyZipFile(childDitaDir, false);
                            df.setChildren(c);
                            list.add(df);
                        }
                    } else {
                        // ディレクトリで.ditaのファイルが無い場合
                        File headDita = new File(file.getCanonicalPath() + DitaXmlUtil.DITA_FILE_EXTENSION);
                        if (!headDita.exists()){
                            df = createEmptyDitaFileSet(file.getName());
                            ArrayList<DitaFileSet> c = stratifyZipFile(file.getCanonicalPath(), false);
                            df.setChildren(c);
                            list.add(df);
                        }
                    }
                }
            }
        }
        return list;
    }

    /**
     * unZipしたファイルを階層化してArrayListに納める(mapfile版)
     *
     * @param map
     * @param mapElement mapを使っている場合、現在どの部分まで読んだかを明確にする。
     * @return
     * @throws java.lang.Exception
     */
    public ArrayList<DitaFileSet> stratifyZipFile(MapDocument map, Element mapElement) throws Exception {

        ArrayList<DitaFileSet> list = new ArrayList<DitaFileSet>();

        String mapFilePath = mapFile.getCanonicalPath();

        // get import target files
        List<Element> targetFiles = map.getChildTopics(mapElement, mapFilePath);

        int position = 0;
        for (Element topicref : targetFiles) {
            position++;
            Attribute href = topicref.attribute("href");
            DitaFileSet df;
            if (href == null) {
                // topichead
                df = new DitaFileSet();
                df.setPageTitle(topicref.attributeValue("navtitle"));
            } else {
                File topicFile = new File(mapFile.getParent() + File.separator + href.getValue());
                df = createDitaFileSet(topicFile);
            }
            df.setPosition(position);
            // 子エレメント
            ArrayList<DitaFileSet> children = stratifyZipFile(map, topicref);
            df.setChildren(children);
            list.add(df);
        }

        return list;
    }

    private DitaFileSet createDitaFileSet(File file) throws Exception {
        DitaFileSet df = null;
        String ditaXml = null;
        // DTD変更してValidateする
        try {
            ditaXml = FileUtils.readFileToString(file, "UTF-8");
            TopicDocument dita = new TopicDocument(ditaXml);
            dita.setLocalDTD();
            dita.validateDTD();
        } catch (DitaDTDValidateErrorException ex) {
            // throw new Exception("[Invalid DITA]" + file.getName() + ":  " + ex.getMessage());
            throw new Exception("[Dita DTD ERROR] at " + file.getName() + "\n" + ex.getDTDErrorMessage(false, false));
        } catch (DocumentException ex) {
            throw new Exception("[ERROR] at " + file.getName() + "\n" + ex.getMessage());
        } catch (TransformerException ex) {
            throw new Exception("[ERROR] at " + file.getName() + "\n" + ex.getMessage());
        } catch (IOException ex) {
            // file not found
            String notFoundFile = file.getCanonicalPath().replace(unZipDirectory, "");
            String errorMessage = i18n.getText("conf2dita.action.import.error.filenotfound.inzip", new Object[]{notFoundFile});
            throw new Exception(errorMessage);
        }

//        String path;
        if (ditaXml != null) {
            // Import用Objectを作成
            df = new DitaFileSet(file);
            // 添付ファイル
//            String zipRoot = this.unZipDirectory + File.separator + zipRootDirectory;
//            path = file.getCanonicalPath().replace(zipRoot, "");
//            String attachmentsPath = getImportPageAttachmentPath(path, zipRoot);
//            df.setAttachments(new File(attachmentsPath));
            df.setAttachments();
        }

        return df;
    }

    /**
     * emptyTopicを使った空のDitaFileSetを作る
     *
     * @return
     */
    private DitaFileSet createEmptyDitaFileSet(String title) throws Exception {
        DitaFileSet df;
        String ditaXml = String.format(
                DitaImportUtils.DITA_EMPTY_TOPIC,
                new Object[]{title});

        df = new DitaFileSet(ditaXml);

        return df;
    }

    /**
     * ditaのXMLのある場所から添付ファイルがあるPathを取り出す TODO: TODO: ZIPファイルの作成する時の命名則に従っている
     * この取り出し方はImportするZIPの構造に依存するので、様々な方法（優先順位）を実装しなければならない。
     *
     * @deprecated
     * @param zipRoot
     * @param ditaPath
     * @return
     */
    private String getImportPageAttachmentPath(String ditaPath, String zipRoot) {

        String ret = "";

        /**
         * case1. ditaファイルが 1234/5678/p2345.dita、添付ファイルディレクトリが
         * attachments/1234/5678/2345 といった場合
         */
        ditaPath = StringUtils.strip(ditaPath, File.separator);
        String[] ditaPathes = StringUtils.split(ditaPath, File.separator);
        String ditaFile = ditaPathes[ditaPathes.length - 1];

        if (ditaFile.endsWith(DitaXmlUtil.DITA_FILE_EXTENSION)) {
            ditaFile = ditaFile.substring(0, ditaFile.lastIndexOf(DitaXmlUtil.DITA_FILE_EXTENSION));
            if (ditaFile.startsWith("p")) {
                ditaFile = ditaFile.substring(1);
            }
            ditaPathes[ditaPathes.length - 1] = ditaFile;
        }
        String attachmentsPath = StringUtils.join(ditaPathes, File.separator);

        File attachmentsDirFile = new File(zipRoot + File.separator + attachmentsDir + File.separator + attachmentsPath);
        if (attachmentsDirFile.isDirectory()) {
            ret = attachmentsDirFile.getAbsolutePath();
        }

        return ret;
    }

    /**
     * ditaファイルの子階層のファイルが入っているディレクトリを取得する TODO: ZIPファイルの作成する時の命名則に従っている
     *
     * @param ditaPath
     * @param zipRoot
     * @return
     */
    private String getImportZipChildDitaPath(String ditaPath, String zipRoot) {

        /**
         * case1. ditaファイルが 1234/p2345.dita、子ファイルが 1234/2345/ 配下に存在 といった場合
         */
        ditaPath = ditaPath.substring(0, ditaPath.lastIndexOf(DitaXmlUtil.DITA_FILE_EXTENSION));
        String[] ditaPathes = StringUtils.split(ditaPath, File.separator);
        String childDirectoryPath = "";
        for (String dp : ditaPathes) {
            if (!dp.equals("")) {
                if (dp.startsWith("p")) {
                    dp = dp.substring(1);
                }
                childDirectoryPath += File.separator + dp;
            }

        }

        childDirectoryPath = zipRoot + childDirectoryPath;
        if (!(new File(childDirectoryPath)).isDirectory()) {
            return "";
        }

        return childDirectoryPath;
    }

    /**
     * ファイルを展開
     *
     * @throws java.io.IOException
     */
    public void unZip() throws IOException, Exception {
        String zipFilePath = zipFile.getCanonicalPath();
        String dist_dir = zipFilePath.substring(0, zipFilePath.lastIndexOf(".tmp"));
        TreeMap<String, File> unZip = JZipFileUtil.unZip(zipFilePath, dist_dir, "MS932");
        unZipDirectory = dist_dir;
    }

}
