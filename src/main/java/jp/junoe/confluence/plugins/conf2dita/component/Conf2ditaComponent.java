/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jp.junoe.confluence.plugins.conf2dita.component;

import com.atlassian.confluence.core.ContentEntityObject;
import jp.junoe.confluence.plugins.Conf2dita;

/**
 * Conf2ditaで使いたいメソッドを提供
 * @author Takashi
 */
public interface Conf2ditaComponent {
    
    String getTopicIdKey();
    
    String getTopicTypeKey();
    
    String getImportedLabelPrefix();
    
    String getTopicId(ContentEntityObject page);
    
    String getTopicType(ContentEntityObject page);
}
