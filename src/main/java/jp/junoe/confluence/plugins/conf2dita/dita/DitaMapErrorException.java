/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jp.junoe.confluence.plugins.conf2dita.dita;

import org.dom4j.DocumentException;

/**
 *
 * @author Takashi
 */
public class DitaMapErrorException  extends DocumentException {

    public DitaMapErrorException(String message) {
        super(message);
    }
}
