/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import java.util.HashMap;
import java.util.List;
import jp.junoe.confluence.plugins.Conf2dita;

/**
 * DITA変換やInport/Export時に使うであろうConfluenceのページ情報を扱うクラス。
 * DBアクセスコストを下げるためメモリ上にデータを残すのでSingletonインターフェイス
 *
 * @author Takashi
 */
public class ConfluencePageCache {

    private static final ConfluencePageCache instance = new ConfluencePageCache();

    /**
     * SpaceKey/トピックID をキーにしたPageInfoのキャッシュ
     */
    private final HashMap<String, HashMap<String, PageInfo>> pageCacheTopicId;

    /**
     * PageManager
     */
    private PageManager pm;

    /**
     * ContentPropertyManager
     */
    private ContentPropertyManager cm;

    /**
     * for Singleton
     */
    private ConfluencePageCache() {
        this.pageCacheTopicId = new HashMap<String, HashMap<String, PageInfo>>();
    }

    /**
     * get singleton class
     *
     * @param pm
     * @param cm
     * @return
     */
    public static ConfluencePageCache getInstance(PageManager pm, ContentPropertyManager cm) {
        instance.pm = pm;
        instance.cm = cm;
        return instance;
    }

    /**
     *
     * @param space
     * @param topicId
     * @return
     */
    public PageInfo getPageIdByTopicId(Space space, String topicId) {
        storeSpacePages(space);

        String spaceKey = space.getKey();
        HashMap<String, PageInfo> pageInfos = pageCacheTopicId.get(spaceKey);
        if (pageInfos != null) {
            PageInfo pi = pageInfos.get(topicId);
            if (pi != null) {
                return pi;
            }
        }

        return null;
    }

    /**
     * @todo 何とか普通にキャッシュを使えるように
     * @param space 
     */
    private void storeSpacePages(Space space) {

        String spaceKey = space.getKey();
        if (pageCacheTopicId.get(spaceKey) == null || true) {

            HashMap<String, PageInfo> map4topicId = new HashMap<String, PageInfo>(); // topicIdをキーにしたMap

            List<Page> pages = pm.getPages(space, true);
            for (Page page : pages) {
                String topicId = cm.getStringProperty(page, Conf2dita.DITA_TOPIC_ID);
                if (!StringUtils.isEmpty(topicId)) {
                    PageInfo pi = new PageInfo(page);
                    pi.setTopicId(topicId);
                    map4topicId.put(topicId, pi);
                }
            }

            pageCacheTopicId.put(spaceKey, map4topicId);
        }
    }

    /**
     * キャッシュしたページの簡易情報
     */
    public final class PageInfo {

        private long pageId = 0;
        private String title = "";
        private String type = "";
        private String topicId = "";

        public PageInfo(Page page) {
            load(page);
        }

        public void load(Page page) {
            pageId = page.getId();
            type = page.getType();
            title = page.getTitle();
        }

        public String getTitle() {
            return title;
        }

        public long getPageId() {
            return pageId;
        }

        public String getType() {
            return type;
        }

        public String getTopicId() {
            return topicId;
        }

        private void setTopicId(String topicId) {
            this.topicId = topicId;
        }
    }
}
