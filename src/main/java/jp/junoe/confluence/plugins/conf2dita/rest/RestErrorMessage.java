package jp.junoe.confluence.plugins.conf2dita.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Common REST Error Message
 * @author Takashi
 */
@XmlRootElement(name = "status")
@XmlAccessorType(XmlAccessType.FIELD)
public class RestErrorMessage {
    
    @XmlElement(name="status-code")
    private int statusCode;
    
    @XmlElement()
    private String messsage;

    public RestErrorMessage() {
    }

    public RestErrorMessage(int statusCode, String messsage) {
        this.statusCode = statusCode;
        this.messsage = messsage;
    }
    
    public static Response createErrorResponse(Status status, String message) {
        return Response.status(status).entity(new RestErrorMessage(status.getStatusCode(), message)).build();
    }
}
