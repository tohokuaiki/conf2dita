/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.listener;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;
import jp.junoe.confluence.plugins.conf2dita.event.DitaPageSettingEvent;
import jp.junoe.confluence.plugins.conf2dita.exception.TopicTypeNotSupportException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

/**
 * Pageに対するDitaの設定値をあれこれしたいためのListener
 *
 * @author Takashi
 */
public class DitaPageSettingListener implements DisposableBean {

    protected EventPublisher eventPublisher;

    private final ContentPropertyManager cm;

    public DitaPageSettingListener(EventPublisher eventPublisher, ContentPropertyManager cm) {
        this.eventPublisher = eventPublisher;
        this.cm = cm;
        eventPublisher.register(this);
    }

    /**
     * 任意のタイミングで実行してTopicTypeを設定する
     *
     * @param event
     */
    @EventListener
    public void setTopicTypeHandler(DitaPageSettingEvent event) {

        String topicType = event.getTopicType();

        ContentEntityObject page = event.getPage();
        try {
            DitaTopicType ditaTopicType = new DitaTopicType(page, cm);
            ditaTopicType.setTopicType(topicType);
            ditaTopicType.setGenerator(event.getGenerator());
            ditaTopicType.store();
        } catch (TopicTypeNotSupportException ex) {
            LoggerFactory.getLogger(this.getClass()).error(ex.getMessage());
        }

    }

    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }
}
