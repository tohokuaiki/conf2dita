package jp.junoe.confluence.plugins.conf2dita.component;

import com.atlassian.confluence.core.ContentEntityObject;

public interface EventProxyComponent
{
    String getName();
    
    void publishDitaPageSettingEvent(ContentEntityObject page, String topicType, String generator);
}