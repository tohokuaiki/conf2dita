/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.component;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import jp.junoe.confluence.plugins.Conf2dita;

/**
 *
 * @author Takashi
 */
public class Conf2ditaComponentImpl implements Conf2ditaComponent {

    ContentPropertyManager contentPropertyManager;

    public Conf2ditaComponentImpl(ContentPropertyManager cm) {
        contentPropertyManager = cm;
    }

    @Override
    public String getTopicIdKey() {
        return Conf2dita.DITA_TOPIC_ID;
    }

    @Override
    public String getTopicTypeKey() {
        return Conf2dita.DITA_TOPIC_TYPE;
    }
    
    @Override
    public String getImportedLabelPrefix(){
        return Conf2dita.DITA_TOPIC_TYPE_LABEL_IMPORTED_PREFIX;
    }

    @Override
    public String getTopicId(ContentEntityObject page) {
        return Conf2dita.getTopicId(page, contentPropertyManager);
    }

    @Override
    public String getTopicType(ContentEntityObject page) {
        return Conf2dita.getTopicType(page, contentPropertyManager);
    }

}
