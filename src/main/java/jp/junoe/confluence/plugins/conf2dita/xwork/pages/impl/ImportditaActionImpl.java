/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.xwork.pages.impl;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
// import com.atlassian.confluence.json.parser.JSONObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.core.util.PairType;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.xwork.FileUploadUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.interceptor.SessionAware;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import jp.junoe.confluence.plugins.JConfluenceHTTPUtil;
import jp.junoe.confluence.plugins.JZipFileUtil;
import jp.junoe.confluence.plugins.conf2dita.ConfluencePageCache;
import jp.junoe.confluence.plugins.conf2dita.DitaFileSet;
import jp.junoe.confluence.plugins.conf2dita.DitaImportUtils;
import jp.junoe.confluence.plugins.conf2dita.dita.MapDocument;
import org.apache.commons.io.FileUtils;
import org.apache.tools.zip.ZipEntry;

/**
 *
 * @author Takashi
 */
abstract public class ImportditaActionImpl extends ViewPageAction implements PageAware, SessionAware {

    private AbstractPage page;

    private Map session;

    // public ArrayList<HashMap<String, ArrayList<ZipEntry>>> ziplist2 = new ArrayList<HashMap<String, ArrayList<ZipEntry>>>();
    public TreeMap<String, HashMap<String, Object>> ziplistinfo = new TreeMap<String, HashMap<String, Object>>();

    public TreeMap<String, HashMap<String, Object>> maplistinfo = new TreeMap<String, HashMap<String, Object>>();

    public String ziprootPath;

    private final String temporary_uploaded_file_key;

    private BandanaManager bandanaManager;

    public ImportditaActionImpl() {
        this.temporary_uploaded_file_key = "import.dita.zipfile.uploaded.path";
    }

    /**
     * Set the value of bandanaManager
     *
     * @param bandanaManager
     */
    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    private ContentPropertyManager contentPropertyManager;

    /**
     * Set the value of bandanaManager
     *
     * @param contentPropertyManager
     */
    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    private String importComment;

    /**
     * Get the value of importComment
     *
     * @return the value of importComment
     */
    public String getImportComment() {
        return importComment;
    }

    /**
     * Set the value of importComment
     *
     * @param importComment new value of importComment
     */
    public void setImportComment(String importComment) {
        this.importComment = importComment;
    }

    /**
     * トピックIDの重複があった場合のエラー
     */
    public boolean duplicateTopicIdError = false;

    /**
     * 添付ファイルの位置決めオプション
     */
    private String hierarchyRuleOption = "";

    /**
     * DitaMapファイルを持っているかどうか
     */
    private boolean hasMapFile = false;

    /**
     * Get the value of hierarchyRuleOption
     *
     * @return the value of hierarchyRuleOption
     */
    public String gethierarchyRuleOption() {
        return hierarchyRuleOption;
    }

    /**
     * Set the value of hierarchyRuleOption
     *
     * @param hierarchyRuleOption new value of hierarchyRuleOption
     */
    public void sethierarchyRuleOption(String hierarchyRuleOption) {
        this.hierarchyRuleOption = hierarchyRuleOption;
    }

    /**
     * importする際に、階層化するかしないかのオプションの選択肢
     *
     * @return the value of hierarchyRuleOption
     */
    public List<PairType> getHierarchyRuleOptions() {
        List<PairType> hierarchyRuleOptions = new ArrayList<PairType>();
        hierarchyRuleOptions.add(
                new PairType(
                        DitaImportUtils.IMPORT_HIERARCHY_OPTION_FLAT,
                        getText("conf2dita.action.import.options.hierarchy.rule.flat.label")));
        hierarchyRuleOptions.add(
                new PairType(
                        DitaImportUtils.IMPORT_HIERARCHY_OPTION_DIRSTRUCTURE,
                        getText("conf2dita.action.import.options.hierarchy.rule.dirstructure.label")));
        if (hasMapFile) {
            hierarchyRuleOptions.add(
                    new PairType(
                            DitaImportUtils.IMPORT_HIERARCHY_OPTION_MAPFILE,
                            getText("conf2dita.action.import.options.hierarchy.rule.mapfile.label")));
        }
        return hierarchyRuleOptions;
    }

    public String getHierarchyRuleOptionDisplay() {
        return getText("conf2dita.action.import.options.hierarchy.rule." + hierarchyRuleOption + ".label");
    }

    /**
     * @action ファイルを選択する画面
     * @return
     * @throws Exception
     */
    public String selectFile() throws Exception {
        return "success";
    }

    /**
     * POSTメソッドのみ許可
     *
     * @action アップロードを行った後の画面
     * @return
     */
    public String uploadFile() {
        String method = ServletActionContext.getRequest().getMethod();

        if (!method.equalsIgnoreCase("post")) {
            return "input";
        }

        FileUploadUtils.UploadedFile uploadedFile;

        try {
            uploadedFile = FileUploadUtils.getSingleUploadedFile();
            if (uploadedFile != null) {
                File upfile = upload(uploadedFile);
                validateAfterUpload();
                if (upfile != null && !hasErrors()) {
                    DitaImportUtils diu = assignUploadFileList(upfile);
                    // トピックIDチェック
                    validateZipListInfo();
                    if (!hasErrors()) {
                        session.put(this.temporary_uploaded_file_key, upfile.getAbsolutePath());
                    }
                }
            } else {
                addFieldError("file", getText("conf2dita.action.import.file.error.required"));
            }
        } catch (FileUploadUtils.FileUploadException e) {
            addFieldError("file", getText("conf2dita.action.import.file.invalidfile") + e.getMessage());
        } catch (IOException ex) {
            addActionError(getText("conf2dita.action.import.file.invalidfile") + ex.getMessage());
        }

        if (hasErrors()) {
            return INPUT;
        }

        // default
        if (this.hasMapFile) {
            hierarchyRuleOption = DitaImportUtils.IMPORT_HIERARCHY_OPTION_MAPFILE;
        } else {
            hierarchyRuleOption = DitaImportUtils.IMPORT_HIERARCHY_OPTION_FLAT;
        }

        return SUCCESS;
    }

    public String importDo() {
        String tmp_file_path = session.get(this.temporary_uploaded_file_key).toString();
        File tmp_file = new File(tmp_file_path);
        if (JZipFileUtil.isZipFile(tmp_file)) {
            try {
                DitaImportUtils diu = assignUploadFileList(tmp_file);
                if (StringUtils.isEmpty(importComment)) {
                    addFieldError("importComment", getText("conf2dita.action.import.comment.error.required"));
                }
                if (StringUtils.isEmpty(hierarchyRuleOption)) {
                    addFieldError("hierarchyRuleOption", getText("conf2dita.action.import.options.hierarchy.error.required"));
                } else if (!JConfluenceHTTPUtil.inSelectOptions(hierarchyRuleOption, getHierarchyRuleOptions())) {
                    addFieldError("hierarchyRuleOption", getText("conf2dita.action.import.options.hierarchy.error.invalid"));
                }
                if (!hasErrors()) {
                    doImportProcess(diu, tmp_file_path);
                }
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        } else {
            addActionError(getText("conf2dita.action.import.file.notexists"));
            return "upload";
        }

        if (hasErrors()) {
            return INPUT;
        }

        return SUCCESS;
    }

    /**
     * アップロードの実務処理
     *
     * @param uploadedFile アップロードファイル
     * @throws IOException ZIPファイルでなかったり、ファイルが無効だったり
     */
    private File upload(final FileUploadUtils.UploadedFile uploadedFile) throws IOException {

        File upfile_dest = null;
        try {
            File upfile = uploadedFile.getFile();
            if (JZipFileUtil.isZipFile(upfile) == false) {
                addActionError(getText("conf2dita.action.import.file.invalidfile"));
            } else {
                upfile_dest = new File(upfile.getAbsolutePath() + "2");
                if (!upfile.getName().endsWith(".tmp")) {
                    throw new IOException("upfile not tmp name.");
                }
                if (!upfile.renameTo(upfile_dest)) {
                    throw new IOException("rename failed");
                }
            }
        } catch (UnsupportedOperationException e) {
            addActionError(getText("error", new Object[]{uploadedFile.getContentType()}));
        }

        return upfile_dest;
    }

    /**
     * ZIPファイルからページを作ったりする。
     *
     * @param zipFile
     */
    private void doImportProcess(DitaImportUtils diu, String zipFile) throws IOException, Exception {
        ConfluenceUser user = getAuthenticatedUser();

        // ZIP展開して展開リストを作成
        diu.unZip();
        ArrayList<DitaFileSet> zipStratifyList;
        if (hierarchyRuleOption.equals("mapfile")) {
            String mapFilePath = diu.unZipDirectory + File.separator + diu.getMapListInfo(pageManager, page, contentPropertyManager).firstKey();
            File mapFile = new File(mapFilePath); // TODO
            MapDocument map = MapDocument.load(FileUtils.readFileToString(mapFile, "UTF-8"), contentPropertyManager);
            diu.setMapFile(mapFile);
            zipStratifyList = diu.stratifyZipFile(map, map.getRootElement());
        } else {
            zipStratifyList = diu.stratifyZipFile(null, hierarchyRuleOption.equals(DitaImportUtils.IMPORT_HIERARCHY_OPTION_FLAT));
        }

        // インポートの初期親階層の設定。トップ階層に現在のページと同じPageId(orタイトル)があった場合は1階層上にする
        Page parentPage = (Page) page;
        for (DitaFileSet dfs : zipStratifyList) {
            if (dfs.getPageId() == page.getId() || dfs.getPageTitle().equals(page.getTitle())) {
                parentPage = ((Page) page).getParent();
                break;
            }
        }
        // インポートする。
        for (DitaFileSet dfs : zipStratifyList) {
            dfs.importToPage(pageManager, labelManager, bandanaManager, contentPropertyManager, getSpace(), parentPage, user, importComment);
        }
    }

    public boolean isEditMode() {
        return true;
    }

    /**
     * アップロードしたファイルの情報をAssignする
     *
     * @param upfile
     */
    private DitaImportUtils assignUploadFileList(File upfile) {

        DitaImportUtils diu = new DitaImportUtils(upfile, pageManager, page, contentPropertyManager);
        List<ZipEntry> zipEntryList = diu.getZipEntryList();
        if (zipEntryList.isEmpty()) {
            addFieldError("file", getText("conf2dita.action.import.file.invalidfile"));
            return diu;
        }

        ziprootPath = diu.getZipRootDirectory();
        try {
            ziplistinfo = diu.getZipListInfo(pageManager, page);
            maplistinfo = diu.getMapListInfo(pageManager, page, contentPropertyManager);
        } catch (IOException ex) {
            addActionError(getText("conf2dita.action.import.file.error.inzipentry", new Object[]{"zip entry", ex.getMessage()}));
        } catch (Exception ex) {
            addActionError(getText("conf2dita.action.import.file.error.inzipentry", new Object[]{"zip entry", ex.getMessage()}));
        }

        // mapfileは1つのみ
        if (maplistinfo.size() == 1) {
            hasMapFile = true;
        } else if (maplistinfo.size() > 1) {
            addFieldError("file", getText("conf2dita.action.import.file.error.mapfile.duplicate"));
        }
        return diu;
    }

    /**
     *
     * @return
     */
    public ContentEntityObject getContentEntityObject() {
        return getPage();
    }

    /**
     *
     * @return
     */
    @Override
    public AbstractPage getPage() {
        return page;
    }

    /**
     *
     * @param page
     */
    @Override
    public void setPage(AbstractPage page) {
        this.page = page;
    }

    @Override
    public boolean isPageRequired() {
        return true;
    }

    @Override
    public boolean isLatestVersionRequired() {
        return true;
    }

    @Override
    public boolean isViewPermissionRequired() {
        return true;
    }

    @Override
    public void setSession(Map map) {
        this.session = map;
    }

    /**
     * Validatorはオートで行われるのでアップしたFileをチェックできないことに注意、プロパティならみられる。
     */
    @Override
    public void validate() {
        // this.addActionError(getText("setup.mail.server.not.specified"));
        // this.addFieldError("email", "conf2dita.action.import.email.error", new Object[]{getEmail()});
    }

    /**
     * ZIPファイルをアップロードした後でValidateする。エラーがあれば、addFieldError/addActionErrorする。
     */
    protected void validateAfterUpload() {
    }

    /**
     * ZIPファイルの中身を解析した後でValidateする。このメソッドではziplistinfoプロパティについて解析する。エラーがあれば、addFieldError/addActionErrorする。
     */
    protected void validateZipListInfo() {
        checkZipListTopicId();
    }

    /**
     * ziplistinfoに入っているZIPの内容についてTopicIDの重複チェックを行う
     * 重複チェックは、同一スペースの既存のページに振られているTopicIDについて
     */
    private void checkZipListTopicId() {
        String topic_id ;
        String dita_title;
        HashMap<String, Object> zipinfo;
        // topic_idに重複が無いかチェック
        for (String zip_name : ziplistinfo.keySet()) {
            zipinfo = ziplistinfo.get(zip_name);
            topic_id = (String) zipinfo.get("dita_topic_id");
            dita_title = (String) zipinfo.get("dita_title");
            if (!StringUtils.isEmpty(topic_id)) {
                ConfluencePageCache CPCache = ConfluencePageCache.getInstance(pageManager, contentPropertyManager);
                ConfluencePageCache.PageInfo pageInfo = CPCache.getPageIdByTopicId(getSpace(), topic_id);
                // 重複したID調査
                if (pageInfo != null) {
                    if (true || !StringUtils.equals(dita_title, pageInfo.getTitle())) {
                        // でもページタイトルが同じならOK(上書きするので)
                        addFieldError("topicIdDuplicate", getText("conf2dita.action.import.file.error.topicidcheck"));
                        zipinfo.put("topicidcheck", pageInfo);
                    }
                }
            }
        }
    }

}
