/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.xwork.pages;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.opensymphony.webwork.interceptor.SessionAware;
import java.util.Map;
import jp.junoe.confluence.plugins.conf2dita.DitaExportUtils;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaMapErrorException;
import jp.junoe.confluence.plugins.conf2dita.dita.MapDocument;

/**
 *
 * @author Takashi
 */
public class ViewditamapAction extends ViewPageAction implements PageAware, SessionAware {

    private AbstractPage page;

    private Map session;

    public String ditamapString;

    /**
     * Get the value of page
     *
     * @return the value of page
     */
    @Override
    public AbstractPage getPage() {
        return page;
    }

    /**
     * Set the value of page
     *
     * @param page new value of page
     */
    @Override
    public void setPage(AbstractPage page) {
        this.page = page;
    }

    @Override
    public void setSession(Map map) {
        this.session = map;
    }

    private ContentPropertyManager contentPropertyManager;

    /**
     * Get the value of contentPropertyManager
     *
     * @return the value of contentPropertyManager
     */
    public ContentPropertyManager getContentPropertyManager() {
        return contentPropertyManager;
    }

    /**
     * Set the value of contentPropertyManager
     *
     * @param contentPropertyManager new value of contentPropertyManager
     */
    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    /**
     * ditamapを表示する
     *
     * @return
     */
    @Override
    public String execute() {
        try {
            Page _page ;
            if (page instanceof Page){
                _page = (Page) page;
                DitaExportUtils deu = new DitaExportUtils(_page, contentPropertyManager, pageManager, _page.isHomePage());
                MapDocument ditamap = MapDocument.create(deu, _page, "map", contentPropertyManager);
                ditamapString = ditamap.asXML(true);
            }
        } catch (DitaMapErrorException ex) {

        }

        return SUCCESS;
    }
}
