/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.listener;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.label.LabelAddEvent;
import com.atlassian.confluence.event.events.label.LabelCreateEvent;
import com.atlassian.confluence.event.events.label.LabelDeleteEvent;
import com.atlassian.confluence.event.events.label.LabelEvent;
import com.atlassian.confluence.event.events.label.LabelRemoveEvent;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelable;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import java.util.List;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;
import jp.junoe.confluence.plugins.conf2dita.event.DitaPageSettingEvent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

/**
 *
 * @author Takashi
 */
public class LabelListener implements DisposableBean {

    /**
     * イベントパブリッシャ
     */
    protected EventPublisher eventPublisher;

    private final ContentPropertyManager cm;

    private final LabelManager lm;

    private final Logger log;

    private static final String DITA_TOPIC_TYPE_GENERATOR_LABEL = "Label";

    public LabelListener(EventPublisher eventPublisher, LabelManager lm, ContentPropertyManager cm) {
        this.eventPublisher = eventPublisher;
        this.cm = cm;
        this.lm = lm;
        this.log = LoggerFactory.getLogger(this.getClass());
        eventPublisher.register(this);
    }

    /**
     * ラベル追加時に動作
     *
     * @param event
     */
    @EventListener
    public void onAddLabelHandler(LabelAddEvent event) {
        Labelable labelled = event.getLabelled();
        ContentEntityObject page;
        if (labelled instanceof ContentEntityObject) {
            page = (ContentEntityObject) labelled;
            String topicType = getTopicType(event);
            if (!StringUtils.isEmpty(topicType)) {
                log.info(String.format("set dita topictype [%s] => %s", topicType, page.getTitle()));
                eventPublisher.publish(new DitaPageSettingEvent(this, page, topicType, DITA_TOPIC_TYPE_GENERATOR_LABEL));
            }
        }
    }

    @EventListener
    public void onCreateLabelHandler(LabelCreateEvent event) {
    }

    @EventListener
    public void onRemoveLabelHandler(LabelRemoveEvent event) {
        Labelable labelled = event.getLabelled();
        ContentEntityObject page;
        String topicType = "";
        String currentTopicType = "";
        String _tt = "";
        // not space but page
        if (labelled instanceof ContentEntityObject) {
            page = (ContentEntityObject) labelled;

            // もし削除したLabelのTopicTypeと今このページが持っているTopicTypeが同じ場合は残ったもので再設定
            topicType = getTopicType(event);
            currentTopicType = DitaTopicType.load(page, cm).getTopicType();
            if (!StringUtils.isEmpty(currentTopicType) && StringUtils.equals(topicType, currentTopicType)) {
                topicType = "";
                // get all labelled
                List<Label> labels = page.getLabels();
                for (Label l : labels) {
                    _tt = getTopicType(l);
                    if (!StringUtils.isEmpty(_tt)) {
                        topicType = _tt;
                    }
                }
                log.info(String.format("Remove label and dita topictype change [%s] to [%s] => %s", currentTopicType, topicType, page.getTitle()));
                eventPublisher.publish(new DitaPageSettingEvent(this, page, topicType, DITA_TOPIC_TYPE_GENERATOR_LABEL));
            }
        }
    }

    @EventListener
    public void onDeleteLabelHandler(LabelDeleteEvent event) {
    }

    /**
     * LabelEventオブジェクトからDitaのトピックタイプを取得する
     *
     * @param label
     * @return
     */
    private String getTopicType(LabelEvent event) {
        String topicType = "";

        Label label = null;
        ContentEntityObject page = null;
        Labelable labelled = event.getLabelled();

        Object source = event.getSource();
        if (source instanceof Label) {
            // labeled to page 
            if (labelled instanceof ContentEntityObject) {
                label = (Label) source;
                topicType = getTopicType(label);
            }
        }

        return topicType;
    }

    /**
     * LabelオブジェクトからDitaのトピックタイプを取得する
     *
     * @param label
     * @return
     */
    private String getTopicType(Label label) {
        String topicType = "";
        if (label.getName().startsWith(Conf2dita.DITA_TOPIC_TYPE_LABEL_PREFIX)) {
            topicType = label.getName().substring(Conf2dita.DITA_TOPIC_TYPE_LABEL_PREFIX.length());
        }
        return topicType;
    }

    @Override
    public void destroy() throws Exception {
        this.eventPublisher.unregister(this);
    }

}
