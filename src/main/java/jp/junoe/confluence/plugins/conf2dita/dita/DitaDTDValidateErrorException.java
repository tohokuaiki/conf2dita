package jp.junoe.confluence.plugins.conf2dita.dita;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.junoe.confluence.plugins.JConfluenceXSLTUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTD;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * バリデートエラーのメッセージをあれこれするための例外クラス
 *
 * @author Takashi
 */
public class DitaDTDValidateErrorException extends DocumentException {

    private final String ditaxml;

    /**
     *
     * @param message
     * @param dita
     */
    public DitaDTDValidateErrorException(String message, Document dita) {
        super(message);
        try {
            dita = DitaDTD.replaceToPublicDTD(dita);
        } catch (IOException ex) {
        }
        ditaxml = JConfluenceXSLTUtil.getFormattedXML(dita);
    }

    /**
     * SaxonのDTDチェックによるエラーメッセージを翻訳するためのmap
     */
    private static final Map<MessageFormat, String> saxonDTDErrorFormats = new HashMap<MessageFormat, String>() {
        {
            // 何行目のどこにエラーか
            put(new MessageFormat("Recoverable error on line {0} column {1}"),
                    "conf2dita.error.saxon.dtd.position");
            // 未定義要素・属性値の使用
            put(new MessageFormat("SXXP0003: Error reported by XML parser: Element type \"{0}\" must be declared."),
                    "conf2dita.error.saxon.dtd.notdeclared");
            // 不適切な場所に要素がある
            put(new MessageFormat("SXXP0003: Error reported by XML parser: The content of element type \"{0}\" must match"),
                    "conf2dita.error.saxon.dtd.invalid.position");
        }
    };

    /**
     * DTDエラーメッセージをわかりやすく表示
     *
     * @param appendXml DITAをエラーメッセージに付ける
     * @param asHtml HTML形式
     * @return
     */
    public String getDTDErrorMessage(boolean appendXml, boolean asHtml) {
        String message = getMessage();
        String message_r = "";
        MessageFormat mf;
        I18NBean i18n = GeneralUtil.getI18n();

        Set<Map.Entry<MessageFormat, String>> errorFormatSet = DitaDTDValidateErrorException.saxonDTDErrorFormats.entrySet();

        for (String s : StringUtils.split(message, "\n")) {
            s = StringUtils.trim(s);
            boolean matchFlag = false;
            for (Map.Entry<MessageFormat, String> e : errorFormatSet) {
                mf = e.getKey();
                try {
                    Object[] result = mf.parse(s);
                    message_r += i18n.getText(e.getValue(), result);
                    matchFlag = true;
                    break;
                } catch (ParseException ex) {
                }
            }
            if (!matchFlag) {
                message_r += s;
            }
            message_r += "\n";
        }

        if (appendXml) {
            // Validate対象となったXMLを付ける
            String ditaxml_r = "";
            String ditaxml_p = StringUtils.replace(ditaxml, "\n\n", "\n \n");
            int xmlLines = Integer.toString(StringUtils.split(ditaxml_p, "\n").length).length();
            String lineFormat = "%0" + xmlLines + "d. ";

            if (asHtml) {
                lineFormat = "<span class=\"linenumber\"><b>" + lineFormat + "</b></span>";
            }
            int i = 1;
            for (String s : StringUtils.split(ditaxml_p, "\n")) {
                if (asHtml) {
                    s = StringEscapeUtils.escapeHtml(s);
                }
                ditaxml_r += String.format(lineFormat, i++) + s + "\n";
            }
            if (asHtml) {
                message_r = StringUtils.replace(message_r, "\n", "<br />\n");
                message_r += "<h4>" + i18n.getText("conf2dita.error.saxon.dtd.ditaxml.label") + "</h4>\n";
                message_r += "<pre>" + ditaxml_r + "</pre>";
            } else {
                message_r += "\n*" + i18n.getText("conf2dita.error.saxon.dtd.ditaxml.label") + ":*";
                message_r += "\n" + ditaxml_r;
            }
        }

        return message_r;
    }
}
