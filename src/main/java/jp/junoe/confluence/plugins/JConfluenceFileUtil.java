/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins;

import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.util.ConfluenceHomeGlobalConstants;
import com.atlassian.core.util.DataUtils;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Confluenceのファイルなどを扱っていて便利なメソッド
 *
 * @author Takashi
 */
public class JConfluenceFileUtil {

    /**
     * Used to ensure unique files names for each id created.
     */
    private static final AtomicLong uniqueId = new AtomicLong(1);

    /**
     * テンポラリなファイル名を作成する
     *
     * @param page
     * @param prefix
     * @return
     */
    public static String getTemporaryFileName(Page page, String prefix) {
        String spaceKey = page.getSpaceKey();
        if (spaceKey.startsWith("~")) {
            spaceKey = spaceKey.substring(1);
        }

        Date now = new Date();
        String fileName
                = prefix + "-" + spaceKey + "-" + MessageFormat.format("{0,date,yyyyMMdd}", new Object[]{now, now}) + "-" + uniqueId.incrementAndGet() + DataUtils.SUFFIX_ZIP;
        return fileName;
    }

    /**
     * Confluenceのテンポラリディレクトリを返す
     *
     * @return
     */
    public static String getTemporaryDir() {
        AtlassianBootstrapManager bm;
        bm = BootstrapUtils.getBootstrapManager();
        String dir = bm.getApplicationHome() + "/" + ConfluenceHomeGlobalConstants.TEMP_DIR;
        return dir;
    }

    /**
     * Directoryのセパレータを\と/が混ざっているのを統一する。
     *
     * @param dir
     * @return
     */
    public static String normalizeDirectorySeparator(String dir) {
        dir = dir.replace("\\", File.separator).replace("/", File.separator);
        return dir;
    }

    /**
     * 2つのディレクトリのパスを合わせる。<br>
     * 例）foo/bar, foo/bar/attachments が来た場合は、"", "attachments"を返却する<br>
     * 例）foo/bar, attachments/foo/bar が来た場合は、foo/bar, attachments/foo/barを返却する
     *
     * @param dir1
     * @param dir2
     * @return 引数の順番にStringが入ったもの
     */
    public static List<String> syncDirectoryPath(String dir1, String dir2) {
        List<String> dirs = new ArrayList<String>();

        dir1 = normalizeDirectorySeparator(dir1);
        dir1 = StringUtils.strip(dir1, File.separator);
        dir2 = normalizeDirectorySeparator(dir2);
        dir2 = StringUtils.strip(dir2, File.separator);

        String[] dir1_a = StringUtils.split(dir1, File.separator);
        String[] dir2_a = StringUtils.split(dir2, File.separator);
        int syncSize = 0;
        for (int i = 0; (i < dir1_a.length && i < dir2_a.length); i++) {
            if (dir2_a[i] != null) {
                if (dir2_a[i].equals(dir1_a[i])) {
                    syncSize++;
                } else {
                    break;
                }
            }
        }

        String[] dir1_b = new String[dir1_a.length - syncSize];
        dir1_b = (String[]) ArrayUtils.subarray(dir1_a, syncSize, dir1_a.length);
        String[] dir2_b = new String[dir2_a.length - syncSize];
        dir2_b = (String[]) ArrayUtils.subarray(dir2_a, syncSize, dir2_a.length);

        dirs.add(StringUtils.join(dir1_b, File.separator));
        dirs.add(StringUtils.join(dir2_b, File.separator));

        return dirs;
    }

    /**
     * ディレクトリ1からファイル2を見た時に、相対パス的にみるとどう表現できるかを示す
     *
     * @param path1 自分が見るディレクトリの位置(ファイルは指定しないこと)
     * @param path2 2のファイル絶対パス(ディレクトリは指定しないこと)
     * @return
     */
    public static String getRelativeDiffPath(String path1, String path2) {
        String diffpath = "";

        // ノーマライズ
        path1 = StringUtils.strip(normalizeDirectorySeparator(path1), File.separator);
        path2 = StringUtils.strip(normalizeDirectorySeparator(path2), File.separator);

        String[] dir1 = StringUtils.split(path1, File.separator);
        String[] dir2 = StringUtils.split(path2, File.separator);

        int end = dir2.length > dir1.length ? dir1.length : dir2.length;
        int cutNum = 0;
        for (int i = 0; i < end; i++) {
            if (!dir1[i].equals(dir2[i])) {
                break;
            }
            cutNum++;
        }

        String[] dir1a, dir2a;
        dir1a = (String[]) ArrayUtils.subarray(dir1, cutNum, dir1.length);
        dir2a = (String[]) ArrayUtils.subarray(dir2, cutNum, dir2.length);

        if (dir1a.length > 0) {
            diffpath = StringUtils.repeat(".." + File.separator, dir1a.length);
        }
        diffpath += StringUtils.join(dir2a, File.separator);

        return diffpath;
    }
}
