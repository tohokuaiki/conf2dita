<?xml version="1.0" encoding="UTF-8"?><!-- 雀の往来 -->
<xsl:stylesheet
	version="2.0"
	xmlns:h="http://www.atlassian.com/schema/confluence/4/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/"
	xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/"
	xmlns:func="http://exslt.org/functions"
	exclude-result-prefixes="h xsl xs ac ri func"
	>
	
	<xsl:import href="conf2ditaFunctions.xsl"/>
	
	<!-- tableで使うdita属性値のprefix -->
	<xsl:variable name="ditaAttributePrefix">ditaProperty</xsl:variable>
	
	<!-- 調整用のマクロパラメータのprefix -->
	<xsl:variable name="controllParameterPrefix">conf2dita</xsl:variable>
	
	<!-- tbody要素を渡して、その1列目がthのみであればtrueを返す  -->
    <xsl:function name="func:hasTableHeader" as="xs:boolean">
		<xsl:param name="tbody" as="element()"/>
		<!-- 2つ以上の時にチェック -->
		<xsl:choose>
			<xsl:when test="count($tbody/h:tr)>=2">
				<!-- 1列目が全てth要素であること -->
				<xsl:choose>
					<xsl:when test="count($tbody/h:tr[1]/node()) = count($tbody/h:tr[1]/h:th)">
						<xsl:value-of select="true()"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="false()"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	
	<!-- XSLT実行時パラメータ -->
	<xsl:param name="dtd_path"/>
	
	<xsl:variable name="topictype">
		<xsl:choose>
			<xsl:when test="/ac:confluence/@dita-topictype">
				<xsl:value-of select="/ac:confluence/@dita-topictype"/>
			</xsl:when>
			<xsl:otherwise>topic</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="Topictype" select="func:ucfirst($topictype)"/>
	<xsl:variable name="bodytag">
		<xsl:choose>
			<xsl:when test="$topictype='concept'">conbody</xsl:when>
			<xsl:when test="$topictype='reference'">refbody</xsl:when>
			<xsl:when test="$topictype='task' or $topictype='generaltask'">taskbody</xsl:when>
			<xsl:when test="$topictype='glossary'">glossbody</xsl:when>
			<xsl:when test="$topictype='topic'">body</xsl:when>
			<xsl:otherwise>body</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="roottag">
		<xsl:choose>
			<xsl:when test="$topictype='generaltask'">task</xsl:when>
			<xsl:otherwise><xsl:value-of select="$topictype" /></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	
	<xsl:variable name="dita_dtd_path">
		<xsl:choose>
			<xsl:when test="$dtd_path">
				<xsl:value-of select="concat($dtd_path, '/', $topictype, '.dtd')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat('http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/', $topictype, '.dtd')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<!-- xsl:output 
		method="xml" omit-xml-declaration="no" encoding="UTF-8" indent="yes"
		doctype-system="http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/${topictype}.dtd"
		doctype-public="-//OASIS//DTD DITA ${topictype}//EN"/ -->
	
	<xsl:template match="/ac:confluence">
	    <xsl:result-document
			doctype-system="{$dita_dtd_path}"
			doctype-public="-//OASIS//DTD DITA {$Topictype}//EN"
			method="xml"
			omit-xml-declaration="no" encoding="UTF-8" indent="yes"
			>
			<xsl:element name="{$roottag}">
				<xsl:attribute name="id"><xsl:value-of select="/ac:confluence/@dita-pageid"/></xsl:attribute>
				<xsl:attribute name="xml:lang">ja-jp</xsl:attribute>
				<!-- title -->
				<title><xsl:value-of select="/ac:confluence/@dita-title"/></title>
				<!-- shortdesc -->
				<xsl:apply-templates select="node()[@ac:name='dita-shortdesc']"/>
				<!-- abstract -->
				<xsl:apply-templates select="node()[@ac:name='dita-abstract']"/>
				<!-- prolog -->
				<xsl:apply-templates select="node()[@ac:name='dita-prolog']"/>
				<!-- body -->
				<xsl:element name="{$bodytag}">
					<xsl:apply-templates select="node()[not(@ac:name='dita-shortdesc') and not(@ac:name='dita-abstract') and not(@ac:name='dita-prolog')]"/>
 				</xsl:element>
			</xsl:element>
		</xsl:result-document>
	</xsl:template>

	<!-- dita-macro -->
	<xsl:template name="confluence-macro" match="ac:structured-macro[matches(@ac:name, '^dita-(.*)$')]">
		<xsl:variable name="dita-element" select="substring(string(@ac:name), 6)"/>
		<!-- xsl:message>dita element[<xsl:value-of select="$dita-element"/>]</xsl:message -->
		<xsl:choose>
			<!-- コメント -->
			<xsl:when test="$dita-element = 'comment'">
				<xsl:comment><xsl:value-of select="ac:parameter[@ac:name='text']"/></xsl:comment>
			</xsl:when>
			<!-- ****-wrapper 系マクロの場合はスルー -->
			<xsl:when test="ends-with($dita-element, '-wrapper')">
				<xsl:message>dita element through => [<xsl:value-of select="$dita-element"/>]</xsl:message>
				<xsl:apply-templates/>
			</xsl:when>
			<!-- 特殊ハック1 -->
			<xsl:when test="$dita-element = 'xxcmd' and count(ac:rich-text-body/h:ol) > 0">
				<xsl:apply-templates select="." mode="smartdita-specialized"/>
			</xsl:when>
			<!-- /特殊ハック1 -->
			<xsl:otherwise>
				<xsl:element name="{$dita-element}">
					<!-- 先にAttributeを充てる -->
					<xsl:apply-templates select="ac:parameter[not(@ac:name='title' or @ac:name='contents')]" mode="ditaParameter"/>
					<!-- title属性とcontents属性は特別 -->
					<xsl:apply-templates select="ac:parameter[@ac:name='title']" mode="ditaParameter"/>
					<xsl:apply-templates select="ac:parameter[@ac:name='contents']" mode="ditaParameter"/>
					<!-- body allow <p> or not -->
					<xsl:choose>
						<xsl:when test="$dita-element = 'shortdesc' or $dita-element = 'abstract' or $dita-element = 'dt'">
							<xsl:value-of select="."/>
							<!--- xsl:copy-of select="."/ -->
						</xsl:when>
						<xsl:when test="$dita-element = 'codeblock' or $dita-element = 'screen' or $dita-element = 'msgblock' or $dita-element = 'pre' or $dita-element = 'cmd'">
							<xsl:apply-templates mode="dita-codeblock" select="ac:rich-text-body"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="ac:rich-text-body"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- dita-macro parameter -->
	<xsl:template match="ac:parameter" mode="ditaParameter">
		<xsl:variable name="paramname" select="func:adjustParameterName(@ac:name)"/>
		<xsl:choose>
			<xsl:when test="starts-with($paramname, $controllParameterPrefix)"/>
			<xsl:when test="$paramname = 'title'">
				<title><xsl:value-of select="."/></title>
			</xsl:when>
			<xsl:when test="$paramname = 'contents'">
				<xsl:value-of select="."/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="{$paramname}" select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- macro paramter body (rich-body) -->
	<xsl:template name="rich-body" match="ac:rich-text-body">
		<xsl:apply-templates select="* | @* |  node()"/>
	</xsl:template>
	
	<!-- image macro -->
	<xsl:template name="ac-image" match="ac:image">
		<xsl:element name="image">
			<xsl:apply-templates select="ac:parameter" mode="ditaParameter"/>
			<xsl:attribute name="href">
				<!--
				<xsl:value-of select="string(ri:attachment/@ri:id)"/>
				<xsl:text>/</xsl:text> -->
				<!-- <xsl:if test="ri:attachment/ri:page/@ri:content-title!='_Images'">
					<xsl:value-of select="string(ri:attachment/ri:page/@ri:content-title)"/>
					<xsl:text>/</xsl:text>
				</xsl:if> -->
				<xsl:value-of select="ri:attachment/@ri:filename"/>
			</xsl:attribute>
			<xsl:if test="@ac:width"><xsl:attribute name="width">
				<xsl:value-of select="@ac:width"/>
			</xsl:attribute></xsl:if>
			<xsl:if test="@ac:align"><xsl:attribute name="align">
				<xsl:value-of select="@ac:align"/>
			</xsl:attribute></xsl:if>
			<xsl:if test="@ac:alt"><xsl:element name="alt">
				<xsl:value-of select="@ac:alt"/>
			</xsl:element></xsl:if>
		</xsl:element>
	</xsl:template>
	
	
	<!-- dita-codeblock などの処理：改行<p>や<br/>を\nにする -->
	<xsl:template mode="dita-codeblock" match="*">
		<xsl:for-each select="*|text()">
			<xsl:choose>
				<xsl:when test=".='&#10;'"/><!-- Pタグの境目の改行コードは除去 -->
				<xsl:when test="lower-case(name()) = 'p'">
					<xsl:apply-templates mode="dita-codeblock" select="."/>
					<xsl:if test="func:index-of-node(../h:p, .) != count(../h:p)"><!-- 最後のPタグでない場合以外はPの終わりは改行として処理する  -->
						<xsl:value-of select="'&#10;'"/>
					</xsl:if>
				</xsl:when>
				<xsl:when test="lower-case(name()) = 'br'">
					<xsl:value-of select="'&#10;'"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- xsl:value-of select="normalize-space()"/ -->
					<xsl:apply-templates select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	
	
	<!-- 全部の属性値に関する処理: 基本的にConfluenceの与える属性値はDita側に持ち込まない -->
	<xsl:template match="h:*/@*"><!-- 
		<xsl:comment><xsl:value-of select="name()"/><xsl:value-of select="."/></xsl:comment>
	--></xsl:template>
	
	<!-- 属性値をあえて与えたい場合はこのモードを使う(table/list) -->
	<xsl:template mode="applyDitaAttributes" match="h:*/@*">
		<xsl:if test="starts-with(name(), $ditaAttributePrefix)">
			<xsl:variable name="attrName">
				<xsl:value-of select="func:adjustParameterName(substring(name(), string-length($ditaAttributePrefix)+1))"/>
			</xsl:variable>
			<xsl:attribute name="{$attrName}"><xsl:value-of select="."/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	
	
	<!-- xhtml element p -->
	<xsl:template match="h:p">
      <xsl:choose>
		  <!-- 空のPタグは除去 -->
          <xsl:when test="func:isEmptyTag(.)"/>
		  <!-- tableセル内のpタグは無意味なので中身だけ取り出す -->
		  <!-- dita-titleは中身だけ取り出す -->
          <xsl:when test="
			  ((parent::h:td | parent::h:th) and count(parent::*[1]/h:p) = 1) or 
			  (name(../..)='ac:structured-macro' and ../../@ac:name='dita-title')
			  ">
			  <xsl:apply-templates select="@* | node()"/>
          </xsl:when>
          <xsl:otherwise>
              <p>
                  <xsl:apply-templates select="@* | node()"/>
              </p>
          </xsl:otherwise>
      </xsl:choose>
    </xsl:template>
	
	<!-- xhtml element table -->
    <xsl:template match="h:table">
		<xsl:variable name="tabletype">
			<xsl:choose>
				<xsl:when test="../../name() = 'ac:structured-macro' and 
								starts-with(string(../../@ac:name), 'dita-') ">
					<xsl:value-of select="substring(string(../../@ac:name), 6)"/>
				</xsl:when>
				<xsl:otherwise>table</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$tabletype = 'choicetable'">
				<xsl:apply-templates mode="choicetableContents"/>
			</xsl:when>
			<xsl:when test="$tabletype = 'simpletable'">
				<xsl:apply-templates mode="simpletableContents"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates mode="tableContents"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
  	<!-- テーブル(choicetable)の処理  -->
	<xsl:template mode="choicetableContents" match="h:tbody">
		<xsl:variable name="hasHeader" select="func:hasTableHeader(.)"/>
		<xsl:for-each select="h:tr">
			<xsl:choose>
				<xsl:when test="$hasHeader and position()=1">
					<chhead>
						<xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
						<xsl:apply-templates select="*" mode="choicetableEntry">
							<xsl:with-param name="isHeader" select="true()"/>
						</xsl:apply-templates>
					</chhead>
				</xsl:when>
				<xsl:otherwise>
					<chrow>
						<xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
						<xsl:apply-templates select="*" mode="choicetableEntry">
							<xsl:with-param name="isHeader" select="false()"/>
						</xsl:apply-templates>
					</chrow>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<!-- テーブル(choicetable-entry)の処理  -->
	<xsl:template mode="choicetableEntry" match="h:th|h:td">
		<xsl:param name="isHeader" as="xs:boolean"/>
		<xsl:variable name="entrytag">
			<xsl:choose>
				<xsl:when test="position() = 1">choption</xsl:when>
				<xsl:otherwise>chdesc</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$isHeader">hd</xsl:if>
		</xsl:variable>
		<xsl:element name="{$entrytag}">
			<xsl:apply-templates mode="applyDitaAttributes" select="@*[not(name()='rowspan') and not(name()='colspan')]"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<!-- テーブル(simpletable)の処理  -->
  	<xsl:template mode="simpletableContents" match="h:tbody">
		<xsl:variable name="hasHeader" select="func:hasTableHeader(.)"/>
		<xsl:for-each select="h:tr">
			<xsl:choose>
				<xsl:when test="$hasHeader and position()=1">
					<sthead>
						<xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
						<xsl:apply-templates select="*" mode="simpletableEntry"/>
					</sthead>
				</xsl:when>
				<xsl:otherwise>
					<strow>
						<xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
						<xsl:apply-templates select="*" mode="simpletableEntry"/>
					</strow>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<!-- テーブル(simpletable-stentry)の処理  -->
	<xsl:template mode="simpletableEntry" match="h:th|h:td">
		<stentry>
			<xsl:apply-templates mode="applyDitaAttributes" select="@*[not(name()='rowspan') and not(name()='colspan')]"/>
			<xsl:apply-templates/>
		</stentry>
	</xsl:template>
	
    <!-- テーブル(dita-table)の処理  -->
	<xsl:template mode="tableContents" match="h:tbody">
		  <tgroup>
              <xsl:variable name="columnCount">
                  <xsl:for-each select="h:tr | h:tbody/h:tr | h:thead/h:tr">
                      <xsl:sort select="count(h:td | h:th)" data-type="number" order="descending"/>
                      <xsl:if test="position()=1">
                          <xsl:value-of select="count(h:td | h:th)"/>
                      </xsl:if>
                  </xsl:for-each>
              </xsl:variable>
              <xsl:attribute name="cols">
                  <xsl:value-of select="$columnCount"/>
              </xsl:attribute>
              <xsl:if test="h:tr/h:td/@rowspan 
                  | h:tr/h:td/@colspan
                  | h:tbody/h:tr/h:td/@rowspan 
                  | h:tbody/h:tr/h:td/@colspan
                  | h:thead/h:tr/h:th/@rowspan 
                  | h:thead/h:tr/h:th/@colspan">
                  <xsl:call-template name="generateColspecs">
                      <xsl:with-param name="count" select="number($columnCount)"/>
                  </xsl:call-template>
              </xsl:if>
			  <xsl:variable name="hasHeader" select="func:hasTableHeader(.)"/>
			  <xsl:choose>
				  <xsl:when test="$hasHeader">
					  <thead><xsl:apply-templates select="h:tr[1]"/></thead>
				  </xsl:when>
				  <xsl:otherwise>
					  <xsl:apply-templates select="h:thead"/>
				  </xsl:otherwise>
			  </xsl:choose>
              <tbody>
                  <!-- <xsl:apply-templates select="h:tr | h:tbody/h:tr | text() | h:b | h:strong | h:i | h:em | h:u | h:tfoot/h:tr"/> -->
				  <xsl:for-each select="h:tr">
					  <xsl:choose>
						  <xsl:when test="$hasHeader and position()=1"/>
						  <xsl:otherwise>
							  <row>
								  <xsl:apply-templates mode="applyDitaAttributes" select="@*[not(name()='rowspan') and not(name()='colspan')]"/>
								  <xsl:apply-templates/>
							  </row>
						  </xsl:otherwise>
					  </xsl:choose>
				  </xsl:for-each>
              </tbody>
          </tgroup>
	  </xsl:template>
  
  <xsl:template match="h:thead">
    <thead>
       <xsl:apply-templates select="@* | node()"/>
    </thead>
  </xsl:template>
  
  <xsl:template match="h:tr">
    <row>
       <xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
       <xsl:apply-templates select="@* | node()"/>
    </row>
  </xsl:template>
  
    
  <xsl:template match="h:th | h:td">
    <xsl:variable name="position" select="count(preceding-sibling::*) + 1"/>
    <entry>
        <xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
        <xsl:if test="number(@colspan) and @colspan > 1">
          <xsl:attribute name="namest">
            <xsl:value-of select="concat('col', $position)"/>
          </xsl:attribute>
          <xsl:attribute name="nameend">
            <xsl:value-of select="concat('col', $position + number(@colspan) - 1)"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="number(@rowspan) and @rowspan > 1">
          <xsl:attribute name="morerows">
            <xsl:value-of select="number(@rowspan) - 1"/>
          </xsl:attribute>
        </xsl:if>
       <xsl:apply-templates select="@* | node()"/>
    </entry>
  </xsl:template>
  
  <xsl:template name="generateColspecs">
    <xsl:param name="count" select="0"/>
    <xsl:param name="number" select="1"/>
    <xsl:choose>
      <xsl:when test="$count &lt; $number"/>
      <xsl:otherwise>
        <colspec>
          <xsl:attribute name="colnum">
            <xsl:value-of select="$number"/>
          </xsl:attribute>
          <xsl:attribute name="colname">
            <xsl:value-of select="concat('col', $number)"/>
          </xsl:attribute>
        </colspec>
        <xsl:call-template name="generateColspecs">
          <xsl:with-param name="count" select="$count"/>
          <xsl:with-param name="number" select="$number + 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <!-- リストの処理 -->
  <xsl:template match="h:ul | h:ol">  
	  <xsl:choose>
		  <xsl:when test="../../name() = 'ac:structured-macro' and (
			  compare(string(../../@ac:name), 'dita-ul') = 0 or
			  compare(string(../../@ac:name), 'dita-ol') = 0 or
			  compare(string(../../@ac:name), 'dita-sl') = 0 )">
			  <xsl:variable name="listtype">
				  <xsl:value-of select="substring(string(../../@ac:name), 6)"/>
			  </xsl:variable>
			  <!-- no ul/ol wrapper -->
			  <xsl:apply-templates/>
		  </xsl:when>
		  <xsl:otherwise>
			  <xsl:element name="{name()}">
				  <xsl:apply-templates/>
			  </xsl:element>
		  </xsl:otherwise>
	  </xsl:choose>
  </xsl:template>
  
  <xsl:template match="h:li">
	  <xsl:variable name="itemname">
		  <xsl:choose>
			  <xsl:when test="../../../name() = 'ac:structured-macro' and compare(string(../../../@ac:name), 'dita-sl') = 0">sli</xsl:when>
			  <xsl:otherwise>li</xsl:otherwise>
		  </xsl:choose>
	  </xsl:variable>
      <xsl:element name="{$itemname}">
		  <!-- liはtr/tdと同様にhtmlで属性が与えられている(not implemented yet in Confluence Java/JavaScript) -->
		  <xsl:apply-templates mode="applyDitaAttributes" select="@*"/>
          <xsl:apply-templates/>
	  </xsl:element>
  </xsl:template>
  
  <!-- xhtml element list -->
  <!-- xhtml element inline elements -->
  <xsl:template match="h:span[preceding-sibling::h:p and not(following-sibling::*)]">
     <p>
          <xsl:apply-templates select="@* | node()"/>
     </p>
 </xsl:template>
  <xsl:template name="insertParaInSection">
      <xsl:param name="childOfPara"/>
      <xsl:choose>
          <xsl:when test="parent::h:section">
              <p><xsl:copy-of select="$childOfPara"/></p>
          </xsl:when>
          <xsl:otherwise>
              <xsl:copy-of select="$childOfPara"/>
          </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  
  <xsl:template match="h:b | h:strong | h:i | h:em | h:u | h:s | h:sub | h:sup">
      <xsl:variable name="tagName">
          <xsl:value-of select="lower-case(name())"/>
      </xsl:variable>
      <xsl:variable name="elementName">
          <xsl:choose>
              <xsl:when test="$tagName = 'strong' ">b</xsl:when>
              <xsl:when test="$tagName = 'em' ">i</xsl:when>
              <xsl:otherwise><xsl:value-of select="$tagName"/></xsl:otherwise>
          </xsl:choose>
      </xsl:variable>
      <xsl:variable name="inlineElement">
         <xsl:element name="{$elementName}">
             <xsl:apply-templates select="@* | node()"/>
         </xsl:element>
     </xsl:variable>
      <xsl:call-template name="insertParaInSection">
          <xsl:with-param name="childOfPara" select="$inlineElement"/>
      </xsl:call-template>
  </xsl:template>
  
  <!-- line breaks-->
   <xsl:template match="h:div[h:br] | h:p[h:br]">
    <xsl:call-template name="brContent"/>
  </xsl:template>
  
  <xsl:template name="brContent">
    <xsl:variable name="preceding-text">
        <xsl:apply-templates select="node()[not(preceding-sibling::h:br)]"/>
    </xsl:variable>
    <xsl:if test="string-length(normalize-space($preceding-text)) > 0">
      <p><xsl:copy-of select="$preceding-text"/></p>
    </xsl:if>
    <xsl:for-each select="h:br">
      <xsl:variable name="following-text">
          <xsl:apply-templates select="parent::*[1]/node()[current() is preceding-sibling::h:br[1]]"/>
      </xsl:variable>
      <xsl:if test="string-length(normalize-space($following-text)) > 0">
        <p><xsl:copy-of select="$following-text"/></p>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
     
  
  <xsl:template match="h:li[h:br]">
    <li>
      <xsl:call-template name="brContent"/>
    </li>
  </xsl:template>

  <!-- ここから先は特殊な処理のサンプル  -->
  <!-- 特殊ハック1: "<cmd><ol><li>は各cmdに分割する"-->
  <xsl:template match="ac:rich-text-body/h:ol/h:li" mode="smartdita-specialized">
	  <cmd><xsl:value-of select="."/></cmd>
  </xsl:template>

</xsl:stylesheet>
