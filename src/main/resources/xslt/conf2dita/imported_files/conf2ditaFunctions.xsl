<?xml version="1.0" encoding="UTF-8"?><!-- 雀の往来 -->
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:func="http://exslt.org/functions"
	>
	
	<!-- 共通的関数 -->
	<!-- 小文字に変更 -->
    <xsl:function name="func:lower-case" as="xs:string*">
		<xsl:param name="in" />
		<xsl:value-of select="translate($in,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')" />
	</xsl:function>

	<!-- 大文字に変更 -->
    <xsl:function name="func:upper-case" as="xs:string*">
		<xsl:param name="in" />
		<xsl:value-of select="translate($in,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
	</xsl:function>

	<!-- 最初の1文字だけ大文字にする -->
	<xsl:function name="func:ucfirst" as="xs:string*">
		<xsl:param name="in" />
		<xsl:value-of select="concat(func:upper-case(substring($in, 1, 1)),func:lower-case(substring($in, 2)))" />
	</xsl:function>
	
	<!-- その要素が全体のよその中の何番目かを返却する -->
	<xsl:function name="func:index-of-node" as="xs:integer*">
		<xsl:param name="nodes" as="node()*"/>
		<xsl:param name="nodeToFind" as="node()"/>
		<xsl:sequence select="
			for $seq in (1 to count($nodes))
			return $seq[$nodes[$seq] is $nodeToFind]
			"/>
	</xsl:function>	
	
	<!-- 半角スペースのみ・子要素無しの場合にtrueを返す -->
	<xsl:function name="func:isEmptyTag" as="xs:boolean">
		<xsl:param name="ele" as="element()"/>
		<xsl:variable name="eletext"><xsl:value-of select="$ele"/></xsl:variable>
		<xsl:choose>
			<xsl:when test="count($ele/*)=0 and (string-length($eletext)=0 or matches($eletext,'^[ (&#160;)]*$'))">
				<xsl:value-of select="true()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	
	<!-- その要素がclass属性以外を1つでも持っているか -->
	<xsl:function name="func:hasAttribute" as="xs:boolean">
		<xsl:param name="ele" as="element()"/>
		<xsl:choose>
			<xsl:when test="count($ele/@*)>1">
				<xsl:value-of select="true()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	
	<!-- dita2confで使用するFunctionとか  -->
<!--
	<xsl:function name="func:substring-before-last" as="xs:string">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:param name="delim" as="xs:string"/>
		
		<xsl:sequence select="
			if (matches($arg, functx:escape-for-regex($delim)))
			then replace($arg,
            concat('^(.*)', functx:escape-for-regex($delim),'.*'),
            '$1')
			else ''
			"/>
	</xsl:function>
-->
    <xsl:function name="func:last-index-string" as="xs:string*">
        <xsl:param name="in" />
        <xsl:param name="needle" />
		<xsl:value-of select="reverse(tokenize($in,$needle))[1]"/>
    </xsl:function>
	
	<!--  改行コードを br タグに置き換える template  -->
	<xsl:template name="nl2br">
		<xsl:param name="value" select="string(.)"/>
		<xsl:choose>
			<xsl:when test="contains($value, '&#xA;')">
				<xsl:value-of select="substring-before($value, '&#xA;')"/>
				<xsl:element name="br"/>
				<xsl:call-template name="nl2br">
					<xsl:with-param name="value" select="substring-after($value, '&#xA;')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

    <xsl:function name="func:node-kind" as="xs:string*"
        xmlns:functx="http://www.functx.com" >
        <xsl:param name="nodes" as="node()*"/>
        <xsl:sequence select="
            for $node in $nodes
            return
            if ($node instance of element()) then 'element'
            else if ($node instance of attribute()) then 'attribute'
            else if ($node instance of text()) then 'text'
            else if ($node instance of document-node()) then 'document-node'
            else if ($node instance of comment()) then 'comment'
            else if ($node instance of processing-instruction())
            then 'processing-instruction'
            else 'unknown'
            "/>
    </xsl:function>

    <!-- DitaMacroとして処理しなければならないタグかどうかを判定する -->
    <xsl:function name="func:isDitaElement" as="xs:boolean">
        <xsl:param name="ele" as="element()"/>
        <!-- xsl:message>[<xsl:value-of select="concat('' , $ele/@class)"/>]</xsl:message -->
        <xsl:choose>
            <!-- ドメイン(領域)モジュール::inline -->
            <xsl:when test="starts-with($ele/@class, '+')">
                <xsl:choose>
                    <xsl:when test="compare($ele/@class, '+ topic/ph hi-d/b ')=0 or
                        compare($ele/@class, '+ topic/ph hi-d/i ')=0   or
                        compare($ele/@class, '+ topic/ph hi-d/sub ')=0 or
                        compare($ele/@class, '+ topic/ph hi-d/sup ')=0 or
                        compare($ele/@class, '+ topic/ph hi-d/u ')=0
                        ">
                        <xsl:value-of select="false()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="true()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- 構造モジュール::block -->
            <xsl:when test="starts-with($ele/@class, '-')">
                <xsl:choose>
                    <!-- そのままConfluenceでHTMLとして使えるタグ -->
                    <xsl:when test="compare($ele/@class, '- topic/body ')=0 or
                        compare($ele/@class, '- topic/li ')=0 or
                        compare($ele/@class, '- topic/sli ')=0 or
                        compare($ele/@class, '- topic/p ')=0
                        ">
                        <xsl:value-of select="false()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="true()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="false()"/> 
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>


	<!-- 表要素に代表されるDITAXMLをHTMLに変換する要素かどうかを判定する -->
    <xsl:function name="func:isDitaTableElement" as="xs:boolean">
        <xsl:param name="ele" as="element()"/>
        <xsl:choose>
			<xsl:when test="compare($ele/@class, '- topic/simpletable task/choicetable ')=0  or
				compare($ele/@class, '- topic/sthead task/chhead ')=0  or
				compare($ele/@class, '- topic/stentry task/choptionhd ')=0  or
				compare($ele/@class, '- topic/stentry task/chdeschd ')=0  or
				compare($ele/@class, '- topic/simpletable task/choicetable ')=0  or
				compare($ele/@class, '- topic/strow task/chrow ')=0  or
				compare($ele/@class, '- topic/stentry task/choption ')=0  or
				compare($ele/@class, '- topic/stentry task/chdesc ')=0  or
				compare($ele/@class, '- map/reltable ')=0  or
				compare($ele/@class, '- map/topicmeta ')=0  or
				compare($ele/@class, '- map/relheader ')=0  or
				compare($ele/@class, '- map/relcolspec ')=0  or
				compare($ele/@class, '- map/relrow ')=0  or
				compare($ele/@class, '- map/relcell ')=0  or
				compare($ele/@class, '- topic/simpletable ')=0  or
				compare($ele/@class, '- topic/sthead ')=0  or
				compare($ele/@class, '- topic/stentry ')=0  or
				compare($ele/@class, '- topic/strow ')=0  or
				compare($ele/@class, '- topic/stentry ')=0  or
				compare($ele/@class, '- topic/table ')=0  or
				compare($ele/@class, '- topic/tgroup ')=0  or
				compare($ele/@class, '- topic/colspec ')=0  or
				compare($ele/@class, '- topic/thead ')=0  or
				compare($ele/@class, '- topic/tbody ')=0  or
				compare($ele/@class, '- topic/row ')=0  or
				compare($ele/@class, '- topic/entry ')=0">
				<xsl:value-of select="true()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
    </xsl:function>
	
	
	<!-- codeblockの様に改行コードを<br />にしなければならないpreな要素を判定 -->
	<xsl:function name="func:isPreformattedElement" as="xs:boolean">
        <xsl:param name="ele" as="element()"/>
        <xsl:choose>
			<xsl:when
		  test="compare($ele/@class, '- topic/pre ')=0  or
				compare($ele/@class, '+ topic/pre pr-d/codeblock ')=0  or
				compare($ele/@class, '+ topic/pre sw-d/msgblock ')=0  or
				compare($ele/@class, '+ topic/pre ui-d/screen ')=0">
				<xsl:value-of select="true()"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="false()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	
	<!-- xml:langのようにConfluenceのマクロでは使えないAttributeを補正する -->
	<xsl:function name="func:adjustParameterName" as="xs:string*">
		<xsl:param name="attr" as="xs:string"/><!-- cast to string -->
		<xsl:choose>
			<!-- xml:lang -->
			<xsl:when test="$attr = 'xmllang'">
				<xsl:value-of select="'xml:lang'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$attr"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

</xsl:stylesheet>
