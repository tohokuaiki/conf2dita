<?xml version="1.0" encoding="UTF-8"?><!-- 雀の往来 -->
<xsl:stylesheet
	version="2.0"
	xmlns="http://www.atlassian.com/schema/confluence/4/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/"
	xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/"
	xmlns:func="http://exslt.org/functions"
	exclude-result-prefixes="xsl xs func"
	>
	
	<xsl:import href="conf2ditaFunctions.xsl"/>

	<xsl:import href="DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/dita2htmlImpl.xsl"/>
    
	<!-- table/imageで使うdita属性値のprefix -->
	<xsl:variable name="ditaAttributePrefix">ditaProperty</xsl:variable>
    
    <!-- root -->
	<xsl:template match="/">
	    <xsl:result-document
			doctype-system="{$confluence_dtd_path}"
			method="xml"
			omit-xml-declaration="no" encoding="UTF-8" indent="yes"
			>
			<ac:confluence>
				<xsl:apply-templates/>
			</ac:confluence>
		</xsl:result-document>
	</xsl:template>
	
	<!-- remove title -->
	<xsl:template match="title"/>
	<!-- shortdesc -->
	<xsl:template match="shortdesc">
		<xsl:apply-templates select="." mode="ditaMacro"/>
	</xsl:template>
	<!-- abstract -->
	<xsl:template match="abstract">
		<xsl:apply-templates select="." mode="ditaMacro"/>
	</xsl:template>
	<!-- prolog -->
	<xsl:template match="prolog">
		<xsl:apply-templates select="." mode="ditaMacro"/>
	</xsl:template>
        
	<!-- related-links -->
        <xsl:template match="related-links">
		<xsl:apply-templates select="." mode="ditaMacro"/>
	</xsl:template>
	
	<!-- body for each topic types -->
    <xsl:template match="/task/taskbody|/concept/conbody|/reference/refbody|glossgroup">
		<xsl:apply-templates mode="main"/>
    </xsl:template>

	<!-- main -->
	<xsl:template match="*" mode="main">
		<xsl:choose>
			<xsl:when test="func:isDitaTableElement(.)">
				<xsl:apply-templates select="."/>
			</xsl:when>
			<xsl:when test="func:isDitaElement(.)">
				<!-- xsl:message>DITA::<xsl:value-of select="name()"/></xsl:message -->
				<xsl:apply-templates select="." mode="ditaMacro"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- xsl:message>*NOT*DITA::<xsl:value-of select="name()"/></xsl:message -->
				<xsl:apply-templates select="." mode="htmlElement"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- HTML要素はそのまま処理 -->
	<xsl:template match="*" mode="htmlElement">
		<xsl:variable name="elementname">
			<xsl:value-of select="name()"/>
		</xsl:variable>
		<xsl:element name="{$elementname}">
			<xsl:apply-templates mode="main"/>
		</xsl:element>
	</xsl:template>
	
	<!-- TODO テキストコンテンツは親に<p>が無い場合はくくる.mode="main" -->
	<xsl:template match="text()|@*" mode="main">
		<xsl:value-of select="."/>
	</xsl:template>
	
	<!-- 入れ替え b=>strong -->
	<xsl:template match="b" mode="main">
		<strong><xsl:apply-templates mode="main"/></strong>
	</xsl:template>
	
	<!-- 入れ替え i=>em -->
	<xsl:template match="i" mode="main">
		<em><xsl:apply-templates mode="main"/></em>
	</xsl:template>
	
	<!-- コメント -->
	<xsl:template match="//comment()">
		<ac:structured-macro ac:name="dita-comment">
			<xsl:variable name="comment">
				<xsl:value-of select="."/>
			</xsl:variable>
			<ac:parameter ac:name="text"><xsl:value-of select="replace($comment, '&#10;', '\\n')"/></ac:parameter>
		</ac:structured-macro>
	</xsl:template>
	<xsl:template match="//comment()" mode="main">
		<ac:structured-macro ac:name="dita-comment">
			<ac:parameter ac:name="text"><xsl:value-of select="."/></ac:parameter>
		</ac:structured-macro>
	</xsl:template>
	
	<!-- data要素 ConfluenceのMacroとして実行 -->
	<xsl:template match="*" mode="ditaMacro">
		<xsl:variable name="elementname"><xsl:value-of select="name()"/></xsl:variable>
		<ac:structured-macro>
			<xsl:attribute name="ac:name">
				<xsl:value-of select="concat('dita-', name())" />
			</xsl:attribute>
			<xsl:apply-templates select="@*" mode="ditaAttribute"/>
			<ac:rich-text-body>
				<xsl:choose>
					<xsl:when test="func:isPreformattedElement(.)">
						<xsl:call-template name="nl2br"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="node()" mode="main"/>
					</xsl:otherwise>
				</xsl:choose>
			</ac:rich-text-body>
		</ac:structured-macro>
	</xsl:template>
	
	<!-- dita要素 特殊なConfluenceマクロ -->
	<!-- 1. image -->
	<xsl:template match="image" mode="ditaMacro">
		<ac:image>
			<ri:attachment>
				<xsl:if test="@href">
					<xsl:attribute name="ri:filename" select="func:last-index-string(@href, '/')"/>
				</xsl:if>
			</ri:attachment>
			<xsl:apply-templates select="@*" mode="ditaAttribute">
				<xsl:with-param name="exclude" select="'href,alt,width'"/>
				<xsl:with-param name="prefix" select="$ditaAttributePrefix"/>
			</xsl:apply-templates>
		</ac:image>
	</xsl:template>
	
	
	<!-- =============== Templates for attributes =============== -->
	<!-- ditaの属性値をマクロのparameterとして付与 -->
	<xsl:template match="@*" mode="ditaAttribute">
		<xsl:param name="exclude" select="''" as="xs:string"/>
		<xsl:param name="prefix"  select="''" as="xs:string"/>
		<xsl:variable name="excludes" as="xs:string*">
			<xsl:sequence select="tokenize($exclude, ',')"/>
		</xsl:variable>
		<xsl:if test="name()!='class'"><!-- classはDITAにとって特殊な意味を持つ -->
			<xsl:if test="empty(index-of($excludes, name()))">
				<!-- xml:lang のような特殊なAttribute対策 remove ":" -->
				<xsl:variable name="attrName"><xsl:value-of select="concat($prefix, replace(name(), ':', ''))"/></xsl:variable>
				<xsl:element name="ac:parameter">
					<xsl:attribute name="ac:name" select="$attrName"/>
					<xsl:value-of select="."/>
				</xsl:element>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<!-- tableなど用にditaProperty用のprefixを持つHTML属性を与える -->
	<!-- this is also used in OASIS Dita-ot xsl which junoe customized -->
	<xsl:template name="setallditaproperties">
		<xsl:variable name="prefix"><xsl:value-of select="$ditaAttributePrefix"/></xsl:variable><!-- Java/JavaScriptでも使われるPrefix -->
		<xsl:for-each select="@*">
			<xsl:if test="name()!='class'"><!-- classはDITAにとって特殊な意味を持つ -->
				<!-- xml:lang のような特殊なAttribute対策 remove ":" -->
				<xsl:variable name="attrname"><xsl:value-of select="concat($prefix, replace(name(), ':', ''))"/></xsl:variable>
				<xsl:attribute name="{$attrname}"><xsl:value-of select="."/></xsl:attribute>
			</xsl:if>
		</xsl:for-each>
		<!-- talbeタグにtabletypeの情報を与える -->
		<xsl:if test="name()='table' or name()='simpletable' or name()='choicetable'">
			<xsl:variable name="tabletypeName"><xsl:value-of select="concat($prefix, 'tabletype')"/></xsl:variable>
			<xsl:attribute name="{$tabletypeName}">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
		</xsl:if>
	</xsl:template>
	
<!-- ========= templates used in OASIS Dita-ot =========== -->
	<!-- paragraphs -->
	<xsl:template match="*[contains(@class,' topic/p ')]" name="topic.p" mode="main">
		<!-- To ensure XHTML validity, need to determine whether the DITA kids are block elements.
			If so, use div_class="p" instead of p -->
		<!-- プロパティ付のものや中にHTMLのブロック要素を内包しているものはマクロ化する -->
		<xsl:choose>
			<xsl:when test="func:hasAttribute(.) or
				descendant::*[contains(@class,' topic/pre ')] or
				descendant::*[contains(@class,' topic/ul ')] or
				descendant::*[contains(@class,' topic/sl ')] or
				descendant::*[contains(@class,' topic/ol ')] or
				descendant::*[contains(@class,' topic/lq ')] or
				descendant::*[contains(@class,' topic/dl ')] or
				descendant::*[contains(@class,' topic/note ')] or
				descendant::*[contains(@class,' topic/lines ')] or
				descendant::*[contains(@class,' topic/fig ')] or
				descendant::*[contains(@class,' topic/table ')] or
				descendant::*[contains(@class,' topic/simpletable ')]">
				<ac:structured-macro>
					<xsl:attribute name="ac:name">dita-p</xsl:attribute>
					<xsl:apply-templates select="@*" mode="ditaAttribute"/>
					<ac:rich-text-body>
						<xsl:call-template name="commonattributes"/>
						<xsl:call-template name="setid"/>
						<xsl:apply-templates mode="main"/>
					</ac:rich-text-body>
				</ac:structured-macro>
			</xsl:when>
			<xsl:otherwise>
				<p>
					<xsl:call-template name="commonattributes"/>
					<xsl:call-template name="setid"/>
					<xsl:apply-templates mode="main"/>
				</p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- ==== overwrite same match expression in dita2htmlImpl.xsl ==== -->
	<!-- table wrapper -->
	<xsl:template match="*[contains(@class,' topic/table ')]" name="topic.table">
		<ac:structured-macro>
			<xsl:attribute name="ac:name">dita-table</xsl:attribute>
			<xsl:apply-templates select="@*" mode="ditaAttribute"/>
			<ac:rich-text-body>
				<!-- title -or- title & desc -->
				<xsl:if test="*[contains(@class,' topic/title ')]">
					<xsl:apply-templates select="title" mode="ditaMacro"/>
				</xsl:if>
				<xsl:if test="*[contains(@class,' topic/desc ')]"> 
					<xsl:apply-templates select="desc" mode="ditaMacro"/>
				</xsl:if>
				<!-- table main -->
				<xsl:apply-templates select="."  mode="table-fmt" />
			</ac:rich-text-body>
		</ac:structured-macro>
	</xsl:template>
	<!-- table title & desc -->
	<xsl:template name="place-tbl-lbl"/>
	
	<!-- simpletable(& choicetable) wrapper -->
	<xsl:template match="*[contains(@class,' topic/simpletable ')]" name="topic.simpletable">
		<ac:structured-macro>
			<xsl:attribute name="ac:name">
				<xsl:choose>
					<xsl:when test="contains(@class,' topic/simpletable task/choicetable ')">dita-choicetable</xsl:when>
					<xsl:otherwise>dita-simpletable</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates select="@*" mode="ditaAttribute"/>
			<ac:rich-text-body>
				<xsl:apply-templates select="."  mode="simpletable-fmt" />
			</ac:rich-text-body>
		</ac:structured-macro>
	</xsl:template>
	
	
	<!-- table implementation(not include simpletable/choicetable) -->
	<xsl:template name="dotable">
		<xsl:apply-templates select="*[contains(@class,' ditaot-d/ditaval-startprop ')]" mode="out-of-line"/>
		<xsl:call-template name="setaname"/>
		<table>
			<tbody>
				<xsl:call-template name="place-tbl-lbl"/>
				<!-- title and desc are processed elsewhere -->
				<xsl:apply-templates select="*[contains(@class,' topic/tgroup ')]"/>
			</tbody>
		</table>
	</xsl:template>
	
	<!-- headerもこちら tbodyはtableの方で付ける  -->
	<xsl:template match="*[contains(@class,' topic/tbody ')]|*[contains(@class,' topic/thead ')]" name="topic.thead-tbody">
		<!-- Get style from parent tgroup, then override with thead if specified locally -->
		<xsl:apply-templates select="../*[contains(@class,' ditaot-d/ditaval-startprop ')]/@outputclass" mode="add-ditaval-style"/>
		<xsl:call-template name="commonattributes"/>
		<xsl:apply-templates/>
		<!-- process table footer -->
		<xsl:apply-templates select="../*[contains(@class,' topic/tfoot ')]" mode="gen-tfoot" />
	</xsl:template>
	
	<!-- topic/row -->
	<xsl:template match="*[contains(@class,' topic/row ')]" name="topic.row">
		<tr>
			<xsl:call-template name="setallditaproperties"/>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>
	
	<!-- リスト要素は全部上書き -->
	<!-- Unordered & Ordered & Simple List - -->
	<xsl:template match="*[compare(@class,'- topic/ul ')=0 or compare(@class,'- topic/ol ')=0 or compare(@class,'- topic/sl ')=0]" name="topic.ul-ol-sl" mode="main">
		<xsl:variable name="htmltag">
			<xsl:choose>
				<xsl:when test="compare(@class, '- topic/ol ')=0">ol</xsl:when>
				<xsl:otherwise>ul</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<ac:structured-macro>
			<xsl:attribute name="ac:name"><xsl:value-of select="concat('dita-', name())"/></xsl:attribute>
			<xsl:apply-templates select="@*" mode="ditaAttribute"/>
			<ac:rich-text-body>
				<xsl:element name="{$htmltag}">
					<xsl:apply-templates mode="main"/>
				</xsl:element>
			</ac:rich-text-body>
		</ac:structured-macro>
	</xsl:template>
	<!-- simple list item -->
	<xsl:template match="*[compare(@class,'- topic/li ')=0 or compare(@class,'- topic/sli ')=0]" name="topic.sli" mode="main">
		<!-- xsl:message><xsl:value-of select="@class"/>||<xsl:value-of select="name()"/></xsl:message -->
		<li>
			<xsl:call-template name="setallditaproperties"/>
			<xsl:apply-templates mode="main"/>
		</li>
	</xsl:template>
	
	<!-- ==== /overwrite same match expression in dita2htmlImpl.xsl ==== -->
	
<!-- ========= /templates used in OASIS Dita-ot =========== -->


</xsl:stylesheet>
