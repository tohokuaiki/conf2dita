<?xml version="1.0" encoding="UTF-8"?><!-- 雀の往来 -->
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	>

	<!-- このファイルはVelocityテンプレートとして評価されるのでVelocity変数が使える。 -->
	<xsl:import href="${xsl_import_root_path}/conf2ditaImpl.xsl"/>

	<!-- XSLT実行時パラメータ -->
	<xsl:param name="dtd_path"/>
	
</xsl:stylesheet>
