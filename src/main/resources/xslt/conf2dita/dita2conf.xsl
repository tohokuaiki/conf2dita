<?xml version="1.0" encoding="UTF-8"?><!-- 雀の往来 -->
<xsl:stylesheet
	version="2.0"
	xmlns="http://www.atlassian.com/schema/confluence/4/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:ac="http://www.atlassian.com/schema/confluence/4/ac/"
	xmlns:ri="http://www.atlassian.com/schema/confluence/4/ri/"
	xmlns:func="http://exslt.org/functions"
	exclude-result-prefixes="xsl xs func"
	>
	
	<!-- このファイルはVelocityテンプレートとして評価されるのでVelocity変数が使える。 -->
	<xsl:import href="${xsl_import_root_path}/dita2confImpl.xsl"/>
    
	<!-- XSLT実行時パラメータ  -->
	<xsl:param name="dtd_path"/>
	<xsl:variable name="confluence_dtd_path">
		<xsl:choose>
			<xsl:when test="$dtd_path">
				<xsl:value-of select="concat($dtd_path, '/confluence.dtd')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'http://wikiworks.jp/confluence-schema/confluence.dtd'"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
</xsl:stylesheet>
