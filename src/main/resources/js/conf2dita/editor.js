/* for edit page */
/* global Confluence, AJS */

if (typeof Conf2dita === 'undefined') {
    var Conf2dita = {};
}

AJS.$((function ($) {
    AJS.log("***** editor");
    /// set global parameters and functions
    var pageId;

    var editor;

    // i18n shortcut
    var _ = function () {
        return AJS.I18n.getText.apply(this, arguments);
    };

    // error message
    var showMessage = function (type, message) {
        Conf2dita.showMessage(type, "#editor-messages", message);
    };

    /**
     * load value to panel (DOM => Panel)
     * @param dom DOMElement which dita property values are stored as data-dita-property-*** attributes
     * @param panel panel object to load values
     */
    var loadPanelValue = function (dom, panel) {
        var properties = Conf2dita.Common.getDitaPropertiesFromClass(dom);
        panel.popup.element.find('input,select').each(function(i, ele) {
            var name = $(ele).attr('name');
            if (name && typeof(properties[name]) !== 'undefined') {
                $(ele).val(properties[name]);
            }
            else {
                $(ele).val("");
            }
        });
    };

    /**
     * get cursor pointed table dita tabletype
     * @param {$(table)} table cursor pointed HTMLtable
     * @returns string
     */
    var getDitaTabletype = function (table) {
        var tabletype = $(table).closest('table.wysiwyg-macro').data('macro-name');
        if (tabletype) {
            tabletype = tabletype.substr("dita-".length);
        }
        return tabletype;
    };

    /**
     * is wrapped by dita table macro or not
     * @param {$(table)} table
     * @returns boolean
     */
    var isWrappedDitaTableMacro = function (table) {
        var ret = false;

        var tabletype = getDitaTabletype(table);
        if (tabletype) {
            for (var ti = 0, tj = Conf2dita.tableTypes.length; ti < tj; ti++) {
                if (tabletype === Conf2dita.tableTypes[ti].toLowerCase()) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    };

    /**
     * get table doms which wrapped by dita-***table macro.
     * @return array of HTML Table dom.
     */
    var getDitaTableMacroWrappedTables = function () {
        var tables = [];
        $('table', editor.dom.doc.getElementsByTagName('body')).each(function (i, table) {
            if (!$(table).data('macroName') && isWrappedDitaTableMacro(table)) {
                tables.push(table);
            }
        });

        return tables;
    };


    /**
     * get list elements
     * @return array of HTML Table dom.
     */
    var getListElements = function () {
        return $('ul,ol', editor.dom.doc.getElementsByTagName('body'));
    };


    /**
     * save value to dom (Panel => DOM)
     * @param dom DOMElement which dita property data to be stored as data-dita-property-*** attributes.
     * @param panel panel object what user input data.
     */
    var savePanelValue = function (dom, panel) {
        var domClass = [];
        // reset data
        $.each(Conf2dita.splitByChar($(dom).attr('class'), " "), function(i, _class){
            if (!_class.match(Conf2dita.ditaPrefixRegexp)){
                domClass.push(_class);
            }
        });
        // set new value as class
        var ditaparam = [];
        panel.popup.element.find('input,select').each(function(i, input) {
            ditaparam.push($(input).attr('name') + "=" + Conf2dita.escapeMacroQueryString($(input).val()));        
        });
        domClass.push(Conf2dita.ditaPrefix + Conf2dita.base64encode(ditaparam.join("&")));
        $(dom).attr("class", domClass.join(" "));
    };


    /**************************************
     *    Main Conf2dita Editor Object    *
     **************************************/
    Conf2dita.Editor = {
        /* dita-table jQuery selector. {***} will replace supported table types by init(). */
        editorTableQuery: 'table.wysiwyg-macro[data-macro-name="{***}"]>tbody>tr>td.wysiwyg-macro-body>table.confluenceTable',
        editorListQuery: 'ul,ol',
        disableButton: function (button, isPrimaryToolbar) {
            if (isPrimaryToolbar === undefined) isPrimaryToolbar = false;
            if (isPrimaryToolbar){
                $(button).closest('li.toolbar-item.aui-button').addClass('disabled');
            } else {
                $(button).prop('disabled', true).addClass('toolbar-item disabled');
            }
        },
        enableButton: function (button, isPrimaryToolbar) {
            if (isPrimaryToolbar === undefined) isPrimaryToolbar = false;
            if (isPrimaryToolbar){
                $(button).closest('li.toolbar-item.aui-button').removeClass('disabled');
            } else {
                $(button).prop('disabled', false).removeClass('toolbar-item disabled');
            }
        },
        // make primary button group from array of Confluence.Editor.Toolbar.Button .
        primaryButtons: function(buttons, options){
            if (options === undefined){
                options = {};
            }
            this.render = function(){
                var group = $("<ul></ul>");
                group.addClass("aui-buttons");
                options.groupClass && group.addClass(options.groupClass);
                options.id && group.attr('id', options.id);
                for (var i=0, j=buttons.length; i<j; i++) {
                    var li = $('<li></li>');
                    var a = $('<a></a>');
                    var button = $(buttons[i].render());
                    a.attr('id', button.attr('id'))
                      .addClass('toolbar-trigger')
                        .append(button);
                    li.attr('id', button.attr('id') + "-trigger")
                      .addClass('toolbar-item aui-button').append(a)
                        .attr('data-tooltip', button.data('tooltip')); // not data()
                    group.append(li);
                }
                return group;
            };
        },
        // init
        init: function (self) {
            var queries = [];
            for (var i = 0, j = Conf2dita.tableTypes.length; i < j; i++) {
                queries.push('table.wysiwyg-macro[data-macro-name="dita-' + Conf2dita.tableTypes[i].toLowerCase() + '"]>tbody>tr>td.wysiwyg-macro-body>table.confluenceTable');
            }
            self.editorTableQuery = queries.join(',');
        
            // editor dom loaded
            AJS.bind('rte.init.ui', function (e, editor) {
                Conf2dita.Editor.onLoad(Conf2dita.Editor);
            });

            // var self = Conf2dita.Editor;
            /// add init ContextToolbar initialization
            $(document).bind('initContextToolbars.Toolbar', function (e, _editor) {
                editor = _editor;
                var listButtons = self.ListToolbar.createButtons();
                $(listButtons).attr('id', 'dita-list-toolbar');
                $('#rte-button-numlist').parent().after(listButtons);
            });
            $(document).bind("createContextToolbarRow.Toolbar", function (e) {
                // add buttons if pageid exists
                var id_name = 'dita-table-toolbar';
                if ($('#'+id_name).length == 0 ){
                    var tableButtons = self.TableToolbar.createButtons();
                    $(tableButtons).attr('id', id_name);
                    $('#rte-toolbar-row-default').append(tableButtons);
                }
            });

            /* do each property init method. */
            for (var prop in self) {
                // alert(prop + ":" +typeof(self[prop].init));
                if (typeof (self[prop].init) === "function") {
                    self[prop].init(self[prop]);
                }
            }
        },
        // onload document
        onLoad: function (self) {
            AJS.log("***** editor onLoad");
            pageId = AJS.params.pageId;

            // var self = self
            /* do onload */
            for (var prop in self) {
                if (typeof (self[prop].onLoad) !== "undefined") {
                    self[prop].onLoad(self[prop]);
                }
            }
        },
        /*********************************
         * dita property setting panel
         *********************************/
        DitaPropertyPanel: {
            panel: {
                obj: null,
                id: "dita-property-setting"
            },
            generalConfiguration_id: "dita-property-generalConfiguration",
            // methods
            init: function (self) {
            },
            onLoad: function (self) {
                self.panel.obj = new AJS.ConfluenceDialog({
                    width: 565,
                    height: 430,
                    id: self.panel.id,
                    onCancel: self.onCancel
                });
                // var self = Conf2dita.Editor.DitaPropertyPanel;
                self.panel.obj
                        .addHeader('DITA Property Settings')
                        .addPanel("generalConfiguration", $('<div/>').attr('id', self.generalConfiguration_id))
                        .addCancel("Cancel", self.onCancel)
                        .addSubmit("Save", self.onSubmitSave);
                $('#rte-button-dita-setproperty').on('click', self.onClickDitaSetting);
            },
            onClickDitaSetting: function (e) {
                var self = Conf2dita.Editor.DitaPropertyPanel;
                $.ajax({
                    url: Conf2dita.ctrlUrl.ditaPropertyCtrl,
                    type: "get",
                    data: {
                        pageId: pageId
                    }
                }).success(function (data, status, xhr) {
                    $('#' + self.generalConfiguration_id).html(data);
                    self.panel.obj.show();
                });
            },
            onCancel: function () {
                Conf2dita.Editor.DitaPropertyPanel.panel.obj.hide();
            },
            onSubmitSave: function (panel) {
                var self = Conf2dita.Editor.DitaPropertyPanel;
                var data = $("#" + Conf2dita.Editor.DitaPropertyPanel.generalConfiguration_id + " form").serializeArray();
                data.push({name: "pageId", value: pageId});
                $.ajax({
                    url: Conf2dita.ctrlUrl.ditaPropertyCtrl,
                    type: "post",
                    data: data
                }).success(function (data, status, xhr) {
                    if (data.result) {
                        Conf2dita.Editor.DitaPropertyPanel.panel.obj.hide();
                    } else {
                        alert(data.error);
                    }
                });
            }
        },
        /********************************************
         * add table property setting UI
         ********************************************/
        TableToolbar: {
            // load/store properties
            panel: {
                table: {obj: null, id: "conf2dita-table-property-setting-panel", target: null},
                row: {obj: null, id: "conf2dita-tablerow-property-setting-panel", target: null},
                cell: {obj: null, id: "conf2dita-tablecell-property-setting-panel", target: null}
            },
           createButtons: function(){
               return (new Confluence.Editor.Toolbar.Components.Group([
                    /* row property */
                    new Confluence.Editor.Toolbar.Components.Button({
                        text: _("set dita table row property"),
                        iconClass: "conf2dita-tablerow-property-setting-button",
                        click: function () {
                            var cell = editor.selection.getStart(); // this must td or th as table buttons aren't disabled!
                            var table = $(cell).closest('table');
                            var tableType = getDitaTabletype(table);
                            if (tableType) {
                                var _panel = Conf2dita.Editor.TableToolbar.panel.row;
                                var row = $(cell).closest('tr');
                                // reset
                                $('#' + _panel.id + ' .dialog-page-body').find('select,input').val('');
                                // load
                                loadPanelValue(row, _panel.obj);
                                _panel.obj.show().gotoPanel(0);
                                _panel.target = row;
                                switch (tableType.toLowerCase()) {
                                    case "table":
                                        $('#conf2dita-editor-table-rowproperty-row-id').show();
                                        break;
                                    default :
                                        $('#conf2dita-editor-table-rowproperty-row-id').hide();
                                }
                            }
                        }
                    }),
                    /* cell property */
                    new Confluence.Editor.Toolbar.Components.Button({
                        text: _("set dita table cell property"),
                        iconClass: "conf2dita-tablecell-property-setting-button",
                        click: function () {
                            var cell = editor.selection.getStart().closest('td,th'); // this must td or th as table buttons aren't disabled!
                            var table = $(cell).closest('table');
                            var tableType = getDitaTabletype(table);
                            if (tableType) {
                                var _panel = Conf2dita.Editor.TableToolbar.panel.cell;
                                // reset
                                $('#' + _panel.id + ' .dialog-page-body').find('select,input').val('');
                                // load
                                loadPanelValue(cell, _panel.obj);
                                _panel.obj.show().gotoPanel(0);
                                _panel.target = cell;
                                switch (tableType.toLowerCase()) {
                                    case "table":
                                        $('#conf2dita-editor-table-cellproperty-entry-id').show();
                                        $('#conf2dita-editor-table-cellproperty-except-entry-id').hide();
                                        break;
                                    default :
                                        $('#conf2dita-editor-table-cellproperty-entry-id').hide();
                                        $('#conf2dita-editor-table-cellproperty-except-entry-id').show();
                                }
                            }
                        }
                    })
                ])).render();
            },
            Events: [],
            init: function (self) {
            },
            onLoad: function (self) {
                /*******************************************
                 * create table-cell property control panel
                 *******************************************/
                /* table controller */
                var panel_id = self.panel.table.id;
                self.panel.table.obj = new AJS.ConfluenceDialog({
                    width: 765,
                    height: 530,
                    id: panel_id,
                    onCancel: function () {
                        self.onCancel(self.panel.table);
                    },
                    onSubmit: function () {
                        self.onSubmitSave(self.panel.table);
                    }
                });

                // add panels
                self.panel.table.obj.addHeader('DITA Table Property Settings')
                        .addCancel("Cancel", function () {
                            self.onCancel(self.panel.table);
                        })
                        .addSubmit("Save", function () {
                            self.onSubmitSave(self.panel.table);
                        })
                        /*                        .addPanel("Tableype",
                         Conf2dita.Editor.Template.tablePropertyTabletype(),
                         "conf2dita-editor-tableproperty-tabletype",
                         "conf2dita-editor-tableproperty-tabletype-id") */
                        .addPanel("Common",
                                Conf2dita.Editor.Template.tablePropertyCommon(),
                                "conf2dita-editor-tableproperty-common",
                                "conf2dita-editor-tableproperty-common-id");
                /* row controller */
                var rowpanel_id = self.panel.row.id;
                self.panel.row.obj = new AJS.ConfluenceDialog({
                    width: 765,
                    height: 530,
                    id: rowpanel_id,
                    onCancel: function () {
                        self.onCancel(self.panel.row);
                    },
                    onSubmit: function () {
                        self.onSubmitSave(self.panel.row);
                    }
                });
                self.panel.row.obj.addHeader('DITA Table Row Property Settings')
                        .addCancel("Cancel", function () {
                            self.onCancel(self.panel.row);
                        })
                        .addSubmit("Save", function () {
                            self.onSubmitSave(self.panel.row);
                        })
                        .addPanel("Common", Conf2dita.Editor.Template.tableRowPropertyCommon(),
                                "conf2dita-editor-table-rowproperty-common",
                                "conf2dita-editor-table-rowproperty-common-id")
                        .addPanel("Row", Conf2dita.Editor.Template.tableRowPropertyRow(),
                                "conf2dita-editor-table-rowproperty-row",
                                "conf2dita-editor-table-rowproperty-row-id");

                /* cell controller */
                var cellpanel_id = self.panel.cell.id;
                self.panel.cell.obj = new AJS.ConfluenceDialog({
                    width: 765,
                    height: 530,
                    id: cellpanel_id,
                    onCancel: function () {
                        self.onCancel(self.panel.cell);
                    },
                    onSubmit: function () {
                        self.onSubmitSave(self.panel.cell);
                    }
                });
                var cellTypes = ["Choptionhd", "Chdeschd", "Choption", "Chdesc", "Stentry", "Entry"];
                self.panel.cell.obj.addHeader('DITA Table Cell Property Settings')
                        .addCancel("Cancel", function () {
                            self.onCancel(self.panel.cell);
                        })
                        .addSubmit("Save", function () {
                            self.onSubmitSave(self.panel.cell);
                        })
                        .addPanel("Common", Conf2dita.Editor.Template.tableCellPropertyCommon(),
                                "conf2dita-editor-table-cellproperty-common",
                                "conf2dita-editor-table-cellproperty-common-id")
                        .addPanel("Entry", Conf2dita.Editor.Template.tableCellPropertyEntry(),
                                "conf2dita-editor-table-cellproperty-entry",
                                "conf2dita-editor-table-cellproperty-entry-id")
                        .addPanel("ExceptEntry", Conf2dita.Editor.Template.tableCellPropertyExceptEntry(),
                                "conf2dita-editor-table-cellproperty-except-entry",
                                "conf2dita-editor-table-cellproperty-except-entry-id");

                // bind to ctrl button disabled when table type is not defined.
                $(document).bind('enableContextToolbarRow.Toolbar', function (e) {
                    // $(editor.selection.getStart()).closest('table');
                    var table = $(editor.selection.getStart()).closest('table');
                    var tabletype = getDitaTabletype(table);
                    var showflag = isWrappedDitaTableMacro(table);
                    var ctbar = editor.plugins.customtoolbar;
                    if (showflag) {
                        ctbar.enableToolbarButton("conf2dita-tablerow-property-setting-button");
                        ctbar.enableToolbarButton("conf2dita-tablecell-property-setting-button");
                    } else {
                        ctbar.disableToolbarButton("conf2dita-tablerow-property-setting-button");
                        ctbar.disableToolbarButton("conf2dita-tablecell-property-setting-button");
                    }
                });
            },
            onCancel: function (_panel) {
                _panel.obj.hide();
                _panel.target = null;
            },
            onSubmitSave: function (_panel) {
                // save to dom property
                savePanelValue(_panel.target, _panel.obj);
                _panel.obj.hide();
                $(document).trigger('enableContextToolbarRow.Toolbar');
            }
        },
        /********************************************
         * add list property setting UI
         ********************************************/
        ListToolbar: {
            // load/store properties
            panel: {
                listitem: {obj: null, id: "conf2dita-listitem-property-setting-panel", target: null}
            },
            createButtons: function(){
                var buttons = [
                    /* listitem property */
                    new Confluence.Editor.Toolbar.Components.Button({
                        id: "conf2dita-listitem-property-setting-button",
                        text: _("set list item dita properties"),
                        iconClass: "conf2dita-listitem-property-setting-button",
                        click: function () {
                            var listitem = $(editor.selection.getStart()).closest('li');
                            if (listitem.length == 0) {
                                return false;
                            }
                            var _panel = Conf2dita.Editor.ListToolbar.panel.listitem;
                            // reset
                            $('#' + _panel.id + ' .dialog-page-body').find('select,input').val('');
                            // load
                            loadPanelValue(listitem, _panel.obj);
                            _panel.obj.show().gotoPanel(0);
                            _panel.target = listitem;
                        }
                    })
                ];
                return (new Conf2dita.Editor.primaryButtons(buttons)).render();
            },
            Events: [],
            init: function (self) {
            },
            onLoad: function (self) {
                // if editor want to use nested list, use dita-li macro.
                // set diabled
                Conf2dita.Editor.disableButton("#conf2dita-listitem-property-setting-button", true);

                /*******************************************
                 * create list property control panel
                 *******************************************/
                /* listitem controller */
                var listitempanel_id = self.panel.listitem.id;
                self.panel.listitem.obj = new AJS.ConfluenceDialog({
                    width: 465,
                    height: 530,
                    id: listitempanel_id,
                    onCancel: function () {
                        self.onCancel(self.panel.listitem);
                    },
                    onSubmit: function () {
                        self.onSubmitSave(self.panel.listitem);
                    }
                });
                self.panel.listitem.obj.addHeader('DITA List Item Property Settings')
                        .addCancel("Cancel", function () {
                            self.onCancel(self.panel.listitem);
                        })
                        .addSubmit("Save", function () {
                            self.onSubmitSave(self.panel.listitem);
                        })
                        .addPanel("Common", Conf2dita.Editor.Template.listitemProperties(),
                                "conf2dita-editor-listitem-properties",
                                "conf2dita-editor-listitem-properties-id");

                // onNodeChange toggle active status
                editor.onNodeChange.add(function (editor, ctrl, focusNode, isCollapsed, ctrl) {
                    var li = $(focusNode).closest('li');
                    if (li.length > 0 ){
                        Conf2dita.Editor.enableButton("#conf2dita-listitem-property-setting-button", true);
                    } else {
                        Conf2dita.Editor.disableButton("#conf2dita-listitem-property-setting-button", true);
                    }
                });
            },
            onCancel: function (_panel) {
                _panel.obj.hide();
                _panel.target = null;
            },
            onSubmitSave: function (_panel) {
                // save to dom property
                savePanelValue(_panel.target, _panel.obj);
                _panel.obj.hide();
            }
        },
        /********************************************
         * add image property setting UI
         ********************************************/
        ImagePropertyPanel: {
            panel: {
                obj: null, id: "conf2dita-image-property-setting-panel", target: null
            },
            storeAttribute: 'confluence-query-params',
            Events: [],
            init: function (self) {
                var $webItemLinkId = "conf2dita-image-property-panel";
                AJS.bind('dialog-created.image-properties', function (event, data) {
                    self.panel.obj = $(Conf2dita.Editor.Template.imageProperties());
                    Confluence.Editor.ImageProps.registerPanel(
                            $webItemLinkId, self.panel.obj, $webItemLinkId,
                            function () {
                                Conf2dita.Editor.ImagePropertyPanel.onSubmitSave(self, data.img);
                            });
                    // load value
                    var queryparams = Conf2dita.parseMacroQueryString($(data.img).attr(self.storeAttribute), "&");
                    if (typeof (queryparams[Conf2dita.ditaPrefix]) === 'string') {
                        queryparams = Conf2dita.parseMacroQueryString(Conf2dita.base64decode(queryparams[Conf2dita.ditaPrefix]));
                        for (var i in queryparams) {
                            if (queryparams[i] !== null) {
                                self.panel.obj.find('[name="' + i + '"]').val(queryparams[i]);
                            }
                        }
                    }
                });
            },
            onLoad: function (self) {
            },
            onCancel: function (_panel) {
                _panel.obj.hide();
                _panel.target = null;
            },
            onSubmitSave: function (self, image) {

                var ditaparam = [];
                self.panel.obj.find('input,select').each(function (i, input) {
                    ditaparam.push($(input).attr('name') + "=" + Conf2dita.escapeMacroQueryString($(input).val()));
                });

                var queryparams = Conf2dita.parseMacroQueryString($(image).attr(self.storeAttribute), "&");
                queryparams[Conf2dita.ditaPrefix] = Conf2dita.base64encode(ditaparam.join("|"));

                var qs = [];
                for (var i in queryparams) {
                    qs.push(i + "=" + queryparams[i]);
                }
                // save to dom property
                $(image).attr(self.storeAttribute, qs.join('&'));
            }
        }
    };


    /******************
     * initialization
     *****************/
    // when this script file is loaded
    Conf2dita.Editor.init(Conf2dita.Editor);
    return function () {
        return;
    };
})(AJS.$));

