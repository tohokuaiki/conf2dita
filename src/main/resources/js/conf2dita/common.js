/* common conf2dita editor and page */
/* global Base64, AJS */

var Conf2dita = {
    /* HTML data attribute prefix */
    ditaPrefix: "ditaProperty",
    ditaPrefixRegexp: null,

    pageMain: '#content',

    /* elements these class has base64 encoded dita properties */
    domPropertyTags: ['tr', 'th', 'td', 'li'],

    /* ajax entry points */
    ctrlUrl: {
        ditaPropertyRest: "{baseUrl}/rest/conf2dita/1.0/ditaproperty.json", /* rest */
        ditaPropertyCtrl: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditapropertycontroller", /* servlet */
        tableProperty: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditatablepropertycontroller", /* servlet @todo move to rest */
        listProperty: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditalistpropertycontroller"  /* servlet @todo move to rest */
    },
    /* Confluence Rest API Url Regexp */
    RestUrlRexExp: {
        label: {
            post: new RegExp("/rest/ui/([0-9\.]+)/content/[0-9]+/labels$"),
            del: new RegExp("/rest/ui/[0-9\.]+/content/[0-9]+/label/[0-9]+$")
        }
    },
    /* supported dita table types */
    tableTypes: ["Choicetable", "Simpletable", "Table"],
    // var tableTypes = ["Choicetable","LcMatchTable","Reltable","Simpletable","SubjectRelTable","Table","TopicSubjectTable"];

    /* error message */
    showMessage: function (type, place, message) {
        // error, success, warning, info
        AJS.messages[type](place, {body: message});
    },
    /* original base64 to my HTML class safety base64. padding:"="=>""(no padding), "+"=>"_", "/"=>"-" */
    base64encode: function (s) {
        return Base64.encode(s).replace(/=/g, "").replace(/\+/g, "_").replace(/\//g, "-");
    },
    base64decode: function (s) {
        try {
            return Base64.decode(s.replace(/_/g, "+").replace(/\-/g, "/"));
        } catch (e){
            alert("failed to base64 encode.");
            return "";
        }
    },
    /* split with escape expression */
    splitByChar: function () {
        var retval = [], targetstring = "", delimiter = "", limit = 0;

        if (typeof (arguments[0]) !== 'undefined')
            targetstring = arguments[0];
        if (typeof (arguments[1]) !== 'undefined')
            delimiter = arguments[1];
        if (typeof (arguments[2]) === 'number')
            limit = arguments[2];

        var chara = "", nchara = "";
        if (targetstring) {
            var escapeFlag = false, buf = "", counter = 0;

            for (var i = 0, j = targetstring.length; i < j; i++) {
                chara = targetstring.charAt(i);
                nchara = targetstring.charAt(i + 1);
                if (chara === "\\" && nchara === delimiter) {
                    escapeFlag = true;
                }
                else if (escapeFlag === false && chara === delimiter) {
                    if (buf.length > 0) {
                        retval.push(buf);
                        if (limit > 0 && ++counter >= limit) {
                            var leftovers = targetstring.substring(i + 1).replace(/\\/g, "");
                            retval.push(leftovers);
                            return retval;
                        }
                    }
                    buf = "";
                }
                else {
                    buf += chara;
                    escapeFlag = false;
                }
            }
            if (buf) {
                retval.push(buf);
            }
        }
        return retval;
    },
    /* escape for splitByChar | and = */
    escapeMacroQueryString: function (s) {
        if (typeof (s) !== "string")
            return "";
        return s.replace(/=/g, "\\=").replace(/\|/g, "\\|");
    },
    parseMacroQueryString: function (querystring) {
        var delim1 = "|", delim2 = "=";

        if (typeof (arguments[1]) !== 'undefined')
            delim1 = arguments[1];
        if (typeof (arguments[2]) !== 'undefined')
            delim2 = arguments[2];

        var params = {};
        var paramsPair = Conf2dita.splitByChar(querystring, delim1); // not data() but attr()
        for (var i = 0, j = paramsPair.length; i < j; i++) {
            var _pair = Conf2dita.splitByChar(paramsPair[i], delim2, 1);
            if (_pair.length < 2) {
                _pair.push(null);
            }
            params[_pair[0]] = _pair[1];
        }
        return params;
    },
    /* get macro parameter from macro wysiwyg table-dom */
    // split 'data-macro-parameters="base=aaa\|bbb\=ccc|conf2dita_table_order=1|frame=bottom|platform=pla\=foo\|\|\|rrr\=mmm|props=test"'
    getMacroParameter: function (macro) {
        return Conf2dita.parseMacroQueryString($(macro).attr('data-macro-parameters'));
    }
};

AJS.$((function ($) {
    AJS.log("***** common");
    /// set global parameters and functions
    var pageId;

    /* Common params for page and editor */
    Conf2dita.Common = {
        init: function () {

            Conf2dita.ditaPrefixRegexp = new RegExp("^" + Conf2dita.ditaPrefix + "(.*)$");

            var baseUrl = AJS.General.getBaseUrl();
            for (var url in Conf2dita.ctrlUrl) {
                Conf2dita.ctrlUrl[url] = Conf2dita.ctrlUrl[url].replace("{baseUrl}", baseUrl);
            }
        },
        onLoad: function () {
            pageId = AJS.params.pageId;
        },
        getDitaPropertiesFromClass: function(dom) {
            var properties = {};
            $.each(Conf2dita.splitByChar($(dom).attr('class'), " "), function(i, _class){
                if (_class.match(Conf2dita.ditaPrefixRegexp)){
                    properties = Conf2dita.parseMacroQueryString(Conf2dita.base64decode(RegExp.$1), '&');
                }
            });
            return properties;
        },
        /* load dita properties from class and set as jQuery.data() properties. */
        loadDitaProperties: function() {
            $(Conf2dita.pageMain).find(Conf2dita.domPropertyTags.join(',')).each(function(i, ele){
                var properties = Conf2dita.Common.getDitaPropertiesFromClass(ele);
                if (Object.keys(properties).length > 0){
                    $(ele).addClass('dita-tag');
                    for (var i in properties){
                        if (properties[i]){
                            $(ele).data('ditaparameter-' + i , properties[i]);
                        }
                    }
                }
            });
        },
        loadListProperties: function (targetLists, property) {
        }
    };

    /* initialize */
    Conf2dita.Common.init();
    return function () {
        //var plugin_key = "jp.co.kodnet.confluence.plugins.smartDITA";
        var plugin_key = "jp.junoe.confluence.plugins.conf2dita";
        // when whole document loaded
        AJS.log("***** common onLoad");
        Conf2dita.Common.onLoad();
        AJS.I18n.get(plugin_key, function () {
        }, function () {
        });
    };
})(AJS.$));
