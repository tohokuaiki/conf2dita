/* for view page */
if (typeof Conf2dita === 'undefined') {
    var Conf2dita = {};
}

AJS.$((function($){
    AJS.log("***** pages"); 
    
    var pageId ;
    
    // i18n shortcut
    var _ = function() {
        return AJS.I18n.getText.apply(this, arguments);
    };
    
    // error message
    var showMessage = function(type, message) {
        Conf2dita.showMessage(type, "#action-messages", message);
    };
    
    Conf2dita.Page = {
      
      init: function(self){
      },
      
      onLoad: function(self){
          AJS.log("***** pages onLoad"); 
          pageId = AJS.params.pageId;
          Conf2dita.Common.loadDitaProperties();
          self.setDitaParameter2qTip($('.dita-tag'));
          self.showDitaTopicType();
          $(document).ajaxComplete(self.onAjaxComplete);
      },
      
      onAjaxComplete: function(event, xhr, ajaxSettings){
          var self = Conf2dita.Page;
          // POST/DELETE label 
          var method = ajaxSettings.type.toLowerCase();
          if ((method === 'post' &&   ajaxSettings.url.match(Conf2dita.RestUrlRexExp.label.post)) ||
              (method === 'delete' && ajaxSettings.url.match(Conf2dita.RestUrlRexExp.label.del))) {
              Conf2dita.Page.showDitaTopicType();
          }
      },
      
      /**
       * get dita elements. 1:dita-macro 2:tables directly wrapped dita-table macros.
       * @return array
       */
      getDitaElements: function(){
      },
      
      // create dita parameter view windows using qtip2
      setDitaParameter2qTip: function(ditaElements) {
          ditaElements.each(function(i, dita) {
              var paramData = $(dita).data();
              var tagname = $(dita).data('ditatag');
              console.log(dita); 
              if (!tagname) {
                  // in case of table element row or cell
                  switch (dita.tagName.toLowerCase()) {
                    case "tr":
                      tagname = "row";
                      break;
                    case "td": case "th":
                      tagname = "cell";
                      break;
                    case "li":
                      tagname = "list item";
                      break;
                    default:
                      tagname = "undefined";
                  }
              }
              
              var span = $('<span class="dita-parameter-qtip">&nbsp;</span>');
              var paramHtml = "", paramName = "", paramValue = "";
              for (var i in paramData){
                  paramName = "";
                  paramValue = paramData[i];
                  // dita macro
                  if (i.substr(0, 13).toLowerCase() === "ditaparameter"){
                      paramName = i.substring(13);
                      // hidden control parameter. contents is display.
                      if (paramName.match(/^Conf2dita_/) ||
                          paramName.toLowerCase() === 'contents'){
                          continue; 
                      }
                  }
                  // table element
                  if (i.match(Conf2dita.ditaPrefixRegexp)){
                      paramName = RegExp.$1;
                  }
                  if (paramName && paramValue){
                      if (!paramHtml) {
                          paramHtml = '<dl class="ditaparameter-qtip">';
                      }
                      paramHtml += "<dt>"+ paramName +"</dt>";
                      paramHtml += "<dd>&nbsp;&nbsp;"+ paramValue + "</dd>";
                  }
              }
              if (paramHtml){
                  paramHtml += "</dl>";
                  span
                    .attr('qtip-content', paramHtml)
                      .attr('qtip-title', tagname); // not property as table sorter TH will to be removed.
                  // insert
                  switch(dita.tagName.toLowerCase()){
                    case "tr":
                      var target = $(dita).find('th,td');
                      if (target.length > 0){
                          $(target.get(0)).append(span);
                      }
                      break;
                    case "th":case "td":
                      $(dita).append(span);
                      break;
                    default:
                      $(dita).prepend(span);
                  }
              }
          });
          $(document).on('click', 'span.dita-parameter-qtip', function(e){
              $(this).qtip({
                overwrite: false,
                content: {
                  title: $(this).attr('qtip-title'),
                  button: true,
                  text:  $(this).attr('qtip-content')
                },
                position: {
                  my: 'top left',
                  at: 'bottom center'
                },
                show: { event: 'click firsttrigger' },
                hide: { event: false }
              }).trigger('firsttrigger');
          });
      },
      /* display dita topictype if assigned  */
      showDitaTopicType: function() {
          $.ajax({
            url: Conf2dita.ctrlUrl.ditaPropertyRest,
            type: "get",
            data: {
                pageId: pageId
            }
          }).success(function(json, status, xhr){
              var wrapper = $('#content.page div.page-metadata');
              var li = Conf2dita.Page.Template.ditaTopicPropertyInfo(json);
              wrapper.find('.dita-topic-property').remove();
              wrapper.append(li);
          });
      }
    };
    
    /* initialize */
    Conf2dita.Page.init(Conf2dita.Page);
    return function(){
        if ($('#content .wiki-content').length > 0){
            Conf2dita.Page.onLoad(Conf2dita.Page);
        }
    };
})(AJS.$));
