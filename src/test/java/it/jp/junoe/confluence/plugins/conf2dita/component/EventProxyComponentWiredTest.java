package it.jp.junoe.confluence.plugins.conf2dita.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import jp.junoe.confluence.plugins.conf2dita.component.EventProxyComponent;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class EventProxyComponentWiredTest
{
    private final ApplicationProperties applicationProperties;
    private final EventProxyComponent myPluginComponent;

    public EventProxyComponentWiredTest(ApplicationProperties applicationProperties,EventProxyComponent myPluginComponent)
    {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
    }

//    @Test
//    public void testMyName()
//    {
//        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(),myPluginComponent.getName());
//    }
}