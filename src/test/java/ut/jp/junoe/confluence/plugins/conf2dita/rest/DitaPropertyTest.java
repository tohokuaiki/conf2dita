package ut.jp.junoe.confluence.plugins.conf2dita.rest;

import org.junit.After;
import org.junit.Before;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class DitaPropertyTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

//    @Test
//    public void messageIsValid() {
//        DitaProperty resource = new DitaProperty();
//
//        Response response = resource.getMessage();
//        final DitaPropertyModel message = (DitaPropertyModel) response.getEntity();
//
//        assertEquals("wrong message", "Hello World", message.getMessage());
//    }

    @XmlRootElement(name = "message")
    @XmlAccessorType(XmlAccessType.FIELD)
    public class DitaPropertyModel {

        @XmlElement(name = "value")
        private String message;

        public DitaPropertyModel() {
        }

        public DitaPropertyModel(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
