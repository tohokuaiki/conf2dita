
    /**
     * @deprecated 
     * XML書き込みのサンプルとして
     *
     * @param XML
     * @throws SAXException
     * @throws UnsupportedEncodingException
     */
    private static void xmlWriterSample(String xmlpath) throws SAXException, UnsupportedEncodingException, IOException, Exception {
        String dtdDir = DitaDTD.setDTDFiles();
        SAXReader reader = new SAXReader();
        Document doc = null;
        InputStreamReader ir = null;
        XMLWriter xmlw = null;
        StringWriter stringWriter = null;
        OutputFormat outputFormat = null;
        reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        try {
            ir = new InputStreamReader(new FileInputStream(xmlpath), "UTF-8");
            // ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(XML.getBytes("UTF-8"));
            // BufferedInputStream bufferedInputStream = new BufferedInputStream(byteArrayInputStream);
            // ir = new InputStreamReader(bufferedInputStream);
            doc = reader.read(ir);
            DitaDTD.replaceToLocalDTD(doc, dtdDir);
            // writer
            stringWriter = new StringWriter();
            outputFormat = new OutputFormat(" ", true, "UTF-8");
            outputFormat.setTrimText(true);
            xmlw = new XMLWriter(stringWriter, outputFormat);
            xmlw.write(doc);
            // FileUtils.writeStringToFile(new File(xmlpath), stringWriter.toString(), "UTF-8");
        } catch (DocumentException ex) {
            Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw (Exception) ex;
        } finally {
            try {
                if (xmlw != null) {
                    xmlw.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(DitaXmlUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
